# P e r B a C C o  

PerBaCCo is a (1+1)D time-domain code to study linear perturbations of TOV stars.

## SOURCE FILES

```
axial.f90
axial_init.f90
background.f90
const.f90
driver.f90
eos.f90
equil.f90
myutil.f90
nradp.f90
nrtype.f90
nrutil.f90
polar.f90
polar_init.f90
polar_potentials.f90
radial.f90
tov_odeint.f90
```

## MODULES & SUBROUTINES LIST


```
const..............[const.f90]..............module, CONTAINS PHYSICAL CONSTANT 
nrtype.............[nrtype.f90].............module, from NR 								
nrutil.............[nrutil.f90].............module, from NR 											
nradp..............[nradp.f90]..............module, CONTAINS ROUTINES ADAPTED from NR
myutil.............[myutil.f90].............module, CONTAINS OTHER USEFUL ROUTINES                                                               
eos................[eos.f90]................module, CONTAINS EoS ROUTINES 									
bckgrd.............[background.f90].........module, CONTAITNS ROUTINES TO LOAD THE EQUILIBRIUM VARS FROM TOV DATA FILES 				
tov_derivs.........[tov_odeint.f90].........subroutine, SET TOV EQUATIONS 									
odeint.............[tov_odeint.f90].........subroutine, INTEGRATE TOV ODEs (See N.R. Chap.16.1-2) 						
rkqs...............[tov_odeint.f90].........subroutine, "Quality-controlled" Runge-Kutta step (See N.R. Chap.16.1-2) 				
rkck...............[tov_odeint.f90].........subroutine, Cash-Karp Runge-Kutta step  (See N.R. Chap.16.1-2) 					
equil..............[equil.f90]..............subroutine, SOLVES THE TOLMAN-OPPENHEIMER-VOLKOFF EQUATIONS FOR THE EQUILIBRIUM CONFIGURATION (POLYTROPIC EoS) 
constdens_equil....[equil.f90]..............subroutine, EQUILIBRIUM CONFIGURATION FOR A CONSTANT DENSITY EoS (Analytic) 				
radial.............[radial.f90].............subroutine, SOLVES THE "WAVE" EQUATION FOR THE RADIAL (L=0) PERTRUBATIONS OF A SPHERICAL TOV STAR    
radial_init........[radial.f90].............subroutine, INITILA CONDITION FOR THE RADIAL TIME EVOLUTION                                          
radial_outtime.....[radial.f90].............subroutine, OUTPUT OF THE RADIAL DATA                                                                
axial..............[axial.f90]..............subroutine, SOLVES THE "WAVE" EQUATION FOR THE AXIAL (ODD-PARITY) PERTURBATIONS OF A SPHERICAL TOV STAR 
axial_potential....[axial.f90]..............subroutine, LOAD BACKGROUND VARS AND BUILD REGGE-WHEELER POTENTIAL 					
axial_outtime......[axial.f90]..............subroutine, OUTPUT OF THE AXIAL DATA 								
axial_init.........[axial_init.f90].........subroutine, INITIAL CONDITION FOR THE AXIAL TIME EVOLUTION 						
polar..............[polar.f90]..............subroutine, SOLVES THE EQUATIONs FOR THE POLAR (EVEN-PARITY) PERTURBATIONS OF A SPHERICAL TOV STAR 	
polar_potentials...[polar_potentials.f90]...subroutine, LOAD BACKGROUND VARS AND BUILD ZERILLI POTENTIAL AND COEFFS 				
polar_init.........[polar_init.f90].........subroutine, INITIAL CONDITION FOR THE POLAR TIME EVOLUTION 						
polar_movie........[polar.f90]..............subroutine, PRINT OUT POLAR MOVIES FOR YGRAPH 							
polar_out_time.....[polar.f90]..............subroutine, OUTPUT OF THE POLAR DATA 								
driver.............[driver.f90].............main 												
```

## PARAMETER FILES

See examples in

```
par/*.par
```

## EOS TABLES

See examples in

```
eos/EOS.INFO
```

## OUTPUT

```
DATAFILES			DATA (je=1:jiequil j=Jmin:Jmax, n=1:Nt)	
----------------------------------------------------------------------------------------------------------------------
radial_init.dat................ r(j) z(0,j)
radial_xi_atobs.dat............ t(n) z@ri1(n) z@ri2(n) z@ri3(n) z@ri4(n) z,t@ri1(n) z,t@ri2(n) z,t@ri3(n) z,t@ri4(n)
radial_xi_norm.dat............. t(n) L2_z(n)
axial_init.dat.................	r(j) z(0,j)
axial_z_atobs.dat..............	t(n) z@ro1(n) z@ro2(n) z@ro3(n) z@ro4(n) z,t@ro1(n) z,t@ro2(n) z,t@ro3(n) z,t@ro4(n)
axial_z_norm.dat...............	t(n) L2_z(n)
polar_chi_atobs.dat............	t(n) chi@ri1(n) chi@ri2(n) chi@ri3(n) chi@ri4(n)  
polar_chi_norm.dat.............	t(n) L2_chi(n)	
polar_h_atobs.dat..............	t(n) H@ri1(n) H@ri2(n) H@ri3(n) H@ri4(n)  
polar_k_atobs.dat..............	t(n) k@ri1(n) k@ri2(n) k@ri3(n) k@ri4(n)	
polar_k_norm.dat...............	t(n) L2_k(n)
polar_zan_atobs.dat............	t(n) Zan@ro1(n) Zan@ro2(n) Zan@ro3(n) Zan@ro4(n) 	
polar_z_atobs.dat..............	t(n) Z@ro1(n) Z@ro2(n) Z@ro3(n) Z@ro4(n)
polar_z_norm.dat...............	t(n) L2_Z(n)
tovequil.dat...................	m(je) p(je) rho(je) cs2(je)
tovequil_grid.dat..............	requil(je)  
tovequil_setup.dat............. Ms rmin drequil Rstar jiequil
```

Output for ygraph:	

```
radial_z.yg
axial_z.yg
polar_chi.yg
polar_h.yg
polar_k.yg
polar_zan.yg
polar_z.yg
```

## CLONE & COMPILE
	
```
$ git clone git@bitbucket.org:bernuzzi/perbacco.git
$ cd perbacco
$ mkdir -p obj
$ make 
```

Need only a F90 compiler. Select your F90 in the Makefile. Currently, 

```
F90=gfortran-9   # works (2025)
F90=ifort        # used to work (2009 ...)
F90=pgf90        # might need some modifications for output files 
```

## DOCUMENTATION

See

```
doc/
```

## DISTRIBUTION

Author: S.Bernuzzi

Distributed under GNU General Public License.

Please cite the following [paper](http://arxiv.org/abs/0803.3804):
       
> Gravitational waves from pulsations of neutron stars described by realistic Equations of State,
> Sebastiano Bernuzzi, Alessandro Nagar
> Phys.Rev.D78:024024,2008 10.1103/PhysRevD.78.024024

BiBTeX:

```
@article{Bernuzzi:2008fu,
    author = "Bernuzzi, Sebastiano and Nagar, Alessandro",
    title = "{Gravitational waves from pulsations of neutron stars described by realistic Equations of State}",
    eprint = "0803.3804",
    archivePrefix = "arXiv",
    primaryClass = "gr-qc",
    doi = "10.1103/PhysRevD.78.024024",
    journal = "Phys. Rev. D",
    volume = "78",
    pages = "024024",
    year = "2008"
}
```