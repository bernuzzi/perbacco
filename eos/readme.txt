	
	=============================================================	
	
	files or dir	Info

	=============================================================

	*.eos 		See EOS.INFO

	NSGTabs/	Tables from NSG	
	
	RNSTabs/	Tables from RNS

	matlab/

		eos.m          	Load and plot e.o.s tables
		hermint_der.m  	Hermite interpolation routines
		linint_der.m  	Linear interpolation routines
		locate.m      	Index-location routine
		rhopolyeos.m    Compute polytropic EoS table
		RNSTabs.m	Convert RNS eos tables to PerturbTOV format
		RNSTabs_conv.m	Conversion script

	=============================================================	
	
