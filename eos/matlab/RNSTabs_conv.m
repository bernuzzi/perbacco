%
% Conversion script from RNS EoS to PerturbTOVCode format
% Use function RNSTabs.m
%

RNSTabs('RNSTabs/eosA','A.eos');
RNSTabs('RNSTabs/eosAU','AU.eos');
RNSTabs('RNSTabs/eosB','B.eos');
RNSTabs('RNSTabs/eosC','C.eos');
RNSTabs('RNSTabs/eosF','F.eos');
RNSTabs('RNSTabs/eosFP','FP.eos');
RNSTabs('RNSTabs/eosFPS','FPSbis.eos');
RNSTabs('RNSTabs/eosG','G.eos');
RNSTabs('RNSTabs/eosL','L.eos');
RNSTabs('RNSTabs/eosN','N.eos');
RNSTabs('RNSTabs/eosNV','NV.eos');
RNSTabs('RNSTabs/eosO','O.eos');
RNSTabs('RNSTabs/eosUU','UU.eos');
%RNSTabs('RNSTabs/eosWNV','WNV.eos');
RNSTabs('RNSTabs/eosWS','WS.eos');
