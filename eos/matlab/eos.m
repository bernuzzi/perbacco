function eos(eosindx)
%
% eos.m 
% Load and plot e.o.s tables
% > eos(eosindx)
% eosindex: 1< <int> <20
%
% eos A             ->  1
% eos AU            ->  2
% eos B             ->  3
% eos C             ->  4
% eos F             ->  5 
% eos FP            ->  6 - BAD!!!
% eos FPSa          ->  7 
% eos FPSbis        ->  8
% eos FPS           ->  9
% eos G             -> 10
% eos L             -> 11
% eos N             -> 12
% eos NV            -> 13
% eos O             -> 14
% eos rhopolyk100n1 -> 15 
% eos SLy4a         -> 16
% eos SLy4          -> 17
% eos UU            -> 18
% eos WNV           -> 19
% eos WS            -> 20
%

disp(' ********************************** ');
disp('  E o S Tables ');
disp(' ********************************** ');

flist={'A.eos' ...               % 1
       'AU.eos' ...              % 2
       'B.eos' ...               % 3
       'C.eos' ...               % 4
       'F.eos' ...               % 5
       'FP.eos' ...              % 6 %%BAD!
       'FPSa.eos' ...            % 7
       'FPSbis.eos' ...          % 8 
       'FPS.eos' ...             % 9
       'G.eos' ...               % 10
       'L.eos' ...               % 11
       'N.eos' ...               % 12
       'NV.eos' ...              % 13
       'O.eos' ...               % 14
       'rhopolyk100n1_200.eos' ... % 15
       'SLy4a.eos' ...           % 16
       'SLy4.eos' ...            % 17
       'UU.eos' ...              % 18
       'WNV.eos' ...             % 19
       'WS.eos'};                % 20

%% Load EoS

ppp=logspace(22,37,600);
for indx=eosindx
  disp([' index = ' num2str(indx) ' -> ' flist{indx}]);
  [eos(indx).nb,eos(indx).rho,eos(indx).pre,eos(indx).cs2,eos(indx).gamma]=...
      LoadEoSFile(flist{indx},ppp);
end

%% Colors List 

col=rand(20,3);

%col=[1.0,0.0,0.0
%     0.0,1.0,0.0
%     0.0,0.0,1.0
%     1.0,1.0,0.0
%     0.0,1.0,1.0
%     1.0,0.0,1.0
%     0.5,0.0,1.0
%     0.0,0.5,1.0
%     1.0,0.5,0.0
%     1.0,0.0,0.5];

%% Plots

disp(' ********************************** ');
disp(' Plot 1 : Log_10(p) Vs Log_10(rho) ');

fh=figure;
axes1 = axes('Parent',fh);
box(axes1,'on');
for indx=eosindx
  hline(indx)=line(log10(eos(indx).rho),log10(eos(indx).pre),...
                   'Color',col(indx,:),'LineWidth',2);
end
xlabel('log_{10}\rho')
ylabel('log_{10}p')
legend(flist(eosindx),'Location','NorthWest')

disp(' ********************************** ');
disp(' Plot 2 : Gamma Vs Log_10(rho) ');

fh=figure;
%axes1 = axes('Parent',fh,'YMinorTick','on','YScale','log');
axes1 = axes('Parent',fh);
box(axes1,'on');
for indx=eosindx
  hline(indx)=line(log10(eos(indx).rho),eos(indx).gamma,...
                   'Color',col(indx,:),'LineWidth',2);
end
xlabel('log_{10}\rho')
ylabel('\Gamma')
axis([6 17 0 5])
legend(flist(eosindx),'Location','NorthWest')

disp(' ********************************** ');
disp(' Plot 3 : C_s Vs Log_10(rho) ');

fh=figure;
axes1 = axes('Parent',fh,'YMinorTick','on','YScale','log');
%axes1 = axes('Parent',fh);
box(axes1,'on');
for indx=eosindx
  hline(indx)=line(log10(eos(indx).rho),sqrt(eos(indx).cs2),...
               'Color',col(indx,:),'LineWidth',2);
end
xlabel('log_{10}\rho')
ylabel('C_s')
legend(flist(eosindx),'Location','NorthWest')

disp(' ********************************** ');

%% Interpolated Plots

disp(' ---------------------------------- ');
disp(' Plot 1 : Log_10(p) Vs Log_10(rho) ');

fh=figure;
axes1 = axes('Parent',fh);
box(axes1,'on');
for indx=eosindx
  X = log10(filter([1:2]/2,1,eos(indx).rho));
  Y = spline(log10(eos(indx).rho),log10(eos(indx).pre),X);
  hline(indx)=line(X,Y,'Color',col(indx,:),'LineWidth',2);
end
xlabel('log_{10}\rho')
ylabel('log_{10}p')
legend(flist(eosindx),'Location','NorthWest')

disp(' ---------------------------------- ');
disp(' Plot 2 : Gamma Vs Log_10(rho) ');

fh=figure;
%axes1 = axes('Parent',fh,'YMinorTick','on','YScale','log');
axes1 = axes('Parent',fh);
box(axes1,'on');
for indx=eosindx
  X = log10(filter([1:2]/2,1,eos(indx).rho));
  Y = spline(log10(eos(indx).rho),eos(indx).gamma,X);
  hline(indx)=line(X,Y,'Color',col(indx,:),'LineWidth',2);
end
xlabel('log_{10}\rho')
ylabel('\Gamma')
axis([3 16 0 5])
legend(flist(eosindx),'Location','NorthWest')

disp(' ---------------------------------- ');
disp(' Plot 3 : C_s Vs Log_10(rho) ');

fh=figure;
axes1 = axes('Parent',fh,'YMinorTick','on','YScale','log');
box(axes1,'on');
for indx=eosindx
  X = log10(filter([1:2]/2,1,eos(indx).rho));
  Y = spline(log10(eos(indx).rho),sqrt(eos(indx).cs2),X);
  hline(indx)=line(X,Y,'Color',col(indx,:),'LineWidth',2);
end
xlabel('log_{10}\rho')
ylabel('C_s')
legend(flist(eosindx),'Location','NorthWest')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [nb,dens,p,cs2,gamma]=LoadEoSFile(fname,p)
fid = fopen(fname);
n = textscan(fid,'%n',1);
d = textscan(fid,'%n%n%n',n);
fclose(fid);
nb=d{1};
rho=d{2};
pre=d{3};
ntab=n{1};
%--------------------------------------------------
cctkMdens_cgs = 6.1764e+17;
cctkPress_cgs = 5.5511e+38;
cctkVolume_cgs = 3.2202e+15;
cctkSpeed_cgs = 2.9979e+10;
%--------------------------------------------------
x = log10(p/cctkPress_cgs);
z = zeros(size(x));
y = zeros(size(x));
xtab = log10(pre/cctkPress_cgs);
ztab = log10(nb*cctkVolume_cgs);
ytab = log10(rho/cctkMdens_cgs);
dydz1law = (10.^xtab+10.^ytab)./(10.^ytab);
for k=1:length(p)
  [z(k),dzdx(k),d2zdx(k)]=linint_der(xtab,ztab,ntab,x(k));
  [y(k),dydz(k),d2ydz(k)]=hermint_der(ztab,ytab,dydz1law,ntab,z(k));
end
dydx = 10.^x.*(10.^x+10.^y)./(d2ydz./log(10)-dydz+dydz.^2)./(10.^(2*y));
dens = 10.^y;
cs2 = 10.^(x-y)./dydx;
%--------------------------------------------------    
dens=dens*cctkMdens_cgs;
cs2=cs2*cctkSpeed_cgs*cctkSpeed_cgs;
gamma=(dens./p + 1/cctkSpeed_cgs/cctkSpeed_cgs).*cs2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



