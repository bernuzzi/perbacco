%%%% 

pvalues=logspace(-11,-4,50);

vlight = 2.99792458d+10;
newtng = 6.673d-08;	                                                           
msun   = 1.9889d+33;	                                                           
mb     = 1.675d-24;     

cctklength_cgs = newtng*msun/(vlight^2);
cctktime_cgs   = newtng*msun/(vlight^3);
cctkmass_cgs   = msun;
cctkenergy_cgs = msun*vlight^2;
cctksurf_cgs   = cctklength_cgs^2;
cctkvol_cgs    = cctklength_cgs^3;
cctkpress_cgs  = msun/(cctklength_cgs*cctktime_cgs^2);
cctkmdens_cgs  = cctkmass_cgs/cctkvol_cgs;
cctkedens_cgs  = cctkenergy_cgs/cctkvol_cgs;

col=load('Atest.eos');

ztab = log10(col(:,1)*cctkvol_cgs);
ytab = log10(col(:,2)/cctkmdens_cgs);
xtab = log10(col(:,3)/cctkpress_cgs);

dydztab = (10.^xtab+10.^ytab)./(10.^ytab);

n = length(xtab);


for k=1:50

  pres=pvalues(k);

  xi = log10(pres);
  
  %dydx = pres*(pres+10**y)/(d2ydz/dlog(10.d0)-dydz+dydz**2)/10**(2.d0*y)
  %dens = 10.^y
  %dpde = pres./dens./dydx
  
  %%%% Hermite L
  
  [zi,dzdxi,d2zdxi] = linint_der(xtab,ztab,n,xi);
  [yi,dydzi,d2yidzi] = hermint_der(ztab,ytab,dydztab,n,zi);
  
  testHl(k) = dydzi - (10.^xi+10.^yi)./10.^yi;

  %%%% Hermite S

  zi = spline(xtab,ztab,xi); % use spline_der.m
  [yi,dydzi,d2yidzi] = hermint_der(ztab,ytab,dydztab,n,zi);

  testHs(k) = dydzi - (10.^xi+10.^yi)./10.^yi;

  %%%% Spline % use spline_der.m

  %
  %
  %dydzi=dydxi./dzdxi;

  %testS(k) = dydzi - (10.^xi+10.^yi)./10.^yi;

  %%%% Linear 
  
  [zi,dzdxi,d2zdxi] = linint_der(xtab,ztab,n,xi);
  [yi,dydxi,d2ydxi] = linint_der(xtab,ytab,n,xi);
  dydzi=dydxi./dzdxi;
  
  testL(k) = dydzi - (10.^xi+10.^yi)./10.^yi;
  
end


figure
%% semilogy(1:50,abs(testL),1:50,abs(testHl),1:50,abs(testHs))
loglog(pvalues,abs(testL),pvalues,abs(testHl),pvalues,abs(testHs))
xlabel('p')
ylabel('|0|')
legend('L','Hl','Hs')