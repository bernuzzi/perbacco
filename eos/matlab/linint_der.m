function [yi,d1yi,d2yi]=linint_der(x,y,n,xi)
% linint_der.m
% Same routine for the interpolation implemented in the code
% Usage: > [yi,d1yi,d2yi]=linint_der(x,y,n,xi);
        
j=locate(x,n,xi);
jpo=j+1;
d2yi=0;
d1yi=(y(jpo)-y(j))/(x(jpo)-x(j));
yi = y(j) + d1yi*(xi - x(j));

