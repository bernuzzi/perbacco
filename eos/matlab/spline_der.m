function [yi,d1yi,d2yi]=spline_der(xa,ya,y2a,n,xi)
% spline_der.m
% Same routine for the interpolation implemented in the code
% Usage: > [yi,d1yi,d2yi]=spline_der(x,y,y2,n,xi);

klo=1;
khi=n;
1 if (khi-klo>1) then
  k=(khi+klo)/2;
  if(xa(k).gt.x)then
    khi=k;
  else
    klo=k;
  end
  goto 1
    end
    h=xa(khi)-xa(klo);
    if (h==0) pause 'bad xa input in splint_der'
      a=(xa(khi)-x)/h;
      b=(x-xa(klo))/h;
  
  y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0;
  
  y1=(ya(khi)-ya(klo))/h-((3.d0*a**2-1.d0)*y2a(klo)+(3.d0*b**2-1.d0)* ...
			  y2a(khi))*h/6.d0;

  y2=a*y2a(klo)+b*y2a(khi);

