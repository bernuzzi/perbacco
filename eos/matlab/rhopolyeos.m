function [nb,e,p]=rhopolyeos(k,g,pts)
% rhopolyeos.m
%              USAGE: > [n,e,p]=rhopolyeos(k,g,N)
%              Compute a table of N entries from the EoS

%--------------------------------------
% constants
mB=1.66*1e-24; %g
cctkMdens_cgs = 6.1764e+17; %g/cm^3
cctkPress_cgs = 5.5511e+38; %dynes/cm^2
%--------------------------------------
% dimensional/adimensional rho
rho_cgs = logspace(0.5,18,pts)';
rho = rho_cgs./cctkMdens_cgs; 
%--------------------------------------
% adimensional 
p = k*rho.^g;
e = rho + p./(g-1);
%--------------------------------------
% dimensional 
nb=rho_cgs./mB;
e_cgs = e*cctkMdens_cgs;
p_cgs = p*cctkPress_cgs;
%--------------------------------------
figure
subplot 121
loglog(e_cgs,p_cgs,'o-')
xlabel('e'); ylabel('P');
subplot 122
loglog(nb,p_cgs,'o-')
xlabel('n_B'); ylabel('P');
%--------------------------------------
tab=[nb e_cgs p_cgs];
save 'ppp.out' tab -ascii
 


