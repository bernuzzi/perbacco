function RNSTabs(eosin,eosout)
%
% RNSTabs.m
%          Convert RNS eos tables ('eosin'):
%
%         lin 1) number of tabulated points in file
%         col 1) energy density/c^2    [g/cm^3]
%         col 2) pressure              [dynes/cm^2]
%         col 3) enthalpy              [cm^2/s^2]
%         col 4) baryon number density [cm^{-3}]
%          
%          to perturb TOV format ('eosout'):
%
%         lin 1) number of tabulated points in file
%         col 1) baryon number density [cm^{-3}]
%         col 2) energy density/c^2    [g/cm^3]
%         col 3) pressure              [dynes/cm^2]
%
%          USAGE: > RNSTabs('eosin','eosout')
%             

disp('RNS -> PerturbTOVCode TAB eos conversion')

%------------------------------------------------------

[s,r] = system(['cp ' eosin ' tmp0']);
if( s~=0)
  disp(' Error during copy on temporary file 0')
  return
end
[s,r] = system(['awk "NR>1" tmp0 > tmp1']);
if( s~=0)
  disp(' Error during copy on temporary file 1')
  return
end

%------------------------------------------------------

aux = load('tmp1');
col1=aux(:,1);
col2=aux(:,2);
col3=aux(:,3);
col4=aux(:,4);
new=[col4 col1 col2];
ntab=length(new);

disp(['ntab = ' num2str(ntab)])

%------------------------------------------------------

fid = fopen(eosout,'w');
fprintf(fid,'%d\n',ntab);
for k=1:ntab
  fprintf(fid,'%12.8e %12.8e %12.8e\n',new(k,1),new(k,2),new(k,3));
end
fclose(fid);

%------------------------------------------------------

[s,r] = system(['ls ' eosout]);
if( isempty(r) )
  disp(' Error: in writing output file')
  return
else
  disp(['Written file: ' r])
end

[s,r] = system(['wc -l ' eosout]);
disp(['Written lines: ' r])

[s,r] = system(['rm tmp0 tmp1']);
if( s~=0)
  disp(' Error during removing temporary files')
  return
else
  disp('Removed temporary files.')
end
