function j=locate(xx,n,x)
% locate.m
% Given the vector xx(1...n) and the value x 
% return the index j such that xx(j) < x < xx(j+1)
% Usage: > j=locate(xx,n,x);
%
% REM: x must be inside xx !!!

jl=0;
ju=n+1;
ascnd=(xx(n)>=xx(1));
while(ju-jl > 1) 
  jm=floor(0.5*(ju+jl)); 
  if ((x >= xx(jm))==ascnd)
    jl=jm;
  else
    ju=jm;
  end
end
if(x==xx(1))
  j=1;
elseif(x==xx(n))
  j=n-1;
else
  j=jl;
end

%% Here a correction for exterme values

if j>=n
  j=n-1;
elseif j<1
  j=1;
end
