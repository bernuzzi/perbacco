function [yi,d1yi,d2yi]=hermint_der(x,y,dydx,n,xi)
% hermint_der.m
% Same routine for the interpolation implemented in the code
% Usage: > [yi,d1yi,d2yi]=hermint_der(x,y,dydx,n,xi);

j=locate(x,n,xi);
jpo=j+1;

w=(xi-x(j))/(x(jpo)-x(j));
h0w = 2.d0*w^3 - 3.d0*w^2 + 1.d0;
h1w = w^3 - 2.d0*w^2 + w;

mw = 1.d0-w;
h0mw = 2.d0*mw^3.d0 - 3.d0*mw^2 + 1.d0;
h1mw = mw^3.d0 - 2.d0*mw^2 + mw;

dydxj   = dydx(j);
dydxjpo = dydx(jpo);

yi = y(j)*h0w ...
     + y(jpo)*h0mw ...
     + dydxj*(x(jpo)-x(j))*h1w ...
     - dydxjpo*(x(jpo)-x(j))*h1mw;

dw = 1.d0/(x(jpo)-x(j));

d1yi = y(j)*(6.d0*w^2.d0-6.d0*w)*dw ...
       - y(jpo)*(6.d0*mw^2-6.d0*mw)*dw ...
       + dydxj*(3.d0*w^2-4.d0*w+1.d0) ...
       + dydxjpo*(3.d0*mw^2-4.d0*mw+1.d0);

d2yi = y(j)*(12.d0*w-6.d0)*dw*dw ...
       + y(jpo)*(12.d0*mw-6.d0)*dw*dw ...
       + dydxj*(6.d0*w-4.d0)*dw ...
       - dydxjpo*(6.d0*mw-4.d0)*dw;




    
