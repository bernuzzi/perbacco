% This script builds from orginal tables new tables
% using interpolations or different sampling

% -------------------------------------------------

NAME='CFL-1-2' % TM1 % CFL-1-1 % CFL-1-2

% points to interpolate
ntab=500;

% indexes for reduced tables

%% rIDX=[1:40:6000 6301:1:6400 6401:40:13000]; % >>> TM1
%% rIDX=[1:40:6300 6303:1:6407 6409:20:7408];  % >>> CFL-1-1
rIDX=[1:40:6300 6303:1:6500 6500:20:7410];     % >>> CFL-1-2

% --------------------------------------------------

cmd=['awk '' NR>1'' ' NAME '.dat > temp '];
system(cmd);

load temp;
n=temp(:,1);
e=temp(:,2);
p=temp(:,3);
dedn=(p+e)./n; % 1st law thermodynamics
cs=diff(p)./diff(e);

loge=log10(e);
logp=log10(p);
logn=log10(n);
dlogedlogn=n.*dedn./e;

% filter table

winSize=5;
fn=filter(ones(1,winSize)/winSize,1,n);
fe=filter(ones(1,winSize)/winSize,1,e);
fp=filter(ones(1,winSize)/winSize,1,p);
fcs=diff(fp)./diff(fe);

% reduced table

nr=n(rIDX);
er=e(rIDX);
pr=p(rIDX);
csr=diff(pr)./diff(er);

% lin interp table

x=linspace(loge(1),loge(end),ntab);
el=10.^x;
for j=1:length(x)
    [yi,d1yi,d2yi]=linint_der(loge,logn,length(loge),x(j));
    nl(j) = 10^yi;
    [yi,d1yi,d2yi]=linint_der(loge,logp,length(loge),x(j));
    pl(j) = 10^yi;
    csl(j)= pl(j)*d1yi/el(j);
end

% Hermite interp table

%{

x=linspace(logn(1),logn(end),ntab);
nh=10.^x;
for j=1:length(x)
    [yi,d1yi,d2yi]=hermint_der(logn,loge,dlogedlogn,length(logn),x(j));
    eh(j) = 10^yi;
    ph(j) = eh(j).*(d1yi -1);    
    csh(j)= d1yi*(1+eh(j)*d2yi) -1; %using 1st law thermodynamics
end

%}

% plots

figure
loglog(e,p,'k')
hold on
loglog(er,pr,'r')
loglog(el,pl,'g')
%loglog(eh,ph,'b')
hold off
xlabel('e')
ylabel('p')
legend('original','reduced','interp lin','interp her','Location','SouthEast')

figure
loglog(e(1:end-1),cs,'k')
hold on
loglog(er(1:end-1),csr,'r')
loglog(el,csl,'g')
%loglog(eh,csh,'b')
hold off
xlabel('e')
ylabel('c_s^2')
legend('original','reduced','interp lin','interp her','Location','SouthEast')

% write out

k=1;
TAB(k).NAME=NAME;
TAB(k).n=nr;
TAB(k).e=er;
TAB(k).p=pr;
TAB(k).fname = [NAME '_red_' num2str(length(nr)) '.eos'];        

k=2;
TAB(k).NAME=NAME;
TAB(k).n=nl;
TAB(k).e=el;
TAB(k).p=pl;
TAB(k).fname = [NAME '_lini_' num2str(ntab) '.eos'];


%{
k=3;
TAB(k).NAME=NAME;
TAB(k).n=nh;
TAB(k).e=eh;
TAB(k).p=ph;
TAB(k).fname = [NAME '_heri_' num2str(ntab) '.eos'];
%}

for k=1:length(TAB)
    fid = fopen(TAB(k).fname,'w');
    fprintf(fid,'%d\n',length(TAB(k).n));
    for j=1:length(TAB(k).n)
        fprintf(fid,'%e %e %e\n',TAB(k).n(j),TAB(k).e(j),TAB(k).p(j));
    end
    fclose(fid);
end