! *****************************************************************
! * This module contains routines to load the equilibrium vars    *
! * from TOV background data files                                *
! *****************************************************************

module bckgrd
  use nrtype
  implicit none 
   
  interface bckgrd_setup
     module procedure bckgrd_setup
  end interface
   
  interface bckgrd_grid
     module procedure bckgrd_grid
  end interface
   
  interface bckgrd_vars
     module procedure bckgrd_vars
  end interface
   
   
contains  
   
  subroutine bckgrd_setup(filetovsetup,mstar,dr,rmin,rstar,jiequil)
    character(64) :: filetovsetup
    real(dp)      :: mstar,rmin,rstar,dr,jiequil_r
    integer(i4b)  :: jiequil
    open(11,file=filetovsetup,status='old')     
    read(11,100)mstar,dr,rmin,rstar,jiequil_r
100 format(5e16.8)
    close(11)
    jiequil=int(jiequil_r)
    return
  end subroutine bckgrd_setup
   
  subroutine bckgrd_grid(filetovgrid,jiequil,r)
    character(64) :: filetovgrid
    integer(i4b)  :: j,jiequil
    real(dp)      :: r(jiequil)
    open(21,file=filetovgrid,status='old')     
    do j=1,jiequil
       read(21,200) r(j)
    end do
    close(21)
200 format(f12.4)
    return
  end subroutine bckgrd_grid
   
  subroutine bckgrd_vars(filetov,jiequil,r_equil,ji,jmax,r_evolv,mass,pres,dens,a,cs2)
    use nradp
    character(64) :: filetov
    integer(i4b)  :: j,jiequil,ji,jmax
    real(dp)      :: r_equil(jiequil),em(jiequil),ep(jiequil),ed(jiequil),ea(jiequil),ec(jiequil)
    real(dp)      :: mass(ji),pres(ji),dens(ji),a(ji),cs2(ji)
    real(dp)      :: r_evolv(jmax)
    real(dp)      :: d1em,d1ed,d1ep,d1ea,d1ec
    real(dp)      :: dnem,dned,dnep,dnea,dnec
    real(dp)      :: d2em(jiequil),d2ed(jiequil),d2ep(jiequil),d2ea(jiequil),d2ec(jiequil)    
     
    open(31,file=filetov,status='old')
    do j=1,jiequil
       read(31,100) em(j),ed(j),ep(j),ea(j),ec(j)
    end do
    close(31)
     
    ! I order  Derivatives at the inner boundary (at the origin)
    d1em=(em(2)-em(1))/(r_equil(2)-r_equil(1))
    d1ed=(ed(2)-ed(1))/(r_equil(2)-r_equil(1))
    d1ep=(ep(2)-ep(1))/(r_equil(2)-r_equil(1))
    d1ea=(ea(2)-ea(1))/(r_equil(2)-r_equil(1))
    d1ec=(ec(2)-ec(1))/(r_equil(2)-r_equil(1))
     
    ! I order Derivatives at the outer boundary
    dnem=(em(jiequil)-em(jiequil-1))/(r_equil(jiequil)-r_equil(jiequil-1))
    dned=(ed(jiequil)-ed(jiequil-1))/(r_equil(jiequil)-r_equil(jiequil-1))
    dnep=(ep(jiequil)-ep(jiequil-1))/(r_equil(jiequil)-r_equil(jiequil-1))
    dnea=(ea(jiequil)-ea(jiequil-1))/(r_equil(jiequil)-r_equil(jiequil-1))
    dnec=(ec(jiequil)-ec(jiequil-1))/(r_equil(jiequil)-r_equil(jiequil-1))
     
    call spline(r_equil,em,jiequil,d1em,dnem,d2em)
    call spline(r_equil,ed,jiequil,d1ed,dned,d2ed)
    call spline(r_equil,ep,jiequil,d1ep,dnep,d2ep)
    call spline(r_equil,ea,jiequil,d1ea,dnea,d2ea)
    call spline(r_equil,ec,jiequil,d1ec,dnec,d2ec)
     
    do j=1,ji
       call splint(r_equil,em,d2em,jiequil,r_evolv(j),mass(j))
       call splint(r_equil,ed,d2ed,jiequil,r_evolv(j),dens(j))
       call splint(r_equil,ep,d2ep,jiequil,r_evolv(j),pres(j))
       call splint(r_equil,ea,d2ea,jiequil,r_evolv(j),a(j))
       call splint(r_equil,ec,d2ec,jiequil,r_evolv(j),cs2(j))
    end do
     
100 format(5e16.8)
    return
     
  end subroutine bckgrd_vars
   
end module bckgrd
