! *****************************************************************
! * T.O.V. polar perturbations                                    *
! * this routine solves the equations for the polar (even-parity) *
! * perturbations of a spherical tov star                         *
! *****************************************************************

subroutine polar(filetovsetup,filetovgrid,filetov,keos,geos,fileh,&
     & l,jmin,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4,djoutput,rextmax,ji, &
     & tottime,nmovie,ncount,cf,methodevChi,methodevZ, &
     & initialflag,ri,sigma,gets,getz,aleph,readfh,nnodes,&
     & perturbation_amplitude,flat,&
     & filepolartimez,filepolartimek, & 
     & filepolartimechi,filepolartimeh,filepolartimezan, &
     & filepolarz,filepolark,filepolarchi,filepolarzan,filepolarh,& 
     & filepolarnormz,filepolarnormk,filepolarnormchi,filepolarinit,eosflag)

  use nrtype
  use bckgrd,      only : bckgrd_setup
  use myutil

  implicit none

  real(dp)                      :: dr,drd,drevol,rextmax,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4
  real(dp),pointer,dimension(:) :: r(:),r2(:),r3(:)
  real(dp),pointer,dimension(:) :: rd(:),rd2(:),rd3(:),rd4(:)
  integer(i4b)                  :: jiequil,jmin,js,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4
  integer(i4b)                  :: ji,je,jmax,djoutput,jimo,jipo,jimt,jmaxmo  
  real(dp)                      :: dt,dt2,cf,cf2,time,tottime,q 
  real(dp)                      :: perturbation_amplitude
  integer(i4b)                  :: nr,nt,nmovie,ncount 
  real(dp)                      :: ri,sigma,aleph,nnodes
  character(64)                 :: initialflag,eosflag 
  real(dp)                      :: keos,geos                         ! eos parameters
  real(dp),pointer,dimension(:) :: eab(:),a(:),b(:),f(:)             ! equilibrium qts
  real(dp),pointer,dimension(:) :: rho(:),p(:),m(:),cs2(:)
  real(dp)                      :: mstar,drequil,rmin,rstar

  real(dp),pointer,dimension(:) :: k(:),knew(:),chi(:),chih(:),chinew(:)     ! perturbative vars
  real(dp),pointer,dimension(:) :: dknewdr(:),dchidr(:),dchinewdr(:)
  real(dp),pointer,dimension(:) :: u(:,:),unew(:,:),s(:,:),x(:),xh(:),xnew(:)
  real(dp),pointer,dimension(:) :: h(:),hh(:),hnew(:)
  real(dp),pointer,dimension(:) :: zan(:),zanpo(:)
  real(dp),pointer,dimension(:) :: z(:),zh(:),znew(:)
  real(dp),pointer,dimension(:) :: uz(:,:),uznew(:,:),sz(:,:)
  real(dp),pointer,dimension(:) :: zdot(:),zandot(:)

  real(dp),pointer,dimension(:) :: alpha(:),beta(:),gamma(:)          !eqs coefs
  real(dp),pointer,dimension(:) :: cs3u4(:),cs3k(:),cs3x(:),cs4u3(:)
  real(dp),pointer,dimension(:) :: cXXrr(:),cXXr(:),cXX(:),cXk(:)
  real(dp),pointer,dimension(:) :: chrr(:),chr(:),ch(:),chkr(:)
  real(dp),pointer,dimension(:) :: chxr(:),chk(:),chx(:)
  real(dp),pointer,dimension(:) :: czu3(:),czu4(:),czz(:),czsz(:)
  real(dp),pointer,dimension(:) :: czrr(:),czr(:),vz(:)
  real(dp),pointer,dimension(:) :: aux(:),auxz(:)
  real(dp),pointer,dimension(:) :: zx(:),zk(:),zkr(:) 
  real(dp),pointer,dimension(:) :: chamu4(:),chamx(:),chamk(:)
  real(dp),pointer,dimension(:) :: chamkrr(:),chamkr(:),chamh(:)

  real(dp)                      :: l,l2,lam,lam2                            ! shorthands
  integer(i4b)                  :: j,n,count                                ! counters
  character(64)                 :: filetovsetup,filetovgrid,filetov,fileh   ! filenames
  character(64)                 :: filepolartimez,filepolartimek,filepolartimechi,filepolartimeh,filepolartimezan
  character(64)                 :: filepolarz,filepolark,filepolarchi,filepolarh,filepolarzan
  character(64)                 :: filepolarnormz,filepolarnormk,filepolarnormchi
  character(64)                 :: readfh,methodevZ,gets,getz,flat          ! flags
  character(64)                 :: methodevChi,filepolarinit

  character(64)                 :: kder !!!!!!!! derivative k,r !!!!!!!!!

  count = 0   !initialization of the counter
  
  ! **************************************************************
  ! * 1. setting up the the grid and the potential               *
  ! **************************************************************
  
  call bckgrd_setup(filetovsetup,mstar,drequil,rmin,rstar,jiequil)
   
  drevol=(rstar-rmin)/(ji-0.5d0)

  jmax=ji+((rextmax-rstar)/drevol+1)  
  jmaxmo = jmax-1
  
  allocate(r(jmax))
  
  !r(0)=rmin is a ghost ...
  r(1)=rmin+0.5d0*drevol
  do j=1,jmaxmo
     r(j+1)=r(j)+drevol
  end do
  
  ! allocate vars
  allocate(r2(jmax),r3(jmax),rd(jmax),rd2(jmax),rd3(jmax),rd4(jmax)) 
  allocate(rho(jmax),p(jmax),m(jmax),cs2(jmax),a(jmax),b(jmax),eab(jmax))
  
  ! allocate perturbation vars
  allocate(k(jmax),knew(jmax),chi(jmax),chih(jmax),chinew(jmax))
  allocate(k(jmax),knew(jmax))
  allocate(dchidr(jmax),dchinewdr(jmax),dknewdr(jmax))
  allocate(u(jmax,2),unew(jmax,2),s(jmax,2)) 
  allocate(h(jmax),hh(jmax),hnew(jmax),zan(jmax),zanpo(jmax))
  allocate(z(jmax),zh(jmax),znew(jmax),uz(jmax,2),uznew(jmax,2),sz(jmax,2))
  allocate(zdot(jmax),zandot(jmax))

  ! allocate coeffs
  allocate(alpha(jmax),beta(jmax),gamma(jmax),f(jmax))
  allocate(cs3u4(jmax),cs3k(jmax),cs3x(jmax),cs4u3(jmax))
  allocate(cXXrr(jmax),cXXr(jmax),cXX(jmax),cXk(jmax))
  allocate(chrr(jmax),chr(jmax),ch(jmax))
  allocate(chkr(jmax),chxr(jmax),chk(jmax),chx(jmax))
  allocate(czu3(jmax),czu4(jmax),czz(jmax),czsz(jmax),czrr(jmax),czr(jmax),vz(jmax))
  allocate(aux(jmax),auxz(jmax))
  allocate(zx(jmax),zk(jmax),zkr(jmax),chamu4(jmax),chamx(jmax),chamk(jmax))
  allocate(chamkrr(jmax),chamkr(jmax),chamh(jmax))
  
  dr = drevol 
  drd = 1.d0/dr

  r2  = r*r
  r3  = r2*r
  rd  = 1.d0/r
  rd2 = rd*rd
  rd3 = rd*rd2
  rd4 = rd*rd3
  
  js = jmin+1   ! 1st point for evolution
  jimo = ji-1
  jipo = ji+1
  jimt = ji-2
  
  ! internal and external observers indexes
  ji1 = (ri1-r(jmin))/drevol
  ji2 = (ri2-r(jmin))/drevol
  ji3 = (ri3-r(jmin))/drevol
  ji4 = (ri4-r(jmin))/drevol
  jo1 = (ro1-r(jmin))/drevol
  jo2 = (ro2-r(jmin))/drevol
  jo3 = (ro3-r(jmin))/drevol
  jo4 = (ro4-r(jmin))/drevol
   
  ! potentials and coeffs
  call polar_potentials( a,b,eab,mstar,drevol,&
       & r,r2,r3,rd,rd2,rd3,rd4,dr,drd,rho,p,m,cs2, &
       & alpha,beta,gamma,cs3u4,cs3k,cs3x,cs4u3, &
       & chrr,chr,ch,chkr,chxr,chk,chx,czu3,czu4,czz,czsz, &
       & cXXrr,cXXr,cXX,cXk,&
       & czrr,czr,vz,zx,zk,zkr,&
       & chamu4,chamx,chamk,chamkrr,chamkr,chamh, &
       & jiequil,jmin,js,ji,jmax,l,filetov,filetovgrid,flat)
  
  ! time evolution settings
  dt      = cf*dr
  dt2     = dt*dt
  cf2     = cf*cf
  nt      = nint(tottime/dt)
  q       = (cf-1.d0)/(cf+1.d0)
  l2      = l*l
  lam     = l*(l+1.d0)
  lam2    = lam*lam
   
  ! screen info
  write(*,*)'  '
  write(*,*)' Settings : '
  write(*,*)'  '
  write(*,*)' l           = ',l
  write(*,*)' cf          = ',cf
  write(*,*)' total time  = ',tottime
  write(*,*)' nt          = ',nt   
  write(*,*)' ncount      = ',ncount
  write(*,*)' dt          = ',dt
  write(*,*)' movie time  = ',tottime
  write(*,*)' nmovie      = ',nmovie
  write(*,*)'  '
  write(*,*)' TOV equilibrium radial grid :'
  write(*,*)'  '
  write(*,*)' dr_equil    = ',drequil
  write(*,*)' jiequil          = ',jiequil
  write(*,*)'  '
  write(*,*)' Radial grid to evolve perturbations :'
  write(*,*)'  '
  write(*,*)' jmin        = ',jmin
  write(*,*)' ji          = ',ji
  write(*,*)' je          = ',jmax-ji
  write(*,*)' jmax        = ',jmax
  write(*,*)' ji1         = ',ji1
  write(*,*)' ji2         = ',ji2
  write(*,*)' ji3         = ',ji3
  write(*,*)' ji4         = ',ji4
  write(*,*)' jo2         = ',jo1
  write(*,*)' jo2         = ',jo2
  write(*,*)' jo3         = ',jo3
  write(*,*)' jo4         = ',jo4
  write(*,*)' djoutput    = ',djoutput
  write(*,*)' dr          = ',drevol  
  write(*,*)' r(1)        = ',r(1)
  write(*,*)' r(jmin)     = ',r(jmin)
  write(*,*)' r(ji)       = ',r(ji)
  write(*,*)' r(jmax)     = ',r(jmax)
  write(*,*)' r(ji1)      = ',r(ji1)
  write(*,*)' r(ji2)      = ',r(ji2)
  write(*,*)' r(ji3)      = ',r(ji3)
  write(*,*)' r(ji4)      = ',r(ji4)
  write(*,*)' r(jo1)      = ',r(jo1)
  write(*,*)' r(jo2)      = ',r(jo2)
  write(*,*)' r(jo3)      = ',r(jo3)
  write(*,*)' r(jo4)      = ',r(jo4)
  write(*,*)'  '
  write(*,*)' Method Ev. Chi   = ',methodevChi
  write(*,*)' Method Ev. Z     = ', methodevZ
  write(*,*)'  '  

  ! **************************************************************
  ! * 2. initial conditions                                      *
  ! **************************************************************
  
  call polar_init(ri,sigma,initialflag,aleph,readfh,nnodes,&
       & r,r2,r3,rd,rd2,dr,rho,m, &
       & k,chi,u,chih,h,hh,zan,z,zh,uz, &
       & chamu4,chamx,chamk,chamkrr,chamkr,chamh, &
       & js,ji,jmax,dt,l,fileh,perturbation_amplitude,filepolarinit)
   
  ! **************************************************************
  ! * 3. open all the output files                               *
  ! **************************************************************
  
  open(10,file = filepolartimez   ,status = 'unknown')
  open(11,file = filepolartimek   ,status = 'unknown')
  open(12,file = filepolartimechi ,status = 'unknown')
  open(13,file = filepolartimeh   ,status = 'unknown')
  open(14,file = filepolartimezan ,status = 'unknown')
  open(20,file = filepolarz       ,status = 'unknown')  
  open(21,file = filepolark       ,status = 'unknown')  
  open(22,file = filepolarchi     ,status = 'unknown')  
  open(23,file = filepolarzan     ,status = 'unknown')  
  open(24,file = filepolarh       ,status = 'unknown')  
  open(30,file = filepolarnormz   ,status = 'unknown')
  open(31,file = filepolarnormk   ,status = 'unknown')
  open(32,file = filepolarnormchi ,status = 'unknown')
  
  ! **************************************************************
  ! * 4. time evolution                                          *
  ! **************************************************************
  
  write(*,*)' Time evolution :'
  write(*,*)' '                                              
  write(*,*)'         iter       of                  '      
  write(*,*)' ***************************************'
  
  do n=0,nt
     
     time = n*dt
     if (mod(n,100).eq.0)then
        write(*,*)n,nt
     end if
     
     ! round to 0
     do j=jmin,jmax
        if(dabs(h(j)).lt.1.d-21)      h(j)=0.d0
        if(dabs(hh(j)).lt.1.d-21)     hh(j)=0.d0
        if(dabs(zan(j)).lt.1.d-21)    zan(j)=0.d0
        if(dabs(k(j)).lt.1.d-21)      k(j)=0.d0
        if(dabs(chi(j)).lt.1.d-21)    chi(j)=0.d0
        if(methodevChi.eq.'leapfrog')then     
           if(dabs(chih(j)).lt.1.d-21) chih(j)=0.d0
        else if((methodevChi.eq.'upwind').or.(methodevChi.eq.'lw'))then     
           if(dabs(u(j,1)).lt.1.d-21) u(j,1)=0.d0
           if(dabs(u(j,2)).lt.1.d-21) u(j,2)=0.d0
        end if
        if(dabs(z(j)).lt.1.d-21)      z(j)=0.d0
        if(methodevZ.eq.'leapfrog')then
           if(dabs(zh(j)).lt.1.d-21)  zh(j)=0.d0
        else if(methodevZ.eq.'upwind')then
           if(dabs(uz(j,1)).lt.1.d-21) uz(j,1)=0.d0
           if(dabs(uz(j,2)).lt.1.d-21) uz(j,2)=0.d0
        end if
     end do
     
     if (mod(n,nmovie).eq.0)then
        call polar_movie(n,dt,r,k,chi,zan,z,h,js,ji,jmax,djoutput)
     end if
     
     ! 4.1
     ! ***********************************************************
     ! * compute sources for upwind schemes                      *
     ! ***********************************************************
     
     if((methodevChi.eq.'upwind').or.(methodevChi.eq.'lw'))then
        
        s(:,1) = cs3u4(:) * u(:,2) + cs3k(:) * k(:) + cs3x(:) * chi(:)
        s(:,2) = cs4u3(:) * u(:,1)
        
     end if
     
     if(methodevZ.eq.'upwind')then
        
        sz(:,1) = czu4(:) * uz(:,2) + czz(:)*z(:)
        sz(:,2) = czu3(:) * uz(:,1)

        do j=jmin,jimo
           sz(j,1) = 0.d0
           sz(j,2) = 0.d0
        end do
        
     end if
     
     ! 4.2
     ! ***********************************************************
     ! * evolution of S=chi/r                                    *
     ! ***********************************************************
     
     if(methodevChi.eq.'upwind')then
        
        ! -------------------------------------------------------
        ! Upwind evolution u=[-exp(b-a)S,t ; S,r]
        ! -------------------------------------------------------
        
        do j=js,jmaxmo
           
           unew(j,1) = u(j,1)-0.5d0*dt*drd*eab(j) &
                & * (2.d0*u(j,1)-u(j-1,1)-u(j+1,1)+u(j+1,2)-u(j-1,2))&
                & + dt * s(j,1)
           unew(j,2) = u(j,2)-0.5d0*dt*drd*eab(j)&
                &  * (2.d0*u(j,2)-u(j-1,2)-u(j+1,2)+u(j+1,1)-u(j-1,1))&
                &  + dt * s(j,2) 
        end do
        
        unew(jmin,1) = 0.d0
        unew(jmin,2) = 0.d0                    
        
        unew(jmax,1) = u(jmaxmo,1)+(unew(jmaxmo,1)-u(jmax,1))*q
        unew(jmax,2) = u(jmaxmo,2)+(unew(jmaxmo,2)-u(jmax,2))*q
        
        ! -------------------------------------------------------
        ! Reconstruction of S
        ! -------------------------------------------------------
        
        if(gets.eq.'rect')then 
           
           ! Use rectangle rule to solve for chi
           ! Usually not very accurate
           
           chinew(jmin)=0.d0
           
           do j=js,jmax
              chinew(j) = chi(j) - dt*dexp(a(j)-b(j))*unew(j,1)
           enddo
           
        elseif(getS.eq.'trapz')then 
           
           ! Use trapezoidal rule to solve for chi

           chinew(jmin)=0.d0

           do j=js,jmax
              chinew(j) = chi(j) - 0.5d0*dt*dexp(a(j)-b(j))*(unew(j,1)+u(j,1))
           enddo
           
        end if
        
        ! -------------------------------------------------------
        ! Compute S,r
        ! -------------------------------------------------------
        
        dchidr = u(:,2)
        dchinewdr = unew(:,2)
        
     elseif(methodevChi.eq.'lw')then
        
        ! -------------------------------------------------------
        ! Lax-Wendroff evolution u=[-exp(b-a)S,t ; S,r]
        ! -------------------------------------------------------
        
        do j=js,jmaxmo
           
           unew(j,1) = u(j,1) + 0.5d0*cf*dexp(b(j)-a(j))&
                & *(u(j+1,2)-u(j-1,2)) &
                & + 0.5d0*cf2*dexp(2.d0*(b(j)-a(j)))&
                & *(u(j-1,1)-2.d0*u(j,1)+u(j+1,1))&
                & + dt * s(j,1)
           
           unew(j,2) = u(j,2) + 0.5d0*cf*dexp(b(j)-a(j))&
                & *(u(j+1,1)-u(j-1,1)) &
                & + 0.5d0*cf2*dexp(2.d0*(b(j)-a(j)))&
                & *(u(j-1,2)-2.d0*u(j,2)+u(j+1,2))&
                & + dt * s(j,2)
           
        end do
        
        unew(jmin,1) = 0.d0
        unew(jmin,2) = 0.d0                    
        
        unew(jmax,1) = u(jmaxmo,1)+(unew(jmaxmo,1)-u(jmax,1))*q
        unew(jmax,2) = u(jmaxmo,2)+(unew(jmaxmo,2)-u(jmax,2))*q
        
        ! -------------------------------------------------------
        ! Reconstruction of S
        ! -------------------------------------------------------
        
        if(gets.eq.'rect')then 
           
           ! Use rectangle rule to solve for chi
           ! Usually not very accurate
           
           chinew(jmin)=0.d0
           
           do j=js,jmax
              chinew(j) = chi(j) - dt*dexp(a(j)-b(j))*unew(j,1)
           enddo
           
        elseif(getS.eq.'trapz')then 
           
           ! Use trapezoidal rule to solve for chi

           chinew(jmin)=0.d0
           
           do j=js,jmax
              chinew(j) = chi(j) - 0.5d0*dt*dexp(a(j)-b(j))*(unew(j,1)+u(j,1))
           enddo
           
        end if
        
        ! -------------------------------------------------------
        ! Compute S,r
        ! -------------------------------------------------------
        
        dchidr = u(:,2)
        dchinewdr = unew(:,2)

     elseif(methodevChi.eq.'leapfrog')then
        
        ! -------------------------------------------------------
        ! Leapfrog evolution of S=chi/r (rem: variable chi==S)
        ! -------------------------------------------------------
        
        ! inner boundary
        chi(jmin)  = 0.d0
        chih(jmin) = 0.d0
        chinew(jmin) = 0.d0
        
        ! leapfrog up to date         
        do j=js,jmaxmo
           chinew(j) = 2d0*chih(j)-chi(j)+Cf2*( &
                &  cXXrr(j)*(chih(j+1)-2.d0*chih(j)+chih(j-1)) &
                & + dr*( cXXr(j)*0.5d0*(chih(j+1)-chih(j-1)) )&
                & + dr**2*(cXk(j)*k(j)+cXX(j)*chih(j))&
                & )
        enddo
        
        ! outer boundary
        chinew(jmax)  = chih(jmax-1)+(chinew(jmax-1)-chih(jmax))*q
        
        ! update 
        chi = chih
        
        ! -------------------------------------------------------
        ! Compute S,r
        ! -------------------------------------------------------
        
        call diff2nd(chi,dchidr,r,jmax)
        call diff2nd(chinew,dchinewdr,r,jmax)
        
     end if
     
     ! 4.3
     ! ***********************************************************
     ! * evolution of the fluid variable h                       *
     ! ***********************************************************
     
     hh(jmin) = 0.d0
     h(jmin)  = 0.d0
     
     do j=js,jimo
        
        hnew(j) = 2d0*hh(j)-h(j)+cf2*( &
             & chrr(j)*( hh(j+1)-2*hh(j)+hh(j-1) ) &
             & + dr*( chr(j) * 0.5d0*(hh(j+1)-hh(j-1)) &
             & + chkr(j) * 0.5d0*(k(j+1)-k(j-1)) &
             & + chxr(j) * dr*dchidr(j) ) & 
             & + dr**2*( ch(j)*hh(j)+chk(j) * k(j) &
             & + chx(j) * chi(j) ) &
             &  )
     end do
     
     hnew(ji) =  2d0*hh(j)-h(j) + dt2 * ( &
          & -mstar*(r(ji)-2*mstar)*rd3(ji)* &
          & (0.5*drd*(3*hh(ji)-4*hh(jimo)+hh(jimt)) &
          & +0.5d0*(r(ji)*dchidr(ji) & 
          & -0.5*drd*(k(jipo)-k(jimo)))) &
          & -0.5d0*mstar*(2*mstar+r(ji))*rd3(ji)*chi(ji) &
          & -2*mstar**2*rd2(ji)*rd2(ji)*k(ji) &
          & )  
     
     ! 4.4
     ! ***********************************************************
     ! * time evolution of k using hamiltonian constraint        *
     ! ***********************************************************
     
     ! -----------------------------------------------------------     
     ! set f inside
     ! -----------------------------------------------------------

     j=js-1
     f(j) = ( (2*r(j)+0.5d0*r(j)*(l*l+l)-2*m(j) &
          & -8*pi*rho(j)*r3(j))*chinew(j) ) * dr**2
     
     do j=js,ji
        f(j) = ( r(j)*(r(j)-2*m(j))*dchinewdr(j) &
             & +(2*r(j)+0.5d0*r(j)*(l*l+l)-2*m(j) &
             & -8*pi*rho(j)*r3(j))*chinew(j)  &
             & -8*pi*(p(j)+rho(j))*r2(j)/cs2(j)*hnew(j) &
             &  ) * dr**2
     end do
     
     ! -----------------------------------------------------------
     ! set f at surface 
     ! -----------------------------------------------------------

     f(ji) = ( r(ji)*(r(ji)-2*m(ji))*dchinewdr(ji) & 
          &  +(2*r(ji)+0.5d0*r(ji)*(l*l+l)-2*m(ji) &
          &  -8.d0*pi*rho(ji)*r3(ji))*chinew(ji) &
          !---------------------------------------------
          ! Use ~ -8 pi H dens / cs^2  for all EoS
          !---------------------------------------------
          & -8.d0*pi*hnew(ji)*rho(ji)/cs2(ji)&
          &  ) * dr**2 
     
     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! 4th Dec 2007 - commented - TODEL - BEGIN
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! ( this is eos dependent via cs2 ! )
     !     if(eosflag.eq.'poly')then
     !        ! poly eos
     !        f(ji) = ( r(ji)*(r(ji)-2*m(ji))*dchinewdr(ji) &
     !             &  +(2*r(ji)+0.5d0*r(ji)*(l*l+l)-2*m(ji) &
     !             &  -8*pi*rho(ji)*r3(ji))*chinew(ji) &
     !             !--------------------------------------------
     !             ! this below is corrected for general geos(>2 !!!) 
     !             !--------------------------------------------
     !             &  -8d0*pi*rho(ji)**(2-geos)*hnew(ji)/(keos*geos) &  
     !             &  ) * dr**2 
     !     else if(eosflag.eq.'rhopoly') then
     !        ! rho-poly eos
     !        f(ji)    = ( r(ji)*(r(ji)-2*m(ji))*dchinewdr(ji) &
     !             &  +(2*r(ji)+0.5d0*r(ji)*(l*l+l)-2*m(ji) &
     !             &  -8*pi*rho(ji)*r3(ji))*chinew(ji) &
     !             !---------------------------------------------
     !             ! Old expression:  
     !             !---------------------------------------------
     !             !&  -8d0*pi*hnew(ji)* geos*rho(ji)/(geos-1.d0) &
     !             !---------------------------------------------
     !             ! now corrected as: 
     !             !---------------------------------------------
     !             & -8.d0*pi*hnew(ji)*geos*keos*keos*rho(ji)**(2.d0*geos -2.d0) &
     !             & /(keos*geos*(geos-1.d0)*(geos-1.d0)*rho(ji)**(geos-2.d0)) &
     !             & -8.d0*pi*hnew(ji)*(geos*geos-1.d0)*keos*rho(ji)**(geos-1.d0) &
     !             & /(keos*geos*(geos-1.d0)*(geos-1.d0)*rho(ji)**(geos-2.d0)) &
     !             & -8.d0*pi*hnew(ji)/(keos*geos*rho(ji)**(geos-2.d0)) &
     !             !----------------------------------------------
     !             &  ) * dr**2 
     !        
     !     else if(eosflag.eq.'tab') then 
     !        f(ji)= ( r(ji)*(r(ji)-2*m(ji))*dchinewdr(ji) &
     !             &  +(2*r(ji)+0.5d0*r(ji)*(l*l+l)-2*m(ji) &
     !             &  -8.d0*pi*rho(ji)*r3(ji))*chinew(ji) &
     !             !---------------------------------------------
     !             ! ~ -8 pi H dens / cs^2  
     !             !---------------------------------------------
     !             & -8.d0*pi*hnew(ji)*rho(ji)/cs2(ji)&
     !             !---------------------------------------------
     !             &  ) * dr**2 
     !     end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! 4th Dec 2007 - commented - TODEL - END
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     
     ! -----------------------------------------------------------
     ! set f outside
     ! -----------------------------------------------------------
     
     do j=jipo,jmaxmo
        f(j) = ( r(j)*(r(j)-2*m(j))*dchinewdr(j) &
             & +(2*r(j)+0.5d0*r(j)*(l*l+l)-2*m(j) &
             & -8*pi*rho(j)*r3(j))*chinew(j) ) * dr**2
     end do
     
     alpha(jmax) = -q
     beta(jmax)  =  1.d0
     gamma(jmax) =  0.d0
     f(jmax)     =  k(jmaxmo)-k(jmax)*q
     
     call tridag(alpha,beta,gamma,f,knew,jmax)
     
     ! 4.5
     ! ***********************************************************
     ! * time evolution of z                                     *
     ! ***********************************************************
     
     if(methodevZ.eq.'leapfrog')then
     
        ! --------------------------------------------------------   
        ! Leapfrog evolution
        ! --------------------------------------------------------   

        do j=jipo,jmax-1
           znew(j) = 2d0*zh(j)-z(j)+cf2*( &
                &  czrr(j)*(zh(j+1)-2.d0*zh(j)+zh(j-1)) &
                & + dr * ( czr(j) *0.5d0*(zh(j+1)-zh(j-1)) ) &     
                & + dr**2* vz(j)*zh(j) )
        end do
        
        ! boundary conditions on z
        j=ji
        znew(j) = 2d0/lam * ((r(j)-2*mstar)*r(j)/(lam-2d0+6.d0*mstar*rd(j)) &
             &  *(2d0*(chinew(j)-0.5d0*drd*(4*knew(j+1)-3*knew(j)-knew(j+2))) &
             &  +knew(j)*rd(j)*(r(j)*lam+2.d0*mstar)/(r(j)-2.d0*mstar)))
        
        znew(jmax)  = zh(jmax-1)+(znew(jmax-1)-zh(jmax))*q
        
        ! compute z,t

        zdot = 0.5d0*(znew-z)/dt

     elseif(methodevZ.eq.'upwind')then

        ! --------------------------------------------------------   
        ! Upwind evolution
        ! --------------------------------------------------------   
        
        do j=jipo,jmax-1
           uznew(j,1) = uz(j,1)-0.5d0*dt*drd*eab(j) &
                & * (2.d0*uz(j,1)-uz(j-1,1)-uz(j+1,1)+uz(j+1,2)-uz(j-1,2)) &
                & + dt * sz(j,1)
           uznew(j,2) = uz(j,2)-0.5d0*dt*drd*eab(j) &
                & * (2.d0*uz(j,2)-uz(j-1,2)-uz(j+1,2)+uz(j+1,1)-uz(j-1,1)) &
                & + dt * sz(j,2) 
        end do
        
        ! evolution at boundaries
        do j=ji,ji+2
           znew(j)= 2d0/lam * ((r(j)-2d0*mstar)*r(j)/(lam-2d0+6d0*mstar*rd(j)) &
                & *( 2d0*( chinew(j) &
                & -0.5d0*drd*(4*knew(j+1)-3*knew(j)-knew(j+2))) &
                & +knew(j)* rd(j)*(r(j)*lam+2d0*mstar)/(r(j)-2d0*mstar)))
        end do
        
        ! j=ji : compute uz(1) and uz(2)
        uznew(ji,1) = -dexp(b(ji)-a(ji))*(znew(ji)-z(ji))/dt
        uznew(ji,2) = 0.5d0*drd*(-3*znew(ji)+4*znew(jipo)-znew(ji+2))
        
        ! j=jmax : outgoing sommerfeld conditions   
        uznew(jmax,1)  = uz(jmax-1,1)+(uznew(jmax-1,1)-uz(jmax,1))*q
        uznew(jmax,2)  = uz(jmax-1,2)+(uznew(jmax-1,2)-uz(jmax,2))*q
        
        ! compute z,t
        
        zdot = - dexp(a-b)*uznew(:,1) 

        ! --------------------------------------------------------   
        ! Reconstruction of Z
        ! --------------------------------------------------------   
        
        if(getz.eq.'rect')then
           
           ! Use rectangle rule to solve for z
           ! Usually not very accurate
           
           znew(ji)=z(ji)
           
           do j=jipo,jmax
              znew(j) = z(j) - dexp(a(j)-b(j))*dt*uznew(j,1) 
           end do
           
        else if(getz.eq.'trapz')then
           
           ! Use trapezoidal rule to solve for z
           
           znew(ji)=z(ji)

           do j=jipo,jmax
              znew(j) = z(j) - 0.5d0*dexp(a(j)-b(j))*dt*(uznew(j,1)+uz(j,1)) 
           end do
           
        end if
        
     end if

     ! 4.6
     ! ***********************************************************
     ! Compute The "analytic" Zerilli-Moncrief function          *
     ! ***********************************************************

     ! -----------------------------------------------------------        
     ! spatial derivative of knew 
     ! -----------------------------------------------------------   
     
     !kder='1bckwrd' 
     !kder='2cntrd'
     kder='4cntrd'
     
     if(kder.eq.'1bckwrd')then
        
        call diff1stb(knew,dknewdr,r,jmax)
        
     elseif(kder.eq.'1frwrd')then
        
        call diff1stf(knew,dknewdr,r,jmax)

     elseif(kder.eq.'2cntrd')then
        
        call diff2nd(knew,dknewdr,r,jmax)
        
     elseif(kder.eq.'4cntrd')then
        
        call diff4th(knew,dknewdr,r,jmax)
        
     endif
     
     ! -----------------------------------------------------------   
     ! Algebrical computation of z from chi k and k,r
     ! Here chi is indeed S,since the r is hidden 
     ! in the background coefficient zx
     ! -----------------------------------------------------------   

     zanpo(jmin) = 0.d0
     zanpo = zx*chinew + zk*knew + zkr*dknewdr
     zanpo(jmax) = zan(jmax)+(zanpo(jmax-1)-zan(jmax))*q

     ! filter signal
     do j=5,jmax-4
!!! zanpo(j)=(zanpo(j-1)+zanpo(j)+zanpo(j+1))/3.d0
!!! zanpo(j)=(zanpo(j-2)+zanpo(j-1)+zanpo(j)+zanpo(j+1)+zanpo(j+2))/5.d0
        zanpo(j)=(zanpo(j-3)+zanpo(j-2)+zanpo(j-1)+zanpo(j)&
             &+zanpo(j+1)+zanpo(j+2)+zanpo(j+3))/7.d0
!!! zanpo(j)=(zanpo(j-4)+zanpo(j-3)+zanpo(j-2)+zanpo(j-1)+zanpo(j)&
        !         &+zanpo(j+1)+zanpo(j+2)+zanpo(j+3)+zanpo(j+4))/9.d0
     end do
     
!!!!!!!!!!!!!!!!!! TODEL - BEGIN
     !zanpo = 0.d0
     !do j=jipo,jmax-1
     !   zanpo(j) = zx(j)*chinew(j) + zk(j)*knew(j) + zkr(j)*dknewdr(j)
     !end do
     !j=ji
     !zanpo(j) = zX(j)*chinew(j)+zK(j)*knew(j) &
     !     &+ zKr(j)*0.5d0*drd*(4*knew(j+1)-3*knew(j)-knew(j+2)) 
     !j=jmax
     !zanpo(j) = zX(j)*chinew(j)+zK(j)*knew(j) &
     !     & + zKr(j)*0.5d0*drd*(-4*knew(j-1)+3*knew(j)-knew(j-2))
!!!!!!!!!!!!!!!!! TODEL - END    

     ! compute zan,t

     zandot = (zanpo - zan)/dt

     ! ***********************************************************
     ! * 5. update all the variables for the next time cycle     *
     ! ***********************************************************

     if(methodevChi.eq.'leapfrog')then     
        chih = chinew
     else if((methodevChi.eq.'upwind').or.(methodevChi.eq.'lw'))then     
        chi = chinew
        u(:,1) = unew(:,1)
        u(:,2) = unew(:,2)
     end if
     
     h  = hh
     hh = hnew
     
     k = knew
     
     zan = zanpo
     
     if(methodevZ.eq.'leapfrog')then
        z  = zh
        zh = znew
     else if(methodevZ.eq.'upwind')then
        z       = znew
        uz(:,1) = uznew(:,1)
        uz(:,2) = uznew(:,2)
     end if
     
     ! ***********************************************************
     ! * 6. printout of the data as functions of time            *
     ! ***********************************************************
     
     if(n.eq.(0+count*ncount))then
        call polar_outtime(n,dt,js,ji,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4,&
             & jmax,k,chi,h,zan,z,zandot,zdot)
        count=count+1
     end if
     
  end do
  
  write(*,*)' *************************************** ' 
  write(*,*)' '
  
  ! **************************************************************
  ! * 7. close all the output files                              *
  ! **************************************************************
  
  close(10)
  close(11)
  close(12)
  close(13)
  close(14)
  close(20)
  close(21)
  close(22)
  close(23)
  close(24)
  close(30)
  close(31)
  close(32)
  
  write(*,*)' Datafiles : '
  write(*,*)' '
  write(*,*)' ',filepolartimez
  write(*,*)' ',filepolartimek
  write(*,*)' ',filepolartimechi
  write(*,*)' ',filepolartimeh
  write(*,*)' ',filepolartimezan
  write(*,*)' ',filepolark
  write(*,*)' ',filepolarchi
  write(*,*)' ',filepolarh
  write(*,*)' ',filepolarzan
  write(*,*)' ',filepolarnormz
  write(*,*)' ',filepolarnormk
  write(*,*)' ',filepolarnormchi
  write(*,*)' ',filepolarinit
   
44 format(i10,e20.8)
   
  return
  
end subroutine polar


! ****************************************************************
! * output of the data                                           *
! ****************************************************************

subroutine polar_outtime(n,dt,js,ji,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4,&
     & jmax,k,chi,h,zan,z,zandot,zdot)
  
  use nrtype
  
  implicit none
  
  integer(i4b)  :: js,ji,je,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4,jmax,n,j,jmin,jipo
  real(dp)      :: dt,r(jmax),k(jmax),chi(jmax),h(jmax),zan(jmax),z(jmax),zandot(jmax),zdot(jmax)
  real(dp)      :: normk,normchi,normz,s
  
  je = jmax-ji
  jmin = js-1
  jipo = ji+1

  ! **************************************************************
  ! * computation of the square norms                            *
  ! **************************************************************
  
  normchi =  0.d0
  normk   =  0.d0
  normz   =  0.d0

  do j=jmin,jmax
     normk   = normk   + 0.25d0*k(j)*k(j)
     normchi = normchi + chi(j)*chi(j)
  end do
  normk   = normk/jmax
  normchi = normchi/jmax
  
  do j=jipo,jmax
     normz   = normz + 0.25d0*z(j)*z(j)
  end do
  normz = normz/je

  ! **************************************************************
  ! * printout of data in time                                   *
  ! * zan -> 0.5 zan = Psie                                      * 
  ! * z   -> 0.5 z   = Psie                                      * 
  ! **************************************************************

  write(11,100)n*dt,k(jo1),k(jo2),k(jo3),k(jo4) 
  write(12,100)n*dt,chi(jo1),chi(jo2),chi(jo3),chi(jo4) 
  write(13,100)n*dt,h(ji1),h(ji2),h(ji3),h(ji4)
  write(14,1000)n*dt,0.5d0*zan(jo1),0.5d0*zan(jo2),0.5d0*zan(jo3),0.5d0*zan(jo4),0.5d0*zandot(jo1),0.5d0*zandot(jo2),0.5d0*zandot(jo3),0.5d0*zandot(jo4)   
  write(10,1000)n*dt,0.5d0*z(jo1),0.5d0*z(jo2),0.5d0*z(jo3),0.5d0*z(jo4),0.5d0*zdot(jo1),0.5d0*zdot(jo2),0.5d0*zdot(jo3),0.5d0*zdot(jo4)   

  write(30,300)n*dt,normz
  write(31,300)n*dt,normk
  write(32,300)n*dt,normchi 

100 format(5e20.12)
1000 format(9e20.12)
300 format(2e20.12)
  
  return
end subroutine polar_outtime

! ****************************************************************
! * print out movies for ygraph                                  *
! ****************************************************************

subroutine polar_movie(n,dt,r,k,chi,zan,z,h,js,ji,jmax,djoutput)
   
  use nrtype
  
  implicit none
  
  integer(i4b) :: jmin,js,ji,jmax,n,j,djoutput
  real(dp)     :: dt,r(jmax),k(jmax),chi(jmax),h(jmax),zan(jmax),z(jmax)
  
  write(24,*)' '              
  write(24,*)'"time = ',n*dt  
  do j=js,ji
     write(24,200)r(j),h(j)
  end do
  
  write(23,*)' '              
  write(23,*)'"time = ',n*dt  
  do j=ji,jmax,djoutput
     write(23,200)r(j),0.5d0*zan(j)
  end do
  
  write(21,*)' '              
  write(21,*)'"time = ',n*dt  
  write(22,*)' '              
  write(22,*)'"time = ',n*dt  
  do j=js,jmax,djoutput             
     write(21,200)r(j),k(j)         
     write(22,200)r(j),chi(j)       
  end do
  
  write(20,*)' '              
  write(20,*)'"time = ',n*dt  
  do j=ji,jmax,djoutput
     write(20,200)r(j),0.5d0*z(j)
  end do

200 format(2e20.12)
  
  return
end subroutine polar_movie
      


      




 
