! *****************************************************************
! * T.O.V. radial pertrubations                                   *
! * this routines solves the  "wave" equation for the radial      *
! * (l=0) of a spherical TOV star                                 *
! *****************************************************************

subroutine radial(filetovsetup,filetovgrid,filetov,l,ji,jmin,ri1,ri2 &
     &,ri3,ri4,djoutput,tottime,nmovie,ncount,cf,methodev &
     &,initialflag,ri,sigma,pertamp,nnodes,fileradialinit,fileradialtime,fileradial &
     &,fileradialnorm,filez,eosflag,geos,keos,file_tab_eos)
  
  use nrtype
  use myutil
  use bckgrd
  use eos
  
  implicit none
  
  real(dp)                      :: ri1,ri2,ri3,ri4,dr,dr2            
  integer(i4b)                  :: jiequil,jmin,ji,jmax,jo1,jo2,jo3,jo4,djoutput,j
  real(dp)                      :: tottime,dt,dt2,cf,cf2,q
  integer(i4b)                  :: n,count,nt,ncount,nmovie
  real(dp)                      :: ri,sigma,nnodes,pertamp
  character(64)                 :: initialflag
  real(dp)                      :: mstar,drequil,rmin,rstar
  real(dp),pointer,dimension(:) :: zold(:),z(:),znew(:),wold(:),wnew(:),ztimeder(:)
  real(dp),pointer,dimension(:) :: r(:),r2(:)
  real(dp),pointer,dimension(:) :: p_o_w(:),q_o_w(:),dp_o_w(:)
  real(dp),pointer,dimension(:) :: m(:),p(:),dens(:),a(:),b(:),cs2(:),requil(:)
  real(dp),pointer,dimension(:) :: dpres(:),dcs2(:),ddens(:),rho(:),dpde(:),d2pde(:)
  character(64)                 :: filetovsetup,filetovgrid,filetov,filez
  character(64)                 :: fileradialinit,fileradialtime,fileradial,fileradialnorm
  real(dp)                      :: l
  character(64)                 :: methodev,eosflag,file_tab_eos
  real(dp)                      :: keos,geos
  real(dp),dimension(:),pointer :: lgntab,lgdtab,lgptab,d2lgdp,dydz1law
  real(dp)                      :: lgd,lgd1,lgd2 
  integer(i4b)                  :: ntab
  
  count=0     !initialization of the counter
  
  ! **************************************************************
  ! * 1. setting up the the grid and the potential               *
  ! **************************************************************
  
  call bckgrd_setup(filetovsetup,mstar,drequil,rmin,rstar,jiequil)
  
  dr=(rstar-rmin)/(ji-0.5d0)
  jmax=ji
  
  allocate(r(jmax),r2(jmax),requil(jiequil))
  
  !r(0)=rmin is a ghost ...
  r(1)=rmin+0.5d0*dr
  do j=1,jmax-1
     r(j+1)=r(j)+dr
  end do
  dr2=dr*dr
  r2=r*r
  
  ! allocate vars
  allocate(p_o_w(jmax),q_o_w(jmax),dp_o_w(jmax))
  allocate(m(jmax),p(jmax),dens(jmax),a(jmax),b(jmax),cs2(jmax))
  allocate(dpres(jmax),dcs2(jmax),ddens(jmax),rho(jmax),dpde(jmax),d2pde(jmax))
  
  if(methodev.eq.'leapfrog')then
     allocate(zold(jmax),z(jmax),znew(jmax),ztimeder(jmax))
  else
     write(*,*)' No other method implemented yet '
     return
  end if
  
  ! internal observers indexes
  jo1 = (ri1-r(jmin))/dr 
  jo2 = (ri2-r(jmin))/dr 
  jo3 = (ri3-r(jmin))/dr 
  jo4 = (ri4-r(jmin))/dr 

  ! potentials 
  call bckgrd_grid(filetovgrid,jiequil,requil)
  call bckgrd_vars(filetov,jiequil,requil,ji,jmax,r,m,p,dens,a,cs2)

  !---------------------------------------------      
  ![*] debug
  open(111,file="cs2.dat",status='unknown')
  do j=1,jmax
     write(111,112)r(j),dens(j),cs2(j),dens(j)*cs2(j)/p(j)+cs2(j)
  end do
  close(111)
112 format(6e20.12)
  !---------------------------------------------      
  
  b=-0.5*dlog(1.d0-2*m/r)
  
  ! equation coeffs
  
  p_o_w = cs2*dexp(2.d0*(a-b))
  
  q_o_w =  dexp(2d0*a)&
       & * ( (4*pi*p*r+m/r2)**2*dexp(2d0*b) &
       & + 4d0/r*(2d0*pi*p*r+m/r2) )
  
  !----------------------------------------------------------------             
  !dp_o_w = -dexp(2d0*a)*( (4d0*pi*p*r+m/r2)& 
  !     & * (1d0-Cs2+(Geos-1d0)/dens*(dens+p))&
  !     & + 2d0*Cs2*(dexp(-2d0*b)/r&
  !     & - 2d0*pi*(p+dens)*r) ) 
  !----------------------------------------------------------------             
  ! Here above the dp_o_w coef is computed using analytic dervts 
  ! for poly EoS , the following is the the general expression 
  ! using the EoS dependent derivatives:
  !
  ! ddens = d dens / dr = d dens / * dp dp / dr
  ! and 
  ! dcs2 = d cs2 / dr
  !----------------------------------------------------------------           
  
  dpres=-(dens+p)*(m+4.d0*pi*r**3*p)/(r*(r-2.d0*m))
  
  if(eosflag.eq.'poly')then
     
     ddens=dens*dpres/(geos*p)
     dcs2=(geos-1)*dpres/dens

     !try also the numeric: 
     !call diff2nd(cs2,dcs2,r,jmax)     
     !call diff4th(cs2,dcs2,r,jmax)     
     
  else if(eosflag.eq.'rhopoly')then
     
     rho=(p/keos)**(1/geos)
     ddens=dpres*( rho*(geos-1) + geos*p )/( geos*(geos-1)*p )
     dcs2=(geos-1)*dpres*rho*(geos**2 - 2.d0*geos + 1) &
          & /( (rho*(geos-1) + geos*p)**2  )
     
     !try also the numeric:
     !call diff2nd(cs2,dcs2,r,jmax)     
     !call diff4th(cs2,dcs2,r,jmax)     
     
  else if(eosflag.eq.'tab') then 
     
     call fileos(file_tab_eos,ntab,lgntab,lgdtab,lgptab,d2lgdp,dydz1law)
     
     do j=1,jmax
        
        !--------------------------------------------- 
        ! -> Compute d2pde2 from interpolation
        !---------------------------------------------        
        call linint_der(lgptab,lgdtab,ntab,dlog10(p(j)),lgd,lgd1,lgd2)
        !-----
        !call splint_der(lgptab,lgdtab,d2lgdp,ntab,dlog10(p(j)),lgd,lgd1,lgd2)
        !---------------------------------------------        
        dens(j)=10.d0**lgd
        dpde(j)=p(j)/dens(j)/lgd1
        d2pde(j)=dpde(j)/dens(j)/lgd1 - p(j)/lgd1/(dens(j)**2) &
             & - dpde(j)*lgd2/dens(j)/dlog(10.d0)/lgd1

     end do
     
     !--------------------------------------------- 
     ! -> Ccompute dcs2 = d^2p/de^2 * de/dr
     !---------------------------------------------        
     ddens=dpres/dpde
     dcs2=ddens*d2pde
         
  end if
  
  dp_o_w=dexp(2.*a) * ( 12.d0*cs2*p*pi*r**3*dens &
       & + 8.d0*r**3*cs2*pi*p**2 + 4.d0*cs2*dens**2*pi*r**3 &
       & + cs2*r**2*ddens + dcs2*r**2*dens + dcs2*r**2*p &
       & - 2.d0*dcs2*r*p*m - 2.d0*dcs2*r*dens*m - 2.d0*cs2*p*r &
       & - 2.d0*cs2*r*ddens*m - 2.d0*cs2*dens*r + 3.d0*cs2*dens*m &
       & + 2.d0*cs2*p*m + 2.d0*cs2*dens*m + 3.d0*cs2*p*m )&
       & /r**2/(dens+p)
  
  ! time evolution settings
  dt      = cf*dr
  dt2     = dt*dt
  cf2     = cf*cf
  nt      = nint(tottime/dt)
  q       = (cf-1.d0)/(cf+1.d0)
  
  ! screen info
  write(*,*)'  '
  write(*,*)' Settings : '
  write(*,*)'  '
  write(*,*)' l           = ',l
  write(*,*)' cf          = ',cf
  write(*,*)' total time  = ',tottime
  write(*,*)' nt          = ',nt
  write(*,*)' ncount      = ',ncount
  write(*,*)' dt          = ',dt
  write(*,*)' movie time  = ',tottime
  write(*,*)' nmovie      = ',nmovie
  write(*,*)'  '
  write(*,*)' TOV equilibrium radial grid :'
  write(*,*)'  '
  write(*,*)' dr_equil    = ',drequil
  write(*,*)' jiequil     = ',jiequil
  write(*,*)'  '
  write(*,*)' Radial grid to evolve perturbations :'
  write(*,*)'  '
  write(*,*)' jmin        = ',jmin
  write(*,*)' ji          = ',ji
  write(*,*)' je          = ',jmax-ji
  write(*,*)' jmax        = ',jmax
  write(*,*)' jo1         = ',jo1
  write(*,*)' jo2         = ',jo2
  write(*,*)' jo3         = ',jo3
  write(*,*)' jo4         = ',jo4
  write(*,*)' djoutput    = ',djoutput
  write(*,*)' dr          = ',dr
  write(*,*)' r(1)        = ',r(1)
  write(*,*)' r(jmin)     = ',r(jmin)
  write(*,*)' r(ji)       = ',r(ji)
  write(*,*)' r(jmax)     = ',r(jmax)
  write(*,*)' r(jo1)      = ',r(jo1)
  write(*,*)' r(jo2)      = ',r(jo2)
  write(*,*)' r(jo3)      = ',r(jo3)
  write(*,*)' r(jo4)      = ',r(jo4)
  write(*,*)'  '
  if(methodev.eq.'leapfrog')then
     write(*,*)' Evolution scheme : leapfrog '
  else
     write(*,*)' This evolution method is not available '
     return
  end if
  write(*,*)'  '
   
  ! **************************************************************
  ! * 2. initial conditions                                      *
  ! **************************************************************
   
  if(methodev.eq.'leapfrog')then
     ! * leapfrog
     call radial_init(zold,z,dt,r,dr,ri,sigma,nnodes,pertamp, &
          & l,initialflag,p_o_w,jmin,jmax,fileradialinit,methodev,filez)
  else
     write(*,*)' No other method implemented yet '
     return
  end if
   
  ! **************************************************************
  ! * 3. open all the output files                               *
  ! **************************************************************
  
  open(70,file   = fileradialtime ,status='unknown')
  open(75,file   = fileradial     ,status='unknown')
  open(80,file   = fileradialnorm ,status='unknown')
  
  ! **************************************************************
  ! * 4. time evolution                                          *
  ! **************************************************************
  
  write(*,*)' Time evolution :'
  write(*,*)' '
  write(*,*)'         iter       of         '
  write(*,*)' ***************************************'
   
  do n=0,nt
    
     if(mod(n,100).eq.0)then
        write(*,*)n,nt
     end if
    
     if(methodev.eq.'leapfrog')then
       
        ! ***********************************************************
        ! * leapfrog                                                *
        ! ***********************************************************
        
        ! * reflection at r = 0
        znew(jmin) = 0.d0

        ! * evolution 
        do j=jmin+1,jmax-1
           
           znew(j) = 2.d0*z(j)-zold(j) &
                & + cf2*( p_o_w(j) * ( z(j+1)- 2.d0*z(j)+z(j-1) ) ) &
                & + dt2*( dp_o_w(j) * 0.5d0 * ( z(j+1)-z(j-1) )/dr &
                & + q_o_w(j)* z(j) )
           
        end do
        
        ! * evolution of the boundaries of the grid
        znew(jmax) = ( 4.d0*z(jmax-1)-z(jmax-2) )/3.d0         

        !* extrapolation:
        !   znew(jmax) = 3.d0*(znew(jmax-1)-znew(jmax-2)) + znew(jmax-3)
        
        ! time derivative
        ztimeder = 0.5d0*( znew-zold )/dt
       
     else
        write(*,*)' No other method implemented yet '
     end if
           
     ! ***********************************************************
     ! * 5. recycling the data for the next time-step            *
     ! ***********************************************************
     
     if(methodev.eq.'leapfrog')then
        ! * leapfrog
        zold = z
        z    = znew
     else
        write(*,*)' No other method implemented yet '
     end if
     ! round to 0
     do j=1,jmax
        if(dabs(zold(j)).lt.1.d-20)      zold(j)=0.d0
        if(dabs(ztimeder(j)).lt.1.d-20)  ztimeder(j)=0.d0
     end do

     ! ***********************************************************
     ! * 6. output                                               *
     ! ***********************************************************
     
     if(n.eq.(0+count*ncount))then
        call  radial_outtime(n,dt,zold,ztimeder,jo1,jo2,jo3,jo4,jmin,jmax,djoutput)
        count=count+1
     end if
     
     if(mod(n,nmovie).eq.0)then
        write(75,*)' '
        write(75,*)'"time = ',n*dt
        do j=jmin,jmax,djoutput
           write(75,200)r(j),zold(j)
        end do
     end if
      
  end do
  
  write(*,*)' *************************************** '
  
  ! **************************************************************
  ! * 9. close all the output files                              *
  ! **************************************************************

  close(70)
  close(75)
  close(80)

200 format(2e20.12)
  write(*,*)' '
  write(*,*)' datafiles : '
  write(*,*)' '
  write(*,*)' ',fileradialinit
  write(*,*)' ',fileradialtime
  write(*,*)' ',fileradial
  write(*,*)' ',fileradialnorm
   
  return
end subroutine radial


! ****************************************************************
! * Initial condition for the time evolution                     *
! ****************************************************************

subroutine radial_init(zold,z,dt,r,dr,ri,sigma,nnodes,pertamp,&
     & l,initialflag,crr,jmin,jmax,fileradialinit,methodev,filez)

  use nrtype

  implicit none  

  integer(i4b)  :: j,jmin,jmax 
  character(64) :: initialflag
  real(dp)      :: r(jmax),zold(jmax),z(jmax),dzdt(jmax),crr(jmax)
  real(dp)      :: l,omega,tau,ri,sigma,dt,dr,nnodes,pertamp
  real(dp)      :: s2,a,intgr
  character(64) :: fileradialinit,methodev,filez
     
  s2=sigma*sigma

  write(*,*)' Initial Condition :'
  write(*,*)' '
   
  if(initialflag.eq.'gauss')then
     
     ! **************************************************************
     ! * gaussian pulse                                             *
     ! **************************************************************
 
     write(*,*)' z := gaussian pulse'
     write(*,*)'    ri    = ',ri
     write(*,*)'    sigma = ',sigma
     write(*,*)' '
     ! compute amplitude
     intgr=0
     do j=jmin,jmax 
        intgr = intgr + dexp( -(ri-r(j))**(2.d0)/s2 )*dr*r(j)**2
     end do
     a =  1/intgr


     if(methodev.eq.'leapfrog')then
        ! leapfrog     
        zold = a*dexp( -(ri-r)**(2.d0)/s2 )                 ! z(r,0) 
        !!!!z    = zold + dt*sqrt(crr)*2.0d0*(ri-r)/s2*zold     ! ingoing condition: z,t = sqrt(crr)z,r
        z    = zold                                         ! time symmetric: z,t = 0
     else
        write(*,*)' No other method implemented yet '
     end if
     
     !!!!dzdt = sqrt(crr)*2.0d0*(ri-r)/s2*zold                  ! dz/dt(r,0) for ingoing id
     dzdt=0                                                     ! dz/dt(r,0) for time symmetric id
  
  else if(initialflag.eq.'sin')then

     ! **************************************************************
     ! * sine                                                       *
     ! **************************************************************
 
     write(*,*)' z := a (r/R)^2 sin(pi n r/R)'
     write(*,*)'    a = ',pertamp
     write(*,*)'    n = ',nnodes
     write(*,*)' '
     
     if(methodev.eq.'leapfrog')then
        
        zold = pertamp*(r/r(jmax))**2 * dsin(pi*(nnodes+1.d0)*r/r(jmax)) !*1e-3
        z    = zold                     ! time symmetric: z,t = 0
        
        !z    = zold + dt* (dzold/dr)   ! ingoing condition: z,t = sqrt(crr)z,r

     else
        write(*,*)' No other method implemented yet '
     end if
     
     !dzdt = (dzolddr)                  ! dz/dt(r,0) for ingoing id
     dzdt=0                             ! dz/dt(r,0) for time symmetric id
     
    
  else if(initialflag.eq.'file')then
     
     ! **************************************************************
     ! * from file                                                  *       
     ! **************************************************************
     
     write(*,*)' z := from file ',filez
     write(*,*)' '
     
     open(27, file = filez  ,status = 'old')
     do j=1,jmax
        read(27,*) r(j),zold(j)
     end do
     close(27)
     
     if(methodev.eq.'leapfrog')then
        z=zold            ! time symmetric: z,t = 0
     else 
        write(*,*)' No other method implemented yet '
     end if
     
  end if
  
  ! write on file
  open(40,file   = fileradialinit ,status='unknown')
  do j=jmin,jmax
     write(40,4000)r(j),zold(j),dzdt(j)
  end do
  close(40)

4000 format(3f20.12)
  
  return
end subroutine radial_init


! ****************************************************************
! * Output of the data                                           *
! ****************************************************************
subroutine radial_outtime(n,dt,z,dz,jo1,jo2,jo3,jo4,jmin,jmax,djoutput)
  use nrtype  
  implicit none  
  integer(i4b) :: n,j,jo1,jo2,jo3,jo4,jmin,jmax,djoutput
  real(dp)     :: z(jmax),dz(jmax),dt,normz     
     
  ! **************************************************************
  ! * computation of the square norms                            *
  ! **************************************************************
  normz =  0.d0
  do j=jmin,jmax,djoutput
     normz = normz + z(j)*z(j)
  end do
  normz = normz/jmax

  ! **************************************************************
  ! * printout of data in time                                   *
  ! **************************************************************
  write(70,210)n*dt,z(jo1),z(jo2),z(jo3),z(jo4),dz(jo1),dz(jo2),dz(jo3),dz(jo4)
  write(80,310)n*dt,normz
   
210 format(9e20.12)
310 format(2e20.12)
   
  return
end subroutine radial_outtime



