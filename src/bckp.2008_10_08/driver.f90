! *****************************************************************
! *                                                               *
! *             T O V  p u l s a t i o n s  C o d e               * 
! *                                                               *
! *****************************************************************

program driver

  use nrtype

  implicit none
   
  ! ***************************************************************
  ! * variables                                                   *
  ! ***************************************************************

  ! ----------------------> equilirium vars 

  real(dp)      :: presc,presatm,mstar,rstar,rmax,dr,r1,r2,l 

  ! ----------------------> grid and time 

  real(dp)      :: rmin,rextmax,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4,drevolv 
  integer(i4b)  :: ji,jmin,djoutput,j 
  real(dp)      :: tottime,cf    
  integer(i4b)  :: ncount,nmovie

  ! ----------------------> equation of state 

  character(64) :: eosflag,file_tab_eos
  real(dp)      :: keos,geos 

  ! ----------------------> initial data 

  character(64) :: initialflaga,initialflagp,initialflagr
  real(dp)      :: rir,sigmar,ria,sigmaa,rip,sigmap,nnodes_rad
  real(dp)      :: nnodes_polar,beta,pertamp_polar,pertamp_rad  

  ! ----------------------> flags 

  character(64) :: datadir,pert,methodeva,methodevpChi,methodevpZ,methodevr
  character(64) :: gets,getz,flat,readfh

  ! ----------------------> out files 

  character(64) :: filetovsetup,filetovgrid,filetov 
  character(64) :: fileradialinit,filexi,fileradialtime
  character(64) :: fileradial,fileradialnorm
  character(64) :: fileaxialinit,filez,fileaxialtime,fileaxial,fileaxialnorm
  character(64) :: fileh,filepolartimez,filepolartimek,filepolartimechi
  character(64) :: filepolartimeh,filepolartimezan
  character(64) :: filepolarz,filepolark,filepolarchi,filepolarh,filepolarzan
  character(64) :: filepolarnormz,filepolarnormk,filepolarnormchi,filepolarinit

  ! ----------------------> dummies 

  character(64) :: dummyc
  logical(lgt),dimension(3) :: check

  ! ----------------------> input file

  character(64)  :: parfile
  integer(i4b)   :: argc,n
  character(128) :: buffer

  write(*,*)' '
  write(*,*)' ****************************************************************'
  write(*,*)' *                                                              *'
  write(*,*)' *            T O V   p u l s a t i o n s   C o d e             *'
  write(*,*)' *                                                              *'
  write(*,*)' ****************************************************************'
  write(*,*)' '

  ! ***************************************************************
  ! * read parameter file                                         *
  ! ***************************************************************
  
  argc = IARGC() 
  if (argc /= 1) then
     write(*,*)' Usage: PerturbTOVCode.exe <parfile>'
     stop
  end if
  call getarg(1,buffer)
  read(buffer, *) parfile
  write(*,*)' Parameter file :',parfile

  ! ***************************************************************
  ! * read parameter file                                         *
  ! ***************************************************************

  write(*,*)' Reading parameters'
  open(100,file=parfile,status='old') 
  !----------------------------------------------------------------------------
  ! parameters used to build  equilibrium model
  !----------------------------------------------------------------------------
  read(100,*)dummyc,presc                  ! central pressure  
  read(100,*)dummyc,presatm                ! pressure atmosphere
  read(100,*)dummyc,r1                     ! 1st grid point
  read(100,*)dummyc,r2                     ! 2nd grid point
  read(100,*)dummyc,dr                     ! grid spacing
  read(100,*)dummyc,rmax                   ! max star radius
  read(100,*)dummyc,eosflag                ! flag for the eos
  read(100,*)dummyc,keos                   ! polytropic eos k
  read(100,*)dummyc,geos                   ! polytropic eos g
  read(100,*)dummyc,file_tab_eos           ! file tabulated eos
  read(100,*)dummyc,mstar                  ! constant density eos m
  read(100,*)dummyc,rstar                  ! constant density eos r
  !----------------------------------------------------------------------------
  ! parameters used to computed perturbations
  !----------------------------------------------------------------------------
  read(100,*)dummyc,pert        ! perturbation type
  read(100,*)dummyc,l           ! l-index of perturbation
  read(100,*)dummyc,rextmax     ! maximum external radius
  read(100,*)dummyc,jmin        ! min radius index
  read(100,*)dummyc,ji          ! internal pts of the star
  read(100,*)dummyc,ri1  ! output point, internal observer no1 (make here fft)
  read(100,*)dummyc,ri2  ! output point, internal observer no2       "
  read(100,*)dummyc,ri3  ! output point, internal observer no3       "
  read(100,*)dummyc,ri4  ! output point, internal observer no4       "
  read(100,*)dummyc,ro1  ! output point, external observer no1       "
  read(100,*)dummyc,ro2  ! output point, external observer no2       "
  read(100,*)dummyc,ro3  ! output point, external observer no3       "
  read(100,*)dummyc,ro4  ! output point, external observer no4       "
  read(100,*)dummyc,tottime     ! total evolution time 
  read(100,*)dummyc,cf          ! courant factor  
  read(100,*)dummyc,djoutput    ! take movies every 'djoutput' radial points
  read(100,*)dummyc,nmovie      ! counter for have the movie
  read(100,*)dummyc,ncount      ! counter for time output
  !----------------------------------------------------------------------------
  ! radial
  !----------------------------------------------------------------------------
  read(100,*)dummyc,methodevr   ! wave solver for radial perturbations
  read(100,*)dummyc,initialflagr ! flag for the initial condition of radial perturbations
  read(100,*)dummyc,rir         ! center of the gaussian pulse
  read(100,*)dummyc,sigmar      ! amplitude of the gaussian pulse
  read(100,*)dummyc,pertamp_rad ! amplitude of the fluid perturbation
  read(100,*)dummyc,nnodes_rad  ! amplitude of the fluid perturbation
  read(100,*)dummyc,filexi      ! filename for initial xsi
  !----------------------------------------------------------------------------
  ! axial
  !----------------------------------------------------------------------------
  read(100,*)dummyc,methodeva   ! wave solver for axial perturbations
  read(100,*)dummyc,initialflaga  ! flag for the initial condtion of axial perturbations
  read(100,*)dummyc,ria         ! center of the gaussian pulse 
  read(100,*)dummyc,sigmaa      ! amplitude of the gaussian pulse 
  read(100,*)dummyc,filez       ! filename for initial axial z 
  !----------------------------------------------------------------------------
  ! polar
  !----------------------------------------------------------------------------
  read(100,*)dummyc,methodevpChi   ! wave solver for Chi eq polar perturbations
  read(100,*)dummyc,methodevpZ     ! wave solver for Z eq polar perturbations
  read(100,*)dummyc,initialflagp  ! flag for the initial condtion of polar perturbations
  read(100,*)dummyc,gets        ! how to compute s [dsdr,dsdt]
  read(100,*)dummyc,getz        ! hot to compute z [dzdr,dzdt] 
  read(100,*)dummyc,readfh      ! read file for initial H [yes/no]
  read(100,*)dummyc,fileh       ! filename for initial h 
  read(100,*)dummyc,nnodes_polar  ! number of initial nodes in H
  read(100,*)dummyc,pertamp_polar ! amplitude of the polar fluid perturbation
  read(100,*)dummyc,beta          ! amount of non-conformal flatness
  read(100,*)dummyc,rip           ! center of the gaussian pulse
  read(100,*)dummyc,sigmap        ! amplitude of the gaussian pulse
  !----------------------------------------------------------------------------
  read(100,*)dummyc,flat        ! use flat metric in axial and polar [yes/no]
  read(100,*)dummyc,datadir     ! output data directory 
  !----------------------------------------------------------------------------
  close(100)

  ! ***************************************************************
  ! * data files                                                  *
  ! ***************************************************************
  
  filetovsetup     = datadir(1:index(datadir,' ')-1)//'/tovequil_setup.dat'
  filetovgrid      = datadir(1:index(datadir,' ')-1)//'/tovequil_grid.dat'
  filetov          = datadir(1:index(datadir,' ')-1)//'/tovequil.dat'
  fileradialinit   = datadir(1:index(datadir,' ')-1)//'/radial_init.dat'
  fileradialtime   = datadir(1:index(datadir,' ')-1)//'/radial_xi_atobs.dat'
  fileradial       = datadir(1:index(datadir,' ')-1)//'/radial_xi.yg' 
  fileradialnorm   = datadir(1:index(datadir,' ')-1)//'/radial_xi_norm.dat'
  fileaxialinit    = datadir(1:index(datadir,' ')-1)//'/axial_init.dat'
  fileaxialtime    = datadir(1:index(datadir,' ')-1)//'/axial_z_atobs.dat'
  fileaxial        = datadir(1:index(datadir,' ')-1)//'/axial_z.yg' 
  fileaxialnorm    = datadir(1:index(datadir,' ')-1)//'/axial_z_norm.dat'
  filepolartimez   = datadir(1:index(datadir,' ')-1)//'/polar_z_atobs.dat'
  filepolartimek   = datadir(1:index(datadir,' ')-1)//'/polar_k_atobs.dat'
  filepolartimechi = datadir(1:index(datadir,' ')-1)//'/polar_chi_atobs.dat'
  filepolartimeh   = datadir(1:index(datadir,' ')-1)//'/polar_h_atobs.dat'
  filepolartimezan = datadir(1:index(datadir,' ')-1)//'/polar_zan_atobs.dat'
  filepolarz       = datadir(1:index(datadir,' ')-1)//'/polar_z.yg'
  filepolark       = datadir(1:index(datadir,' ')-1)//'/polar_k.yg'
  filepolarchi     = datadir(1:index(datadir,' ')-1)//'/polar_chi.yg'
  filepolarh       = datadir(1:index(datadir,' ')-1)//'/polar_h.yg'
  filepolarzan     = datadir(1:index(datadir,' ')-1)//'/polar_zan.yg'
  filepolarnormz   = datadir(1:index(datadir,' ')-1)//'/polar_z_norm.dat'
  filepolarnormk   = datadir(1:index(datadir,' ')-1)//'/polar_k_norm.dat'
  filepolarnormchi = datadir(1:index(datadir,' ')-1)//'/polar_chi_norm.dat'
  filepolarinit    = datadir(1:index(datadir,' ')-1)//'/polar_init.dat'

  ! ***************************************************************
  ! * EoS info                                                    *
  ! ***************************************************************

  write(*,*)' '  
  write(*,*)' EoS : '
  if(eosflag.eq.'constd') then
     write(*,*)' constant density  '
     write(*,*)'                  m = ',mstar
     write(*,*)'                  r = ',rstar
  else if(eosflag.eq.'poly')then
     write(*,*)' polytropic '
     write(*,*)'                  k = ',keos
     write(*,*)'              gamma = ',geos
  else if(eosflag.eq.'rhopoly') then
     write(*,*)' rho-polytropic  '
     write(*,*)'                  k = ',keos
     write(*,*)'              gamma = ',geos
  else if(eosflag.eq.'tab') then
     write(*,*)' tabulated '
     write(*,*)' file : ',file_tab_eos
     inquire(file=file_tab_eos,exist=check(1))
     if( check(1).eq..true. ) then  
        write(*,*)' check for eos file : ok'
     else
        write(*,*)' !!! EoS file does not exist !!! '
        write(*,*)' ...aborting  run ... '
        stop
     end if
  else
     write(*,*)' !!! unknown EoS name in par file !!! '
     write(*,*)' ...aborting  run ... '
     stop
  end if
  
  ! ***************************************************************
  ! * start                                                       *
  ! ***************************************************************

  write(*,*)' '  
  write(*,*)' Starting computation ...'
  write(*,*)' '
  
  if(pert.eq.'no')then
     
     ! ***************************************************************
     ! * Compute TOV equilibrium model                               *
     ! ***************************************************************

     write(*,*)' ***************************************'
     write(*,*)' *                                     *'
     write(*,*)' * ====> T.O.V. equilibrium model      *'
     write(*,*)' *                                     *'
     write(*,*)' ***************************************'
     write(*,*)' '     
     if (eosflag.eq.'constd') then
        call constdens_equil(mstar,r2,dr,rstar,filetovsetup,&
             & filetovgrid,filetov)
     else
        call equil(presc,presatm,r1,r2,dr,rmax,eosflag,keos,geos,&
             & filetovsetup,filetovgrid,filetov,file_tab_eos)  
     end if

  else

     ! ***************************************************************
     ! * Compute  Perturbations                                      *
     ! ***************************************************************

     ! * check for the equilibrium model *

     inquire(file=filetovsetup,exist=check(1))
     inquire(file=filetovgrid,exist=check(2))
     inquire(file=filetov,exist=check(3))
     if( all(check).eq..true. ) then  
        write(*,*)' checking for the equilibrium model : ok'
     else
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> T.O.V. equilibrium model      *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'
        write(*,*)' '     
        if (eosflag.eq.'constd') then
           call constdens_equil(mstar,r2,dr,rstar,filetovsetup,&
                & filetovgrid,filetov)
        else
           call equil(presc,presatm,r1,r2,dr,rmax,eosflag,keos,geos,&
                & filetovsetup,filetovgrid,filetov,file_tab_eos)  
        end if
     end if
     
     if(pert.eq.'radial')then
        
        ! * radial (l=0) *     
        
        write(*,*)' '
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> Radial pulsations             *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'
         
        call radial(filetovsetup,filetovgrid,filetov, &
             & l,ji,jmin,ri1,ri2,ri3,ri4,djoutput,&
             & tottime,nmovie,ncount,cf,methodevr,initialflagr,&
             & rir,sigmar,pertamp_rad,nnodes_rad, &
             & fileradialinit,fileradialtime,fileradial,fileradialnorm,&
             & filexi,eosflag,geos,keos,file_tab_eos)
        
     else if(pert.eq.'axial')then
        
        ! * axial (odd-parity) *
        
        write(*,*)' '
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> Axial pulsations              *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'
         
        call axial(filetovsetup,filetovgrid,filetov, &
             & l,rextmax,ji,jmin,ro1,ro2,ro3,ro4,djoutput, &
             & tottime,nmovie,ncount,cf,methodeva,initialflaga,flat,&
             & ria,sigmaa,fileaxialinit,fileaxialtime,fileaxial,&
             & fileaxialnorm,filez)
        
     else if(pert.eq.'polar')then
        if (eosflag.eq.'constd') then
           stop ' constant density eos is not available for polar perturbations. '
        end if
        
        ! * polar (even-parity) *
        
        write(*,*)' '
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> Polar pulsations              *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'
         
        call polar(filetovsetup,filetovgrid,filetov,keos,geos,fileh,&
             & l,jmin,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4,djoutput,rextmax,ji, &
             & tottime,nmovie,ncount,cf,methodevpChi,methodevpZ, &
             & initialflagp,rip,sigmap,gets,getz,beta,readfh,nnodes_polar,&
             & pertamp_polar,flat,&
             & filepolartimez,filepolartimek, & 
             & filepolartimechi,filepolartimeh,filepolartimezan, &
             & filepolarz,filepolark,filepolarchi,filepolarzan,filepolarh,& 
             & filepolarnormz,filepolarnormk,filepolarnormchi,filepolarinit,&
             & eosflag)
         
     else if(pert.eq.'all')then
        if (eosflag.eq.'constd') then
           stop ' constant density eos is not available for polar perturbations. '
        end if
        
        ! Do all evolutions ....
        
        ! * radial (l=0) *     
        
        write(*,*)' '
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> Radial pulsations             *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'
        
        call radial(filetovsetup,filetovgrid,filetov, &
             & l,ji,jmin,ri1,ri2,ri3,ri4,djoutput,&
             & tottime,nmovie,ncount,cf,methodevr,initialflagr,&
             & rir,sigmar,pertamp_rad,nnodes_rad, &
             & fileradialinit,fileradialtime,fileradial,fileradialnorm,&
             & filexi,eosflag,geos,keos,file_tab_eos)
        
        ! * axial (odd-parity) *
        
        write(*,*)' '
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> Axial pulsations              *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'

        call axial(filetovsetup,filetovgrid,filetov, &
             & l,rextmax,ji,jmin,ro1,ro2,ro3,ro4,djoutput, &
             & tottime,nmovie,ncount,cf,methodeva,initialflaga,flat,&
             & ria,sigmaa,fileaxialinit,fileaxialtime,fileaxial,&
             & fileaxialnorm,filez)
        
        ! * polar (even-parity) *
        
        write(*,*)' '
        write(*,*)' ***************************************'
        write(*,*)' *                                     *'
        write(*,*)' * ====> Polar pulsations              *'
        write(*,*)' *                                     *'
        write(*,*)' ***************************************'
         
        call polar(filetovsetup,filetovgrid,filetov,keos,geos,fileh,&
             & l,jmin,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4,djoutput,rextmax,ji, &
             & tottime,nmovie,ncount,cf,methodevpChi,methodevpZ, &
             & initialflagp,rip,sigmap,gets,getz,beta,readfh,nnodes_polar,&
             & pertamp_polar,flat,&
             & filepolartimez,filepolartimek, & 
             & filepolartimechi,filepolartimeh,filepolartimezan, &
             & filepolarz,filepolark,filepolarchi,filepolarzan,filepolarh,& 
             & filepolarnormz,filepolarnormk,filepolarnormchi,filepolarinit, &
             & eosflag)
         
     end if
  end if
   
  write(*,*)' '
  write(*,*)' ****************************************************************'
  write(*,*)' *                           E N D                              *'
  write(*,*)' ****************************************************************'
  write(*,*)' '
  
  stop
end program driver
