! *****************************************************************
! * T.O.V. axial pertrubations                                    *
! * this routines solves the  "wave" equation for the axial       *
! * (odd-parity) of a spherical TOV star                          *
! *****************************************************************

subroutine axial(filetovsetup,filetovgrid,filetov, &
     & l,rextmax,ji,jmin,ro1,ro2,ro3,ro4,djoutput,&
     & tottime,nmovie,ncount,cf,methodev,initialflag,flat,ri,sigma, &
     & fileaxialinit,fileaxialtime,fileaxial,fileaxialnorm,filez)
  
  use nrtype
  use nrutil,      only : nrerror,dreallocate
  use myutil,      only : diff2nd
  use bckgrd,      only : bckgrd_setup
  
  implicit none
  
  real(dp)                      :: rextmax,ro1,ro2,ro3,ro4,dr,dr2            
  integer(i4b)                  :: jiequil,jmin,ji,jmax,jo1,jo2,jo3,jo4,djoutput,j
  real(dp)                      :: tottime,dt,dt2,cf,cf2,q
  integer(i4b)                  :: n,count,nt,ncount,nmovie
  real(dp)                      :: ri,sigma
  real(dp)                      :: mstar,drequil,rmin,rstar
  real(dp),pointer,dimension(:) :: zold(:),z(:),znew(:),wold(:),wnew(:),ztimeder(:)
  real(dp),pointer,dimension(:) :: r(:),c(:),cr(:),crr(:)
  character(64)                 :: filetovsetup,filetovgrid,filetov,filez
  character(64)                 :: fileaxialinit,fileaxialtime,fileaxial,fileaxialnorm
  character(64)                 :: initialflag
  real(dp)                      :: l
  character(64)                 :: methodev,flat 
   
  count=0     !initialization of the counter
  
  ! **************************************************************
  ! * 1. setting up the the grid and the potential               *
  ! **************************************************************

  call bckgrd_setup(filetovsetup,mstar,drequil,rmin,rstar,jiequil)
  dr=(rstar-rmin)/(ji-0.5d0)
  jmax=ji+((rextmax-rstar)/dr+1)
  
  allocate(r(jmax))
  
  !r(0)=rmin is a ghost ...
  r(1)=rmin+0.5d0*dr
  do j=1,jmax-1
     r(j+1)=r(j)+dr
  end do
  dr2=dr*dr
  
  ! allocate vars
  allocate(c(jmax),cr(jmax),crr(jmax))
  if(methodev.eq.'leapfrog')then
     allocate(zold(jmax),z(jmax),znew(jmax),ztimeder(jmax))
  elseif(methodev.eq.'lw')then
     allocate(zold(jmax),znew(jmax),wold(jmax),wnew(jmax),ztimeder(jmax))
  end if
  
  ! external observers indexes
  jo1 = (ro1-r(jmin))/dr
  jo2 = (ro2-r(jmin))/dr
  jo3 = (ro3-r(jmin))/dr
  jo4 = (ro4-r(jmin))/dr
  
  ! potentials and coeffs
  call axial_potential(jmin,jiequil,ji,jmax,r,mstar,dr,l,filetov,filetovgrid,flat,c,cr,crr)
  
  ! time evolution settings
  dt      = cf*dr
  dt2     = dt*dt
  cf2     = cf*cf
  nt      = nint(tottime/dt)
  q       = (cf-1.d0)/(cf+1.d0)
  
  ! screen info
  write(*,*)'  '
  write(*,*)' Settings : '
  write(*,*)'  '
  write(*,*)' l           = ',l
  write(*,*)' cf          = ',cf
  write(*,*)' total time  = ',tottime
  write(*,*)' nt          = ',nt
  write(*,*)' ncount      = ',ncount
  write(*,*)' dt          = ',dt
  write(*,*)' movie time  = ',tottime
  write(*,*)' nmovie      = ',nmovie
  write(*,*)'  '
  write(*,*)' TOV equilibrium radial grid :'
  write(*,*)'  '
  write(*,*)' dr_equil    = ',drequil
  write(*,*)' jiequil     = ',jiequil
  write(*,*)'  '
  write(*,*)' Radial grid to evolve perturbations :'
  write(*,*)'  '
  write(*,*)' jmin        = ',jmin
  write(*,*)' ji          = ',ji
  write(*,*)' je          = ',jmax-ji
  write(*,*)' jmax        = ',jmax
  write(*,*)' jo2         = ',jo1
  write(*,*)' jo2         = ',jo2
  write(*,*)' jo3         = ',jo3
  write(*,*)' jo4         = ',jo4
  write(*,*)' djoutput    = ',djoutput
  write(*,*)' dr          = ',dr
  write(*,*)' r(1)        = ',r(1)
  write(*,*)' r(jmin)     = ',r(jmin)
  write(*,*)' r(ji)       = ',r(ji)
  write(*,*)' r(jmax)     = ',r(jmax)
  write(*,*)' r(jo1)      = ',r(jo1)
  write(*,*)' r(jo2)      = ',r(jo2)
  write(*,*)' r(jo3)      = ',r(jo3)
  write(*,*)' r(jo4)      = ',r(jo4)
  write(*,*)'  '
  if(methodev.eq.'leapfrog')then
     write(*,*)' Evolution scheme : leapfrog '
  elseif(methodev.eq.'lw')then
     write(*,*)' Evolution scheme : lax-wendroff '
  else
     write(*,*)' This evolution method is not available '
     return
  end if
  write(*,*)'  '

  ! **************************************************************
  ! * 2. initial conditions                                      *
  ! **************************************************************

  if(methodev.eq.'leapfrog')then
     ! * leapfrog
     call axial_init(zold,z,dt,r,dr,ri,sigma,l,initialflag,crr,jmin,jmax,fileaxialinit,methodev,filez)
  elseif(methodev.eq.'lw')then
     ! * lax-wendroff
     call axial_init(zold,wold,dt,r,dr,ri,sigma,l,initialflag,crr,jmin,jmax,fileaxialinit,methodev,filez)
  end if

  ! **************************************************************
  ! * 3. open all the output files                               *
  ! **************************************************************

  open(70,file   = fileaxialtime ,status='unknown')
  open(75,file   = fileaxial     ,status='unknown')
  open(80,file   = fileaxialnorm ,status='unknown')

  ! **************************************************************
  ! * 4. time evolution                                          *
  ! **************************************************************

  write(*,*)' Time evolution :'
  write(*,*)' '
  write(*,*)'         iter       of         '
  write(*,*)' ***************************************'
      
  do n=0,nt
     if(mod(n,100).eq.0)then
        write(*,*)n,nt
     end if
     
     if(methodev.eq.'leapfrog')then
   
        ! ***********************************************************
        ! * leapfrog                                                *
        ! ***********************************************************
        
        ! * reflection at r = 0
        znew(jmin) = 0.d0
        ! * evolution (interior+exterior)
        do j=jmin+1,jmax-1
           znew(j) = 2.d0*z(j)-zold(j) + cf2*( &
                &                crr(j)*( z(j+1)- 2.d0*z(j)+z(j-1) ) &
                &                + dr*0.5d0*cr(j)*( z(j+1)-z(j-1) ) &
                &                + dr2*c(j)*z(j) &
                &                )
        end do
        ! * evolution of the boundaries of the grid
        znew(jmax) = z(jmax-1) + znew(jmax-1)*q - z(jmax)*q
        ! time derivative
        ztimeder = 0.5d0*( znew-zold )/dt

     elseif(methodev.eq.'lw')then
        
        ! ***********************************************************
        ! * lax-wendroff                                            *
        ! ***********************************************************
        
        ! * reflection at r = 0
        znew(jmin)    = 0.d0
        wnew(jmin)    = 0.d0
        ! * evolution (interior+exterior)
        do j=jmin+1,jmax-1
           znew(j) = zold(j) + 0.5d0*cf*dsqrt(crr(j))*(zold(j+1)-zold(j-1) ) &
                & + 0.5d0*cf2*crr(j)*( zold(j+1)- 2.d0*zold(j)+zold(j-1) ) &
                & + dt*wold(j)
            
           wnew(j) = wold(j) - 0.5d0*cf*dsqrt(crr(j))*(wold(j+1)-wold(j-1) ) &
                & + 0.5d0*cf2*crr(j)*( wold(j+1)- 2.d0*wold(j)+wold(j-1) ) &
                & + dt*c(j)*zold(j)
        end do
        ! * evolution of the boundaries of the grid
        znew(jmax) = zold(jmax-1) + znew(jmax-1)*q - zold(jmax)*q
        wnew(jmax) = wold(jmax-1) + wnew(jmax-1)*q - wold(jmax)*q
        ! time derivative
        call diff2nd(znew,ztimeder,r,jmax)
        ztimeder = wnew + sqrt(crr)*ztimeder
     end if

     ! ***********************************************************
     ! * 5. recycling the data for the next time-step            *
     ! ***********************************************************

     if(methodev.eq.'leapfrog')then
        ! * leapfrog
        zold = z
        z    = znew
     elseif(methodev.eq.'lw')then
        ! * lax-wendroff
        zold = znew
        wold = wnew
     end if
     ! round to 0
     do j=1,jmax
        if(dabs(zold(j)).lt.1.d-20)      zold(j)=0.d0
        if(dabs(ztimeder(j)).lt.1.d-20)  ztimeder(j)=0.d0
     end do

     ! ***********************************************************
     ! * 6. output                                               *
     ! ***********************************************************

     if(n.eq.(0+count*ncount))then
        call  axial_outtime(n,dt,zold,ztimeder,jo1,jo2,jo3,jo4,jmin,jmax,djoutput)
        count=count+1
     end if
     if(mod(n,nmovie).eq.0)then
        write(75,*)' '
        write(75,*)'"time = ',n*dt
        do j=jmin,jmax,djoutput
           write(75,200)r(j),zold(j)
        end do
     end if
  end do
   
  write(*,*)' *************************************** '

  ! **************************************************************
  ! * 9. close all the output files                              *
  ! **************************************************************

  close(70)
  close(75)
  close(80)
200 format(2e20.12)
  write(*,*)' '
  write(*,*)' datafiles : '
  write(*,*)' '
  write(*,*)' ',fileaxialinit
  write(*,*)' ',fileaxialtime
  write(*,*)' ',fileaxial
  write(*,*)' ',fileaxialnorm
   
   
  return
end subroutine axial

! *********************************************************************
! * Load background vars and build Regge-Wheeler potential and coeffs *
! *********************************************************************

subroutine axial_potential(jmin,jiequil,ji,jmax,r,mstar,dr,l,filetov,filetovgrid,flat,c,cr,crr)

  use nrtype 
  use bckgrd

  implicit none 

  integer(i4b)  :: jmin,jiequil,ji,jmax,j                  !indexes
  real(dp)      :: r(jmax),rd(jmax),rd2(jmax),rd3(jmax),dr !r-grid
  real(dp)      :: rho(ji),p(ji),m(ji),cs2(ji),a(ji)       !equilirium vars
  real(dp)      :: e2a(jmax),em2b(jmax),pmrho(jmax)        !shorthands
  real(dp)      :: requil(jiequil)            
  real(dp)      :: mstar
  character(64) :: filetov,filetovgrid
  real(dp)      :: c(jmax),cr(jmax),crr(jmax) !equation coefs
  real(dp)      :: l
  character(64) :: flat
     
  call bckgrd_grid(filetovgrid,jiequil,requil)
  call bckgrd_vars(filetov,jiequil,requil,ji,jmax,r,m,p,rho,a,cs2)
   
  if (flat.eq.'no')then
      
     do j=jmin,ji
         
        rd(j)    = 1.d0/r(j)
        rd2(j)   = rd(j)*rd(j)
        rd3(j)   = rd(j)*rd(j)*rd(j)
        e2a(j)   = dexp(2.d0*a(j))
        em2b(j)  = 1.d0-2.d0*m(j)*rd(j)
        pmrho(j) = p(j)-rho(j)
        crr(j)   = e2a(j) * em2b(j)
        cr(j)    = e2a(j) * ( 2.d0*m(j)*rd2(j) + 4.d0*pi*r(j)*(pmrho(j)) )
        c(j)     = e2a(j) * ( 6.d0*m(j)*rd3(j) + 4.d0*pi*(pmrho(j)) -l*(l+1d0)*rd2(j) ) !potential
         
     end do
      
     do j=ji+1,jmax
         
        rd(j)    = 1.d0/r(j)
        rd2(j)   = rd(j)*rd(j)
        rd3(j)   = rd(j)*rd(j)*rd(j)
        e2a(j)   = 1.d0-2.d0*mstar*rd(j)
        em2b(j)  = 1.d0-2.d0*mstar*rd(j)
        pmrho(j) = 0.d0
        crr(j)   = e2a(j) * em2b(j)
        cr(j)    = e2a(j) * ( 2.d0*mstar*rd2(j) + 4.d0*pi*r(j)*(pmrho(j)) )
        c(j)     = e2a(j) * ( 6.d0*mstar*rd3(j) - l*(l+1d0)*rd2(j) + 4.d0*pi*(pmrho(j)) ) 
         
     end do
      
  elseif(flat.eq.'yes')then   ! use flat metric :
      
     do j=jmin,jmax
         
        c(j)   = 0.d0
        cr(j)  = 0.d0
        crr(j) = 1.d0
         
     end do
      
  end if
     
  return
end subroutine axial_potential

! ****************************************************************
! * Output of the data                                           *
! ****************************************************************

subroutine axial_outtime(n,dt,z,dz,jo1,jo2,jo3,jo4,jmin,jmax,djoutput)

  use nrtype  

  implicit none  

  integer(i4b) :: n,j,jo1,jo2,jo3,jo4,jmin,jmax,djoutput
  real(dp)     :: z(jmax),dz(jmax),dt,normz     

  ! **************************************************************
  ! * computation of the square norms                            *
  ! **************************************************************

  normz =  0.d0
  do j=jmin,jmax,djoutput
     normz =  normz + z(j)*z(j)
  end do
  normz=normz/jmax

  ! **************************************************************
  ! * printout of data in time                                   *
  ! **************************************************************

  write(70,210)n*dt,z(jo1),z(jo2),z(jo3),z(jo4),dz(jo1),dz(jo2),dz(jo3),dz(jo4)
  write(80,310)n*dt,normz

210 format(9e20.12)
310 format(2e20.12)

  return
end subroutine axial_outtime

