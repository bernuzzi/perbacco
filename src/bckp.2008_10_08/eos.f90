! *****************************************************************
! * This module contains the equations of state routines          *
! *****************************************************************

module eos  
  
  use nrtype

  implicit none 
  
  interface polyeos
     module procedure polyeos
  end interface
  
  interface rhopolyeos
     module procedure rhopolyeos
  end interface
  
  interface fileos
     module procedure fileos
  end interface
  
  interface tabeos
     module procedure tabeos
  end interface
  
contains

  ! --------------------------------------------------------------- 
  ! polytropic eos :: 
  ! pres = k * dens^g
  ! ( here dens is the total energy density, do not confuse with mass density )
  ! --------------------------------------------------------------- 

  subroutine polyeos(dens,pres,dpddens,k,g)

    implicit none

    real(dp) :: pres
    real(dp) :: dens,dpddens
    real(dp) :: k,g

    dens = (pres/k)**(1.d0/g) 
    dpddens = g*pres/dens

  end subroutine polyeos
   
  ! --------------------------------------------------------------- 
  ! rho-polytropic eos ::
  ! pres = k * rho^g ; e = rho + p/(g-1)
  ! ( here rho is the mass density and dens is the total energy density)
  ! --------------------------------------------------------------- 

  subroutine rhopolyeos(dens,pres,dpddens,k,g)

    implicit none

    real(dp),intent(in)  :: pres
    real(dp),intent(out) :: dens,dpddens
    real(dp) :: k,g,rho

    rho     = (pres/k)**(1.d0/g)
    dens    = (pres/k)**(1.d0/g) + pres/(g-1)
    dpddens =  k*g*(g-1.d0)/(g-1.d0 + k*g*rho**(g-1.d0))*rho**(g-1.d0)

  end subroutine rhopolyeos
  
  ! --------------------------------------------------------------- 
  ! tabulated eos from TAB file
  ! 1 parameter eos in the form dens = dens(pres) 
  ! "fileos" : read tab file 
  ! "tabeos" : do interpolation
  ! ------------------------------------
  ! file "ftab" must be like:
  ! ------------------------------------
  ! ntab
  ! col1=n_B[cm^-3] col2=dens[g/cm^3] col3=pres[dyn/cm^2] 
  ! --------------------------------------------------------------- 

  subroutine fileos(ftab,ntab,lgnbtab,lgrhotab,lgprestab,d2lgdp,dlgrn)

    use nradp
    use const 

    implicit none
    
    character(64),intent(in) :: ftab
    real(dp),dimension(:),pointer,intent(inout) :: lgnbtab,lgrhotab,lgprestab,d2lgdp,dlgrn
    integer(i4b),intent(out) :: ntab
    real(dp) :: temp1,temp2,col1,col2,col3
    integer(i4b) :: j
    
    write(*,*)' Reading EoS file ... '
    
    open(31,file=ftab,status='old')
    
    read(31,200) ntab
    
    write(*,*)' tab eos entries = ',ntab
    
    allocate(lgnbtab(ntab),lgrhotab(ntab),lgprestab(ntab),d2lgdp(ntab),dlgrn(ntab))
    
    do j=1,ntab
       !--------------------------------------------
       ! n_B[cm^-3] dens[g/cm^3] col3=pres[dyn/cm^2] 
       !--------------------------------------------
       read(31,*) col1,col2,col3
       lgnbtab(j)=dlog10(col1*cctkvol_cgs)
       lgrhotab(j)=dlog10(col2/cctkmdens_cgs)
       lgprestab(j)=dlog10(col3/cctkpress_cgs)
    end do
    
    close(31)
    
    write(*,*)' ... done '    
    
    !--------------------------------------------
    ! Compute II derivative d2Log(rho)/dLog(p)^2 
    ! needed by spline
    !--------------------------------------------
    temp1=(lgrhotab(2)-lgrhotab(1))/(lgprestab(2)-lgprestab(1))
    temp2=(lgrhotab(ntab)-lgrhotab(ntab-1))/(lgprestab(ntab)-lgprestab(ntab-1))
    call spline(lgprestab,lgrhotab,ntab,temp1,temp2,d2lgdp)
    
    !--------------------------------------------
    ! Compute derivative for thermodynamic I Law 
    ! needed by Hermite interpolation
    !--------------------------------------------
    dlgrn = (10**lgprestab+10**lgrhotab)/(10**lgrhotab)
    
200 format(i4)   
    
    return
  end subroutine fileos
  
  !--------------------------------------------------------------- 
  ! Do interpolation :
  ! -> Linear NOT thermodynamically consistent
  ! -> Spline NOT thermodynamically consistent
  ! -> Hermite (3) thermodynamically consistent
  ! ( Change here the method for equilibrium model, polar and axial 
  !   change in radial.f90 the method for radial )
  !--------------------------------------------------------------- 
  
  subroutine tabeos(pres,ztab,ytab,xtab,d2ydxtab,dydz1law,ntab,&
       & dens,dpde)
    
    use nradp
    use const
    use myutil
    
    implicit none

    integer(i4b),intent(in) :: ntab
    real(dp),intent(in)     :: pres,xtab(ntab),ztab(ntab),ytab(ntab)
    real(dp),intent(in)     :: d2ydxtab(ntab),dydz1law(ntab)
    real(dp),intent(out)    :: dens,dpde
    real(dp)                :: x,y,dydx,d2ydx,dydz,d2ydz
    real(dp)                :: z,dzdx,d2zdx
    integer(i4b)            :: j

    real(dp)                :: temp1,temp2,temp3(ntab)
    
    x=dlog10(pres)
   
    !-----------------------
    ! linear interpolation
    !-----------------------
    !
    call linint_der(xtab,ytab,ntab,x,y,dydx,d2ydx)   
    !-----------------------
    ! spline interpolation
    !-----------------------
    !call splint_der(xtab,ytab,d2ydxtab,ntab,x,y,dydx,d2ydx)
    !-----------------------
    ! Hermite 3 interpolation
    !-----------------------
    !**** SPLINE
    !****  II derivative d2Log(nB)/dLog(p)^2 needed by spline
    !    temp1=(ztab(2)-ztab(1))/(xtab(2)-xtab(1))
    !    temp2=(ztab(ntab)-ztab(ntab-1))/(xtab(ntab)-xtab(ntab-1))
    !    call spline(xtab,ztab,ntab,temp1,temp2,temp3)
    !    call splint_der(xtab,ztab,temp3,ntab,x,z,dzdx,d2zdx)
    !**** LINEAR
    call linint_der(xtab,ztab,ntab,x,z,dzdx,d2zdx)
    ! *** ->
    call hermint_der(ztab,ytab,dydz1law,ntab,z,y,dydz,d2ydz)
    dydx = pres*(pres+10**y)/(d2ydz/dlog(10.d0)-dydz+dydz**2)/10**(2.d0*y)

    !-----------------------
    ! compute non-log qts
    !-----------------------

    dens = 10**y
    dpde = pres/dens/dydx
    
    !This is the expression for dpde in term of dydz drvts
    !dpde = dens/(pres+dens)*(d2ydz/dlog(10.d0)-dydz+dydz**2)
    
    return
  end subroutine tabeos
   
 end module eos


