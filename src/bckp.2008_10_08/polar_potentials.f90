! ****************************************************************
! * Load background vars and build zerilli potential and coeffs  *
! ****************************************************************

subroutine polar_potentials( a,b,eab,mstar,drevol,&
     & r,r2,r3,rd,rd2,rd3,rd4,dr,drd,rho,p,m,cs2, &
     & alpha,beta,gamma,cs3u4,cs3k,cs3x,cs4u3, &
     & chrr,chr,ch,chkr,chxr,chk,chx,czu3,czu4,czz,czsz, &
     & cXXrr,cXXr,cXX,cXk,&
     & czrr,czr,vz,zx,zk,zkr,& 
     & chamu4,chamx,chamk,chamkrr,chamkr,chamh, &
     & jiequil,jmin,js,ji,jmax,l,filetov,filetovgrid,flat )

  use nrtype
  use bckgrd
  
  implicit none

  integer(i4b)  :: jiequil,jmin,js,ji,jmax,j ! grid
  real(dp)      :: r(jmax),r2(jmax),r3(jmax)
  real(dp)      :: rd(jmax),rd2(jmax),rd3(jmax),rd4(jmax)
  real(dp)      :: dr
  real(dp)      :: drd !!not used
  real(dp)      :: eab(jmax),eapb(jmax),a(jmax),b(jmax) 
  real(dp)      :: mstar,rmin,rstar,drevol
  real(dp)      :: rho(jmax),p(jmax),m(jmax),cs2(jmax) 
  real(dp)      :: requil(jiequil)
  real(dp)      :: alpha(jmax),beta(jmax),gamma(jmax)  
  real(dp)      :: cs3u4(jmax),cs3k(jmax),cs3x(jmax),cs4u3(jmax)
  real(dp)      :: cXXrr(jmax),cXXr(jmax),cXX(jmax),cXk(jmax)
  real(dp)      :: chrr(jmax),chr(jmax),ch(jmax),chkr(jmax)
  real(dp)      :: chxr(jmax),chk(jmax),chx(jmax)
  real(dp)      :: czu3(jmax),czu4(jmax),czz(jmax),czsz(jmax)
  real(dp)      :: czrr(jmax),czr(jmax),vz(jmax)
  real(dp)      :: zx(jmax),zk(jmax),zkr(jmax)
  real(dp)      :: chamu4(jmax),chamx(jmax),chamk(jmax)
  real(dp)      :: chamkrr(jmax),chamkr(jmax),chamh(jmax)
  real(dp)      :: l,cl,l2,lz,lam                      
  character(64) :: filetov,filetovgrid,flat     
     
  call bckgrd_grid(filetovgrid,jiequil,requil)
  call bckgrd_vars(filetov,jiequil,requil,ji,jmax,r,m,p,rho,a,cs2)
   
  cl  = 2d0/(l*(l+1.d0))
  l2  = l*l
  lz  = 0.5d0*l*(l+1.d0)-1.d0
  lam = l*(l+1)
  
  ! interior
  do j=1,ji
     
     b(j)   = -0.5d0 * dlog(1-2*m(j)*rd(j))
     eab(j) =  dexp(a(j)-b(j))
     eapb(j)=  dexp(a(j)+b(j))
     
     ! **************************************************************
     ! * coefficients of the S-equation in upwind                   *
     ! **************************************************************
     
     cs3u4(j) = -eapb(j)*(4*pi*r(j)*(5*p(j)-rho(j))+6*m(j)*rd2(j))
     
     cs3k(j)  = -2*eapb(j)*(2.d0*r(j)*(m(j)*rd2(j) &
          & +4.d0*pi*p(j)*r(j))**2/(r(j)-2d0*m(j))+8*pi*rho(j) &
          & -6d0*m(j)*rd3(j))*rd(j)
      
     cs3x(j)  = -eapb(j)*(4*r(j)*(m(j)*rd2(j)+4.d0*pi*p(j)*r(j))**2 &
          & /(r(j)-2d0*m(j)) + 4*pi*(3*rho(j)+5*p(j)) &
          & -2*rd2(j)*(1d0+m(j)*rd(j)) &
          & -(l-1.d0)*(l+2d0)*rd2(j))
      
     cs4u3(j) = -eapb(j)*(2d0*m(j)*rd2(j)+4*pi*r(j)*(p(j)-rho(j)))
      
     ! **************************************************************
     ! * coefficients of S-equation in standard differencing        *
     ! **************************************************************

     cXXrr(j) = dexp(2.d0*(a(j)-b(j)))
     
     cXXr(j)  = dexp(2*a(j))*(4.d0*pi*r(j)*(5.d0*p(j)-rho(j)) &
          & + 6.d0*m(j)*rd2(j)) 
     
     cXX(j)   = -dexp(2*a(j))*(2.d0*rd2(j)*(1.d0+m(j)*rd(j)) &
          & -4.d0*pi*(3.d0*rho(j)+5.d0*p(j)) &
          & -4*dexp(2.d0*b(j))*(m(j)*rd2(j)+4*pi*r(j)*p(j))**2 &
          & +(l-1.d0)*(l+2.d0)*rd2(j))

     cXk(j)   = dexp(2*a(j))*2*rd(j)*(2*dexp(2*b(j)) &
          & * (m(j)*rd2(j)+4*pi*r(j)*p(j))**2+8*pi*rho(j) &
          & - 6*m(j)*rd3(j)) 
     
     ! **************************************************************
     ! * coefficients of fluid equation: use standard differencing  *
     ! ************************************************************** 
      
     chrr(j) = cs2(j)*dexp(2*(a(j)-b(j)))
      
     chr(j)  = -dexp(2*a(j))*(m(j)*rd2(j)*(1+cs2(j))&
          & +4*pi*p(j)*r(j)*(1-2*cs2(j))&
          & +cs2(j)*(4*pi*r(j)*rho(j)-2*rd(j)))
      
     ch(j)   = dexp(2*a(j))*(4*pi*(p(j)+rho(j))*(3*cs2(j)+1.d0)&
          &   -cs2(j)*l*(l+1d0)*rd2(j))
      
     chkr(j) = dexp(2d0*a(j))*0.5d0*(1-cs2(j))&
          &  * (m(j)*rd2(j)+4*pi*p(j)*r(j))
       
     chxr(j) = -dexp(2d0*a(j))*0.5d0*(1-cs2(j))&
          &   *(m(j)*rd(j)+4d0*pi*p(j)*r2(j))
      
     chk(j)  = -dexp(2*a(j))&
          &  *(2*(m(j)+4*pi*p(j)*r3(j))**2 *rd3(j)/(r(j)-2*m(j))&
          &  -4*pi*cs2(j)*(3*p(j)+rho(j)))
      
     chx(j)  = -dexp(2*a(j))&
          & *(2*(m(j)+4*pi*p(j)*r3(j))**2 *rd2(j)/(r(j)-2*m(j))&
          & -4*pi*r(j)*cs2(j)*(3*p(j)+rho(j))&
          & +0.5*(1.d0-cs2(j))*(m(j)*rd2(j)+4*pi*p(j)*r(j)))
      
     ! **************************************************************
     ! * coefficients of hamiltonian constraint                     *
     ! **************************************************************
      
     chamkrr(j)= r(j)*(r(j)-2*m(j))
      
     chamkr(j) = 2*r(j)-3*m(j)-4*pi*rho(j)*r3(j)
      
     chamk(j)  = -(l*(l+1d0)-8*pi*rho(j)*r2(j))
      
     chamx(j)  = -2*r(j)+2*m(j)-0.5*r(j)*l*(l+1d0)+8*pi*rho(j)*r3(j)
      
     chamu4(j) = -r(j)*(r(j)-2*m(j))
      
  end do
   
  do j=1,ji-1
     chamh(j)  =  8*pi*(p(j)+rho(j))*r2(j)/cs2(j)
  end do
   
  ! exterior
  do j=ji,jmax
      
     ! use flat space ?
     if(flat.eq.'yes')then 
        mstar  =  0.d0         
     end if
      
     a(j)   =  0.5 * dlog(1-2*mstar*rd(j))
     b(j)   = -0.5 * dlog(1-2*mstar*rd(j))
     m(j)   =  mstar
     p(j)   =  0.d0
     rho(j) =  0.d0
     eab(j) =  dexp(a(j)-b(j))
      
     ! **************************************************************
     ! * coefficients of the S-equation in upwind                   *
     ! **************************************************************
      
     cs3u4(j) = -6*mstar*rd2(j)
           
     cs3k(j)  = -4*mstar*(7*mstar-3*r(j))*rd4(j)/(r(j)-2d0*mstar)
      
     cs3x(j)  = -(8*mstar**2+2*mstar*r(j)-2*r2(j))*rd3(j)/(r(j)-2d0*mstar)&
          & +(l-1d0)*(l+2d0)*rd2(j)
      
     cs4u3(j) = -2d0*mstar*rd2(j)
 
     ! **************************************************************
     ! * coefficients of the S-equation in leapfrog                 *
     ! **************************************************************
     
     cXXrr(j) = dexp(2.d0*(a(j)-b(j)))
     
     cXXr(j) = dexp(2*a(j))*6.d0*mstar*rd2(j)
     
     cXk(j) = dexp(2*a(j))*rd(j)*4*mstar*(7*mstar-3*r(j))/(r(j)-2*mstar)*rd3(j)
     
     cXX(j) = -dexp(2.d0*a(j))*(2.d0*rd2(j)*(1.d0+m(j)*rd(j)) &
          & -4.d0*pi*(3.d0*rho(j)+5.d0*p(j)) &
          & -4*dexp(2.d0*b(j))*(m(j)*rd2(j)+4*pi*r(j)*p(j))**2 &
          &  +(l-1.d0)*(l+2.d0)*rd2(j)) 
     
     ! ************************************************************************
     ! * potential e coefficients for the evolution of the zerilli's function *
     ! ************************************************************************
      
     czrr(j) = (1d0-2d0*mstar*rd(j))**2
      
     czr(j)  = 2.d0*mstar*rd2(j)*(1.d0-2d0*mstar*rd(j))
      
     vz(j) =  - 2.d0*(lz**2*(lz+1.d0)*r3(j)+3.d0*lz**2*mstar*r2(j) &
          &          + 9.d0*lz*mstar**2*r(j)+9.d0*mstar**3) &
          &          /(r3(j)*(lz*r(j)+3.d0*mstar)**2) &
          &          * dexp(2.d0*a(j))
       
     ! **************************************************************
     ! * coefficients of hamiltonian constraint                     *
     ! **************************************************************
      
     chamkrr(j) = r(j)*(r(j)-2*mstar)
      
     chamkr(j)  = 2*r(j)-3*mstar
      
     chamk(j)   = -l*(l+1d0)
      
     chamx(j)   = -(2*r(j)-2*mstar+0.5d0*r(j)*l*(l+1d0))
      
     chamu4(j)  = -r(j)*(r(j)-2*mstar)
      
     chamh(j)   = 0.d0
      
     ! **************************************************************
     ! *  coefficients for the zerilli's equation in upwind form    *
     ! **************************************************************
      
     czu4(j) = -2.d0*mstar*rd2(j)
      
     czz(j)  = -vz(j)*dexp(b(j)-a(j))
      
     czsz(j) = -dexp(b(j)-a(j))
      
     czu3(j) = -2d0*mstar*rd2(j)
      
     ! ******************************************************************
     ! * coefficientst for the zerilli's function computed algebrically * 
     ! ******************************************************************
      
     zx(j)  =    4d0*r2(j)*(r(j)-2d0*mstar)/lam/((lam-2d0)*r(j)+6d0*mstar)
           
     zkr(j) =   -4d0*r2(j)*(r(j)-2d0*mstar)/lam/((lam-2d0)*r(j)+6d0*mstar)
      
     zk(j)  = 2d0*r(j)*(r(j)*lam+2d0*mstar)/lam/((lam-2d0)*r(j)+6d0*mstar)     
      
  end do
   
  ! **************************************************************
  ! * coefficients for equation for k (hamiltonian constraint)   *
  ! **************************************************************
   
  do j=js,ji
      
     alpha(j) = r2(j)-2d0*m(j)*r(j) &
          &  -0.5d0*(2*r(j)-3d0*m(j)-4*pi*rho(j)*r3(j))*dr
      
     beta(j)  = -(l*(l+1d0)-8*pi*rho(j)*r2(j))*dr**2&
          & -2d0*r(j)*(r(j)-2d0*m(j))
      
     gamma(j) = r2(j)-2d0*m(j)*r(j)&
     & +0.5d0*(2*r(j)-3d0*m(j)-4*pi*rho(j)*r3(j))*dr
      
  end do
   
  do j=ji+1,jmax-1
      
     alpha(j) = r2(j)-2d0*m(j)*r(j)&
          & -0.5d0*(2*r(j)-3d0*m(j)-4*pi*rho(j)*r3(j))*dr
      
     beta(j)  = -(l*(l+1d0)-8*pi*rho(j)*r2(j))*dr**2&
          & -2d0*r(j)*(r(j)-2d0*m(j))
      
     gamma(j) = r2(j)-2d0*m(j)*r(j)&
          & +0.5d0*(2*r(j)-3d0*m(j)-4*pi*rho(j)*r3(j))*dr
      
  end do
   
  j=js-1
   
  alpha(j)= 0.d0
   
  beta(j)  = -(l*(l+1d0)-8*pi*rho(j)*r2(j))*dr**2&
       &  -2d0*r(j)*(r(j)-2d0*m(j))
   
  gamma(j) = r2(j)-2d0*m(j)*r(j)&
       &  +0.5d0*(2*r(j)-3d0*m(j)-4*pi*rho(j)*r3(j))*dr
   
  return
end subroutine  polar_potentials
   
