! *****************************************************************
! * this module contains some utilities                           *
! *****************************************************************

module myutil

  use nrtype

  implicit none 

  interface diff1stf
     module procedure diff1stf
  end interface

  interface diff1stb
     module procedure diff1stb
  end interface

  interface diff2nd
     module procedure diff2nd
  end interface

  interface diff4th
     module procedure diff4th
  end interface

  interface locate
     module procedure locate
  end interface

  interface linint_der
     module procedure linint_der
  end interface
  
  interface hermint_der
     module procedure hermint_der
  end interface
   
contains
  
  ! -----------------------------------------------
  ! compute I derivative 1st order forward
  ! -----------------------------------------------
  
  subroutine diff1stf(psi,dpsi,x,jmax)
    
    integer(i4b) :: j,jmax
    real(dp)     :: psi(jmax),dpsi(jmax),x(jmax)
    
    do j=2,jmax
       dpsi(j)=(psi(j)-psi(j-1))/(x(j)-x(j-1))
    end do
    
    ! copy boundaries
    dpsi(1)=dpsi(2)

    return
  end subroutine diff1stf

  ! -----------------------------------------------
  ! compute I derivative 1st order backward
  ! -----------------------------------------------
  
  subroutine diff1stb(psi,dpsi,x,jmax)
    
    integer(i4b) :: j,jmax
    real(dp)     :: psi(jmax),dpsi(jmax),x(jmax)
    
    do j=1,jmax-1
       dpsi(j)=(psi(j+1)-psi(j))/(x(j+1)-x(j))
    end do
    
    ! copy boundaries
    dpsi(jmax)=dpsi(jmax-1)

    return
  end subroutine diff1stb

  ! -----------------------------------------------
  ! compute I derivative 2nd order centered stancil
  ! -----------------------------------------------
  
  subroutine diff2nd(psi,dpsi,x,jmax)

    integer(i4b) :: j,jmax
    real(dp)     :: psi(jmax),dpsi(jmax),x(jmax)
    
    do j=2,jmax-1
       dpsi(j)=(psi(j+1)-psi(j-1))/(x(j+1)-x(j-1))
    end do
    
    ! o(dx^2) boundaries
    dpsi(1)=-(3.d0*psi(1)-4.d0*psi(2)+psi(3))/(x(3)-x(1))
    dpsi(jmax)=(3*psi(jmax)-4.d0*psi(jmax-1)+psi(jmax-2))/(x(jmax)-x(jmax-2)) 

    return
  end subroutine diff2nd

  ! -----------------------------------------------
  ! compute I derivative 4th order centered stancil
  ! note here is given x(jmax) instead of dx    
  ! -----------------------------------------------
  
  subroutine diff4th(psi,dpsi,x,jmax)
    
    integer(i4b) :: j,jmax
    real(dp)     :: psi(jmax),dpsi(jmax),x(jmax)
    
    do j=3,(jmax-2)
       dpsi(j)=1.d0/3.d0*(8.d0*psi(j+1)-psi(j+2)-8.d0*psi(j-1)+psi(j-2))/(x(j+2)-x(j-2))
    end do
    
    ! o(dx^4) boundaries
    dpsi(1)=( -24.d0/17.d0*psi(1) + 59.d0/34.d0*psi(2) - 4.d0/17.d0*psi(3) &
         & - 3.d0/34.d0*psi(1+3) )/(x(2)-x(1))
    dpsi(2)=(-1.d0/2.d0*psi(1) + 1.d0/2.d0*psi(3) )/(x(2)-x(1));
    
    dpsi(jmax) = -( -24.d0/17.d0*psi(jmax) + 59.d0/34.d0*psi(jmax-1) - 4.d0/17.d0*psi(jmax-2) &
         & - 3.d0/34.d0*psi(jmax-3) )/(x(jmax)-x(jmax-1))
    dpsi(jmax-1) = -( -1.d0/2.d0*psi(jmax) + 1.d0/2.d0*psi(jmax-2) )/(x(jmax)-x(jmax-1))
    
    return
  end subroutine diff4th
  
  ! -----------------------------------------------
  ! given the vector xx(1...n) and the value x 
  ! return the index j such that xx(j) < x < xx(j+1)
  ! -----------------------------------------------

  subroutine locate(xx,n,x,j)
    
    integer(i4b) :: j,n
    real(dp) :: x,xx(n)
    integer(i4b) :: jl,jm,ju

    jl=0
    ju=n+1
10  if(ju-jl.gt.1)then
       jm=(ju+jl)/2
       if((xx(n).ge.xx(1)).eqv.(x.ge.xx(jm)))then
          jl=jm
       else
          ju=jm
       endif
       goto 10
    endif
    if(x.eq.xx(1))then
       j=1
    else if(x.eq.xx(n))then
       j=n-1
    else
       j=jl
    endif
    
    return
  end subroutine locate

  ! -----------------------------------------------
  ! given the vectors x(1...n) and y(x) return 
  ! the linear interpolated value yi corresponding 
  ! to xi and its derivatives
  ! -----------------------------------------------

  subroutine linint_der(x,y,n,xi,yi,d1yi,d2yi)
    
    integer(i4b) :: n,j
    real(dp) :: x(n),y(n),xi
    real(dp) :: yi,d1yi,d2yi
    
    call locate(x,n,xi,j)
    d2yi = 0.d0
    d1yi = (y(j+1)-y(j))/(x(j+1)-x(j))
    yi   = y(j) + d1yi*(xi - x(j))
    
    return
  end subroutine linint_der

  ! -----------------------------------------------
  ! given the vectors x(1...n), y(x) and y'(x) return 
  ! the 3rd order Hermite interpolated value yi 
  ! corresponding to xi and its derivatives
  ! -----------------------------------------------

  subroutine hermint_der(x,y,dydx,n,xi,yi,d1yi,d2yi)
    
    integer(i4b) :: n,j,jpo
    real(dp) :: x(n),y(n),dydx(n),xi
    real(dp) :: yi,d1yi,d2yi
    real(dp) :: dydxj,dydxjpo
    real(dp) :: w,h0w,h1w,mw,h0mw,h1mw,dw

    call locate(x,n,xi,j)
    jpo = j+1

    w=(xi-x(j))/(x(jpo)-x(j))
    h0w = 2.d0*w**3 - 3.d0*w**2 + 1.d0
    h1w = w**3 - 2.d0*w**2 + w
    
    mw = 1.d0-w
    h0mw = 2.d0*mw**3.d0 - 3.d0*mw**2 + 1.d0
    h1mw = mw**3.d0 - 2.d0*mw**2 + mw
    
    dydxj   = dydx(j)
    dydxjpo = dydx(jpo)
    
    yi = y(j)*h0w &
         & + y(jpo)*h0mw &
         & + dydxj*(x(jpo)-x(j))*h1w &
         & - dydxjpo*(x(jpo)-x(j))*h1mw
    
    dw = 1.d0/(x(jpo)-x(j))
    
    d1yi = y(j)*(6.d0*w**2.d0-6.d0*w)*dw &
         & - y(jpo)*(6.d0*mw**2-6.d0*mw)*dw &
         & + dydxj*(3.d0*w**2-4.d0*w+1.d0) &
         & + dydxjpo*(3.d0*mw**2-4.d0*mw+1.d0)
    
    d2yi = y(j)*(12.d0*w-6.d0)*dw*dw &
         & + y(jpo)*(12.d0*mw-6.d0)*dw*dw &
         & + dydxj*(6.d0*w-4.d0)*dw &
         & - dydxjpo*(6.d0*mw-4.d0)*dw
    
    return
  end subroutine hermint_der

end module myutil
