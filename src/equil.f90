! *****************************************************************
! * T.O.V. equilibrium configuration                              *
! * this routine solve the Tolman-Oppenheimer-Volkoff equations   *
! * for the static equilibrium of a spherical star                *
! *****************************************************************

subroutine equil(presc,presatm,r1,r2,dr,rmax,eosflag,k,g, &
     & filetovsetup,filetovgrid,filetov,filetabeos)  

  use nrtype
  use nrutil, only : nrerror, dreallocate
  use eos

  implicit none     

  real(dp)                      :: presc    ! central pressure (used as new pressure every step)
  real(dp)                      :: presatm, deFe56
  real(dp)                      :: r1,r2,dr
  real(dp)                      :: rmax
  real(dp)                      :: k,g
  real(dp),dimension(:),pointer :: lgntab,lgdtab,lgptab,d2lgdp,dlgrn !EoS tables array
  integer(i4b)                  :: ntab
  character(64)                 :: eosflag,filetabeos
  integer(i4b)                  :: i,j,count,nok,nbad
  integer(i4b)                  :: ji,ni
  real(dp)                      :: x1,x2,h1,hmin
  real(dp)                      :: y(3),dydx(3),r
  real(dp)                      :: de,dpdde ! total energy density and p-derivative (change every step)
  real(dp),dimension(:),pointer :: mass,pres,dens,a,cs2,rg 
  real(dp)                      :: ms,rstar
  character(64)                 :: filetovsetup,filetovgrid,filetov
  
  ! ***************************************************************
  ! * 1. setting tov model and initial data                       *
  ! *************************************************************** 
  
  if(eosflag.eq.'poly')then
     call polyeos(de,presc,dpdde,k,g)
  else if(eosflag.eq.'rhopoly') then
     call rhopolyeos(de,presc,dpdde,k,g)
  else if(eosflag.eq.'tab') then 
     call fileos(filetabeos,ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
     call tabeos(presc,lgntab,lgdtab,lgptab,d2lgdp,dlgrn,ntab,de,dpdde)
  end if
  deFe56 = 1.2726d-17        !density of solid 56Fe

  x1=r1
  x2=r2

  !---------------------------
  ! central values
  !---------------------------
  y(1)=4.d0*pi*x1**3*de/3.d0 !mass
  y(2)=0.d0                  !"a" metric function
  y(3)=presc                 !pressure 
  
  h1=1.d-1*(x2-x1)
  hmin=1.d-12                !keep attention at this values
  
  ! ***************************************************************
  ! * 2. solve odes and stop when pressure reaches zero           *
  ! ***************************************************************
  
  count=1

  nullify(mass)
  nullify(pres)
  nullify(dens)
  nullify(a)
  nullify(cs2)
  nullify(rg)
  
  write(*,*)' '
  write(*,*)'         iter       p(r)       '                                     
  write(*,*)' ***************************************'
  
  loop_on_rg : do while(x2.lt.rmax)
     
     call odeint(y,3,x1,x2,h1,hmin,nok,nbad,eosflag,k,g, &
          & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
     
     presc=y(3)
     
     if(eosflag.eq.'poly')then
        call polyeos(de,presc,dpdde,k,g)
     else if(eosflag.eq.'rhopoly') then
        call rhopolyeos(de,presc,dpdde,k,g)
     else if(eosflag.eq.'tab') then
        call tabeos(presc,lgntab,lgdtab,lgptab,d2lgdp,dlgrn,ntab,de,dpdde)
     end if
     
     !----------------------------------
     ! This would be the correct condition 
     ! to stop the integration ...
     !----------------------------------
     ! if(de.lt.deFe56) exit loop_on_rg
     !----------------------------------

     call tov_derivs(x2,y,dydx,eosflag,k,g, &
          & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)     
     
     ! reallocate vectors and store step data
     mass=>dreallocate(mass,count)
     pres=>dreallocate(pres,count)
     dens=>dreallocate(dens,count)
     a   =>dreallocate(a,count)
     cs2 =>dreallocate(cs2,count)
     rg  =>dreallocate(rg,count)

     mass(count)=y(1)
     pres(count)=y(3)
     dens(count)=de
     a(count)   =y(2)
     cs2(count) =dpdde
     rg(count)  =x2
     
     if(presc.lt.presatm) exit loop_on_rg

     ! prepare next lap
     x1=x2
     x2=x1+dr
     h1=1.d-1*(x2-x1)
     count=count+1
     
     if (mod(count,100).eq.0)then
        write(*,*)count,presc
     end if
     
  end do loop_on_rg

  write(*,*)' ***************************************'
  write(*,*)' '

  if(presc.gt.presatm)then
     write(*,*)' !!! warning !!! '
     write(*,*)' !!! warning !!! '
     write(*,*)' !!! warning : zero of pressure not reached : warning !!! '
     write(*,*)' !!! warning : use greater value for rmax             !!! '
     write(*,*)' !!! warning !!! '
     write(*,*)' !!! warning !!! '
  end if
  
  ji=count
  ms=y(1)  ! star mass
  rstar=x2 ! star radius 
  
  ! schwarzschild match for "a"
  a = a+0.5*dlog(1-(2*ms)/rstar)-a(ji); 

  ! ***************************************************************
  ! * 3. write datafiles & screen info                            *
  ! ***************************************************************
  
  open(100,file=filetovsetup) 
  open(200,file=filetovgrid)  
  open(300,file=filetov)      
  write(100,100)ms,dr,r2,rstar,real(ji)
  do j=1,ji
     write(200,200)rg(j)
     write(300,100)mass(j),dens(j),pres(j),a(j),cs2(j)
  end do
  close(100);
  close(200);
  close(300);
100 format(5e16.8)
200 format(f12.4)
  write(*,*)' results :'
  write(*,*)' '
  write(*,*)' m_star                  =',ms
  write(*,*)' r_star                  =',rstar
  write(*,*)' p_c                     =',pres(1)
  write(*,*)' dens_c                  =',dens(1)

  if(eosflag.eq.'poly')then
     write(*,*)' rho_c                   =',' - no rho_c with this EoS - '
  elseif(eosflag.eq.'rhopoly')then
     write(*,*)' rho_c                   =',(pres(1)/k)**(1.d0/g)
  else if(eosflag.eq.'tab') then
     write(*,*)' rho_c                   =',' - no rho_c with this EoS - '
  end if

  write(*,*)' '
  write(*,*)' rmin                    = ',rg(1)
  write(*,*)' dr                      = ',h1
  write(*,*)' ji                      = ',ji
  write(*,*)' r(ji)                   = ',rg(ji)
  write(*,*)' '
  write(*,*)' data files : '
  write(*,*)' '
  write(*,*)' ',filetovsetup
  write(*,*)' ',filetovgrid
  write(*,*)' ',filetov

  return
end subroutine equil


! ****************************************************************
! * constant density eos : equilibrium structure                 *
! ****************************************************************

subroutine constdens_equil(mstar,rmin,dr,rstar,filetovsetup,filetovgrid,filetov)  
 
  use nrtype
  implicit none     
  real(dp)                      :: rmin,dr,rstar,r3,mstar
  integer(i4b)                  :: j
  integer(i4b)                  :: ji
  real(dp),dimension(:),pointer :: mass,pres,dens,a,cs2,rg,expa,tempy
  real(dp)                      :: presc,tempy1
  character(64)                 :: filetovsetup,filetovgrid,filetov

  ji=(rstar-rmin)/dr+1
  allocate(mass(ji),pres(ji),dens(ji),a(ji),cs2(ji),rg(ji),expa(ji),tempy(ji))

  ! radial grid
  rg(1)=rmin
  do j=2,ji
     rg(j)=rg(j-1)+dr
  end do

  r3=rstar*rstar*rstar  
  
  ! ***************************************************************
  ! * 1. built vars from the analytic solution                    *
  ! *************************************************************** 

  dens = (3.d0/4.d0)*mstar/(pi*r3)
  mass = (4.d0/3.d0)*pi*dens*rg**3

  tempy  = dsqrt(1.d0-2.d0*mass/rg)
  tempy1 = dsqrt(1.d0-2.d0*mstar/rstar)

  presc = dens(1)*(  1 - tempy1 )/( 3*tempy1 - 1 )
  pres  = dens*( tempy - tempy1 )/( 3*tempy1 - tempy )
  
  expa = (3.d0/2.d0)*dsqrt(tempy1) - 0.5d0*dsqrt( 1-2.d0*mstar*rg**2/r3 )
  a    = dlog(expa)
  
  cs2  = 0
  
  ! ***************************************************************
  ! * 3. write datafiles & screen info                            *
  ! ***************************************************************
  
  open(100,file=filetovsetup) 
  open(200,file=filetovgrid)  
  open(300,file=filetov)      
  write(100,100)mstar,dr,rmin,rstar,ji
  do j=1,ji
     write(200,200)rg(j)
     write(300,100)mass(j),dens(j),pres(j),a(j),cs2(j)
  end do
  close(100);
  close(200);
  close(300);
100 format(5e16.8)
200 format(f12.4)
  write(*,*)' results :'
  write(*,*)' '
  write(*,*)' m_star                  =',mstar
  write(*,*)' r_star                  =',rstar
  write(*,*)' p_c                     =',pres(1)
  write(*,*)' dens_c                  =',dens(1)
  write(*,*)' '
  write(*,*)' rmin                    = ',rg(1)
  write(*,*)' dr                      = ',dr
  write(*,*)' ji                      = ',ji
  write(*,*)' r(ji)                   = ',rg(ji)
  write(*,*)' '
  write(*,*)' data files : '
  write(*,*)' '
  write(*,*)' ',filetovsetup
  write(*,*)' ',filetovgrid
  write(*,*)' ',filetov

  return
end subroutine constdens_equil
