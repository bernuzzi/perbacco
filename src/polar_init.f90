! *******************************************************************
! * initial condition for the time evolution of polar perturbations *
! *******************************************************************

subroutine polar_init(ri,sigma,initialflag,aleph,readfh,nnodes,&
     & r,r2,r3,rd,rd2,dr,rho,m, &
     & k,chi,u,chih,h,hh,zan,z,zh,uz, & 
     & chamu4,chamx,chamk,chamkrr,chamkr,chamh, &
     & js,ji,jmax,dt,l,fileh,perturbation_amplitude, &
     & filepolarinit)

  use nrtype
  use myutil

  implicit none
  
  integer(i4b)  :: jmin,js,ji,jmax,j         
  real(dp)      :: r(jmax),r2(jmax),r3(jmax)    
  real(dp)      :: rd(jmax),rd2(jmax)
  real(dp)      :: dr,dt
  real(dp)      :: q,ri,sigma,aleph,nnodes,nn,intgr   
  real(dp)      :: perturbation_amplitude
  character(64) :: initialflag,filepolarinit
  character(64) :: fileh,readfh     
  real(dp)      :: rho(jmax),m(jmax)   
  real(dp)      :: k(jmax),chi(jmax),zan(jmax)
  real(dp)      :: u(jmax,2),chih(jmax)
  real(dp)      :: h(jmax),hh(jmax)
  real(dp)      :: z(jmax),zh(jmax),uz(jmax,2)
  real(dp)      :: chamu4(jmax),chamx(jmax),chamk(jmax),chamkrr(jmax)
  real(dp)      :: chamkr(jmax),chamh(jmax)   
  real(dp)      :: a(jmax),b(jmax),c(jmax),f(jmax)
  real(dp)      :: d2z(jmax),d2zh(jmax),dz(jmax),dzh(jmax),dk(jmax),d2k(jmax)
  real(dp)      :: kh(jmax),chihold(jmax),dchi(jmax)
  real(dp)      :: ham(jmax)
  real(dp)      :: l,l2,lam

  !----------------------------------------------------------------------
  ! The following possibilities 
  !----------------------------------------------------------------------
  ! [OK] 'zgauss' ->  Gaussian pulse in Z (better using leapfrog for Z).
  ! [OK] 'chigauss' -> Gaussian pulse in S, H=0, compute k by Hamiltonian constraint.
  ! [??] 'kgauss' -> Gaussian pulse in k, H=0, compute S by Hamiltonian constraint.
  ! [OK] 'betafam' -> H=sin(Npir/R), S=beta*k, compute k by Hamiltonian constraint.
  ! [OK] 'hkzero' -> H=sin(Npir/R), k=0, compute S by Hamiltonian constraint.
  ! [TODO] 'kgausschizero' -> Gaussian pulse in k, S=0, compute H by Hamiltonian constraint.
  !-----------------------------------------------------------------------
   
  l2   = l*l
  lam  = l*(l+1.d0)
  q    = sigma**2  
  jmin = js-1

  write(*,*)' Initial condition :'
   
  if(initialflag.eq.'zgauss')then
   
     ! ***********************************************************
     ! * gaussian pulse in z                                     *
     ! ***********************************************************
     
     write(*,*)' '
     write(*,*)' z   := gaussian pulse '
     write(*,*)'     ri    = ',ri
     write(*,*)'     sigma = ',sigma
     write(*,*)' k   := k(z)'
     write(*,*)' chi := chi(z)'
     write(*,*)' '
     
     !------------------------------------------
     ! set z
     !------------------------------------------

     ! ################### FIXME
     ! in j=ji the I.D. are not consistent with B.C.
     ! ###################
     
     z = perturbation_amplitude*dexp(-(r-ri)**(2.d0)/q)
     
     do j=jmin,ji
        z(j) = 0.d0
     end do
     
     write(*,*)' Warning: At star surface these ID are NOT consistent with B.C.'
     
     dz = -2.d0*(r-ri)*z/q
     d2z = -2.d0*z/q -2.d0*(r-ri)*dz/q
     
     !------------------------------------------
     ! set zh 
     !------------------------------------------
     
     !zh  = z                       ! time symmetric gaussian pulse
     zh = z + dt*(1-2*m(ji)/r(j))*dz ! ingoing gaussian pulse
     
     ! compute numeric derivatives of zh
     
     do j=js+1,jmax-1
        dzh(j)  = 0.5d0*(zh(j+1)-zh(j-1))/dr
        d2zh(j) = (zh(j+1)-2*zh(j)+zh(j-1))/dr**2
     end do
      
     dzh(js)    = dzh(js+1)
     dzh(jmax)  = dzh(jmax-1)
     d2zh(js)   = d2zh(js+1)
     d2zh(jmax) = d2zh(jmax-1)
     
     !------------------------------------------
     ! computation of k and chi starting from z
     ! Ruoff PRD 63 064018 Eqs.(63a)-(63b)
     ! k := T(Ruoff)/r - chi = S(Ruoff)
     ! z := 2 z(Ruoff)
     !------------------------------------------
     
     k(jmin)   = 0.d0
     chi(jmin) = 0.d0
     
     k = (1-2*m*rd)*dz + rd*z*( &
          & 0.5d0*l*(l+1d0) &
          & - 6.d0*m*rd*(1-2*m*rd)&
          & /( (l-1.d0)*(l+2.d0)+6*m*rd )&
          & )

     chi = (1-2*m*rd)*d2z & 
          & + m*rd2*dz*( 1.d0 &
          &   - 6.d0*(1-2*m*rd)/( (l-1.d0)*(l+2d0)+6.d0*m*rd )&
          & ) & 
          & + z*rd2*( 3.d0*m*rd - l*(l+1.d0) &
          &   + 6.d0*m*rd*( 3.d0-8*m*rd )/( (l-1.d0)*(l+2.d0)+6.d0*m*rd )  &
          &   - (1.d0-2.d0*m*rd)*( 6.d0*m*rd/( (l-1.d0)*(l+2.d0) +6*m*rd ) )**2 &
          & ) 

     kh = (1-2*m*rd)*dzh + rd*zh*( &
          & 0.5d0*l*(l+1d0) &
          & - 6.d0*m*rd*(1-2*m*rd)&
          & /( (l-1.d0)*(l+2.d0)+6*m*rd )&
          & )

     chih = (1-2*m*rd)*d2zh & 
          & + m*rd2*dzh*( 1.d0 &
          &   - 6.d0*(1-2*m*rd)/( (l-1.d0)*(l+2d0)+6.d0*m*rd )&
          & ) & 
          & + zh*rd2*( 3.d0*m*rd - l*(l+1.d0) &
          &   + 6.d0*m*rd*( 3.d0-8*m*rd )/( (l-1.d0)*(l+2.d0)+6.d0*m*rd )  &
          &   - (1.d0-2.d0*m*rd)*( 6.d0*m*rd/( (l-1.d0)*(l+2.d0) +6*m*rd ) )**2 &
          & )
     
     k=2.d0*k
     kh=2.d0*k
     chi=2.d0*chi
     chih=2.d0*chih

     !------------------------------------------
     ! computation of u(j,2) from hamiltonian constraint     
     !------------------------------------------     
     
     u(jmin,2) =  0.d0   
     
     do j=js,jmax-1
        u(j,2) = -(chamkrr(j)*(k(j+1)-2*k(j)+k(j-1))/dr**2 &
             &  + chamkr(j) *0.5d0*(k(j+1)-k(j-1))/dr &
             &  + chamk(j) * k(j)  &
             &  + chamx(j) * chi(j) )/chamu4(j)
     end do
     
     u(jmax,2) = u(jmax-1,2)
     
     !------------------------------------------
     ! set chi,t 
     !------------------------------------------     
     
     u(:,1) = (chih-chi)/dt
     !u(:,1) = 0.d0
     
     !------------------------------------------     
     ! set initial data for the fluid
     !------------------------------------------     
     
     h  = 0.d0
     hh = 0.d0
     
     !------------------------------------------     
     ! re-computation of the zerilli's function from k and chi 
     !------------------------------------------          

     !call diff2nd(k,dk,r,jmax)
     call diff4th(k,dk,r,jmax) ! USE THIS !

     zan = 2d0/(l*(l+1d0)) * r*(r-2.d0*m)/( (l*l+l-2d0)*r +6*m ) &
          & * ( r*chi - r*dk &
          &     + 0.5*(r*l*(l+1d0)+2*m)*k/(r-2*m) &
          &   )
     
     do j=1,ji-1
        zan(j) = 0.d0
     end do
     
     !------------------------------------------     
     ! set uz(j,1) and uz(j,2) 
     !------------------------------------------     
     
     uz(:,2) = dz         
     uz(:,1) = -uz(:,2)  ! ingoing gaussian pulse
     
  elseif(initialflag.eq.'chigauss')then
     
     ! ***********************************************************
     ! * gaussian pulse in the S function                        *
     ! ***********************************************************
     
     write(*,*)' '
     write(*,*)' chi := gaussian pulse '
     write(*,*)'     ri    = ',ri
     write(*,*)'     sigma = ',sigma
     write(*,*)' k   := solving hamiltonian constraint'
     write(*,*)' '      

     !------------------------------------------     
     ! set S (rem:  here chi==S)
     !------------------------------------------          
     
     chi = perturbation_amplitude*dexp(-(r-ri)**(2.d0)/q)
     chi(jmin) = 0.d0
     dchi = -2.d0*(r-ri)*chi/q 
     
     !------------------------------------------     
     ! set H=0
     !------------------------------------------          
     
     h   = 0.d0
     hh  = 0.d0
     
     !------------------------------------------     
     ! get k from hamiltonian constraint
     !------------------------------------------     

     do j=js,jmax-1
        a(j) = chamkrr(j)-0.5*chamkr(j)*dr
        b(j) = chamk(j)*dr**2-2.d0*chamkrr(j)
        c(j) = chamkrr(j)+0.5*chamkr(j)*dr
        f(j) = -(chamx(j)*chi(j)+chamu4(j)*dchi(j) &
             &  +chamh(j)*h(j))*dr**2
     end do
     
     j=jmin
     a(j) = 0.d0
     b(j) = chamk(j)*dr**2-2.d0*chamkrr(j)
     c(j) = chamkrr(j)+0.5*chamkr(j)*dr
     f(j) = -(chamx(j)*chi(j)+chamu4(j)*dchi(j))*dr**2
     
     j=jmax
     a(j) = chamkrr(j)-0.5*chamkr(j)*dr
     b(j) = chamk(j)*dr**2-chamkrr(j)+0.5*chamkr(j)*dr
     c(j) = 0.d0
     f(j) = -(chamx(j)*chi(j)+chamu4(j)*dchi(j))*dr**2
     
     call tridag(a,b,c,f,k,jmax)

     !------------------------------------------ 
     ! set chi,r
     !------------------------------------------ 
     
     u(:,2) = dchi
     u(1,2) = 0.d0
     
     !------------------------------------------ 
     ! set chi,t=0
     !------------------------------------------ 
     
     ! time symmetric gaussian pulse

     u(:,1) = 0.d0 
     chih = chi 

     ! ingoing gaussian pulse

     !u(:,1) = -u(:,2) 
     !chih = chi + dt*(1-2*m(ji)/r(j))*dchi 

     !------------------------------------------      
     ! computation of z & zan and from k and chi
     !------------------------------------------ 
     
     call diff4th(k,dk,r,jmax) ! USE THIS !

     zan = 2d0/(l*(l+1d0)) * r*(r-2.d0*m)/( (l*l+l-2d0)*r +6*m ) &
          & * ( r*chi - r*dk &
          &     + 0.5*(r*l*(l+1d0)+2*m)*k/(r-2*m) &
          &   )
     
     do j=1,ji-1
        zan(j) = 0.d0
     end do
     
     z = zan
     zh = zan ! time symmetric ID
     
     !------------------------------------------ 
     ! set uz(j,1) and uz(j,2) 
     !------------------------------------------      
     
     call diff2nd(z,dz,r,jmax)
     uz(:,2) = dz         
     uz(:,1) = 0.d0 ! time symmetric ID
     !uz(:,1) = -uz(:,2)      ! ingoing pulse
     
  elseif(initialflag.eq.'kgauss')then

     ! ***********************************************************
     ! * gaussian pulse in the k function                        *
     ! ***********************************************************

     write(*,*)' '
     write(*,*)' k  := gaussian pulse '
     write(*,*)'     ri    = ',ri
     write(*,*)'     sigma = ',sigma
     write(*,*)' chi := solving hamiltonian constraint'
     write(*,*)' '
     
     !------------------------------------------ 
     ! set k
     !------------------------------------------  

     k  = perturbation_amplitude*dexp(-(ri-r)**(2.d0)/q)
     k(jmin) = 0.d0     
     
     dk  = -2.d0*(r-ri)*k/q 
     d2k = -2.d0*k/q -2.d0*(r-ri)*dk/q
     
     !------------------------------------------ 
     ! set H=0
     !------------------------------------------ 

     h  = 0.d0
     hh = 0.d0
     
     !------------------------------------------ 
     ! get chi from hamiltonian constraint
     !------------------------------------------ 
     
     a = 0.5d0*chamu4
     b = -chamx*dr
     c = -a
     f = dr*(chamh*h + chamkrr*d2k + chamkr*dk + chamk*k)
     
     j=jmin
     a(j) = 0.d0
     f(j) = 0.d0
     
     j=ji
     f(j) = 0.d0

     j=jmax
     b(j) = -chamx(j)*dr + chamu4(j)*0.5d0
     c(j) = 0.d0
     f(j) = 0.d0
     
     call tridag(a,b,c,f,chi,jmax)

     !------------------------------------------ 
     ! computation of u(j,2)=S,r 
     ! from hamiltonian constraint
     !------------------------------------------ 
     
     u(:,2) = -(chamkrr*d2k + chamkr*dk  &
          &+ chamk*k + chamx*chi + chamh*h)/chamu4
     
     u(jmin,2) = 0.d0
     u(jmax,2) = u(jmax-1,2)
     
     !------------------------------------------ 
     ! set chi,t
     !------------------------------------------ 

     u(:,1)=0.d0
     chih=chi

     !------------------------------------------ 
     ! computation of zan and z from k and chi
     !------------------------------------------ 

     call diff4th(k,dk,r,jmax) ! USE THIS !

     zan = 2d0/(l*(l+1d0)) * r*(r-2.d0*m)/( (l*l+l-2d0)*r +6*m ) &
          & * ( r*chi - r*dk &
          &     + 0.5*(r*l*(l+1d0)+2*m)*k/(r-2*m) &
          &   )
     
     do j=1,ji-1
        zan(j) = 0.d0
     end do
     
     z = zan
     zh = zan ! time symmetric ID

     !------------------------------------------ 
     ! set uz(j,1) and uz(j,2) 
     !------------------------------------------ 
     
     call diff2nd(z,dz,r,jmax)
     uz(:,2) = dz         
     uz(:,1) = -uz(:,2)      
     
  elseif(initialflag.eq.'betafam')then

     ! ***********************************************************
     ! *  family of initial data depending on beta               *
     ! ***********************************************************

     !------------------------------------------ 
     ! set H from file or ``sin'' function
     !------------------------------------------ 

     write(*,*)' '
     if(readfh.eq.'yes')then
        open(27,file = fileh  ,status = 'old')
        do j=1,ji
           read(27,*) r(j),h(j)
           hh(j) = h(j)
        end do
        close(27)
        write(*,*)' h := ',fileh
     else if(readfh.eq.'no')then            
        do j=1,ji
           h(j)  = perturbation_amplitude*r(j)**(l-1.d0)/(r(ji)**(l-1.d0))&
                & *dsin((nnodes+1.d0)*pi*r(j)/r(ji)) 
           hh(j) = h(j)
        end do
        write(*,*)' h :=  a*(r/R)^(l-1)*sin(n pi r/R)'
        write(*,*)'    a =',perturbation_amplitude
        write(*,*)'    n =',nnodes
     end if
     
     do j=ji+1,jmax
        h(j)  = 0.d0
        hh(j) = 0.d0
     end do
     
     write(*,*)' beta := ',aleph         
     write(*,*)' k    := solving hamiltonian constraint'
     write(*,*)' chi  := beta * k'
     write(*,*)' '
     
     !------------------------------------------  
     ! get k from hamiltonian constraint
     !------------------------------------------ 
     
     do j=js,jmax-1
        a(j) = r(j)*(r(j)-2*m(j))-0.5*(2*r(j)-3*m(j) &
             & - 4*pi*r3(j)*rho(j)-aleph*(r(j)-2*m(j)))*dr
        b(j) = -2*r(j)*(r(j)-2*m(j)) &
             & + dr**2*(-lam+8*pi*rho(j)*r2(j) &
             & + aleph*(8*pi*rho(j)*r2(j)-0.5*(lam+2)))
        c(j) = r(j)*(r(j)-2*m(j))+0.5*(2*r(j)-3*m(j) &
             & - 4*pi*r3(j)*rho(j)-aleph*(r(j)-2*m(j)))*dr
        f(j) = -chamh(j)*h(j)*dr**2
     end do
     
     j=jmin
     a(j) = 0.d0
     b(j) = -2*r(j)*(r(j)-2*m(j)) &
          & + dr**2*(-lam+8*pi*rho(j)*r2(j) &
          & + aleph*(8*pi*rho(j)*r2(j)-0.5*(lam+2)))
     c(j) =  r(j)*(r(j)-2*m(j))+0.5*(2*r(j)-3*m(j) &
          & - 4*pi*r3(j)*rho(j)-aleph*(r(j)-2*m(j)))*dr
     f(j) = -chamh(j)*h(j)*dr**2
     j=jmax
     a(j) = r(j)*(r(j)-2*m(j))-0.5*(2*r(j)-3*m(j) &
          & - 4*pi*r3(j)*rho(j)-aleph*(r(j)-2*m(j)))*dr 
     b(j) =  -2*r(j)*(r(j)-2*m(j)) &
          & + dr**2*(-lam+8*pi*rho(j)*r2(j) &
          & + aleph*(8*pi*rho(j)*r2(j)-0.5*(lam+2))) &
          & + r(j)*(r(j)-2*m(j))+0.5*(2*r(j)-3*m(j) &
          & - 4*pi*r3(j)*rho(j)-aleph*(r(j)-2*m(j)))*dr
     c(j) =  0.d0
     f(j) =  0.d0
     
     call tridag(a,b,c,f,k,jmax)
     
     !------------------------------------------ 
     ! set S = beta k/r
     !------------------------------------------ 
     
     chi = aleph*k*rd
     
     !------------------------------------------ 
     ! set chi,t=0
     !------------------------------------------ 
     
     u(:,1) = 0.d0
     chih = chi

     !------------------------------------------ 
     ! compute chi,r (use "dz" as dummy var)
     !------------------------------------------ 

     call diff2nd(chi,dz,r,jmax) 
     u(:,2) = dz
     u(jmin,2) = 0.d0
     
     !------------------------------------------ 
     ! initial data on the zerilli's function
     !------------------------------------------      

     do j=ji+1,jmax-1
        zan(j) = 2d0/(l*(l+1d0)) * ((r(j)-2*m(j))*r(j) &
             & /(l*l+l-2d0+6*m(j)*rd(j)) &
             & * (2d0*chi(j)-(k(j+1)-k(j-1))/dr &
             & + k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
     end do
     
     j=ji
     zan(j) = 2d0/(l*(l+1d0)) * ((r(j)-2*m(j))*r(j) &
          & /(l*l+l-2d0+6*m(j)*rd(j)) &
          & * (2d0*(chi(j)-0.5d0*(4*k(j+1)-3*k(j)-k(j+2))/dr) &
          & + k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
     
     j=jmax
     zan(j) = 2d0/(l*(l+1d0)) * ((r(j)-2*m(j))*r(j) &
          & /(l*l+l-2d0+6*m(j)*rd(j)) &
          & * (2d0*(chi(j)-0.5d0/dr*(3*k(j)-4*k(j-1)+k(j-2))) &
          & + k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))

     z = zan 
     zh = zan ! time symmetric ID

     !------------------------------------------ 
     ! set uz(j,1)=-exp(b-a)z,t and uz(j,2)=z,r 
     !------------------------------------------ 

     call diff2nd(z,dz,r,jmax)
     uz(:,2)= dz         
     !uz(:,1) = -uz(:,2) ! ingoing
     uz(:,1) = 0.d0 ! time symmetric ID     

  elseif(initialflag.eq.'hkzero')then
     
     ! ***********************************************************
     ! * set h and put k=0; solve for chi                        *
     ! * maximum gws content in the initial data                 *
     ! ***********************************************************

     !------------------------------------------ 
     ! set H from file or with a ``sin'' function
     !------------------------------------------  
     
     write(*,*)' '
     if(readfh.eq.'yes')then
        open(27,file = fileh  ,status = 'old')
        do j=1,ji
           read(27,*) r(j),h(j)
           hh(j) = h(j)
        end do
        close(27)
        write(*,*)'h     := ',fileh
     elseif(readfh.eq.'no')then            
        do j=1,ji
           h(j)  = perturbation_amplitude*r(j)**(l-1.d0)/(r(ji)**(l-1.d0))&
                & *dsin((nnodes+1.d0)*pi*r(j)/r(ji)) 
           hh(j) = h(j)
        end do
        write(*,*)' h :=  a*(r/R)^(l-1)*sin(n pi r/R)'
        write(*,*)'    a =',perturbation_amplitude
        write(*,*)'    n =',nnodes
     end if
     
     do j=ji+1,jmax
        h(j)  = 0.d0
        hh(j) = 0.d0
     end do
     
     write(*,*)' k   := ',0
     write(*,*)' chi := solving hamiltonian constraint'
     write(*,*)' '      
     
     !------------------------------------------ 
     ! set k = 0
     !------------------------------------------ 
     
     k = 0.d0
     
     !------------------------------------------ 
     ! get chi from hamiltonian constraint
     !------------------------------------------ 

     a = 0.5d0*chamu4
     b = -chamx*dr
     c = -a
     f = chamh*h*dr
     
     j=jmin
     a(j) = 0.d0
     f(j) = 0.d0
     
     j=ji
     f(j) = 0.d0

     j=jmax
     b(j) = -chamx(j)*dr + chamu4(j)*0.5d0
     c(j) = 0.d0
     f(j) = 0.d0
     
     call tridag(a,b,c,f,chi,jmax)
     
     !------------------------------------------  
     ! computation of u(j,2) from hamiltonian constraint 
     !------------------------------------------ 
     
     do j=js,jmax-1
        u(j,2) = -(chamkrr(j)*(k(j+1)-2*k(j)+k(j-1))/dr**2 &
             & +chamkr(j) * 0.5*(k(j+1)-k(j-1))/dr &
             & +chamk(j)  * k(j)+chamx(j) * chi(j) &
             & +chamh(j)  * h(j))/chamu4(j) 
     end do
     u(jmin,2) = 0.d0
     u(jmax,2) = u(jmax-1,2)
     
     !------------------------------------------ 
     ! set chi,t = 0
     !------------------------------------------ 
     
     u(:,1) = 0.d0
     chih = chi
     
     !------------------------------------------ 
     ! initial data for the zerilli's function
     !------------------------------------------ 
     
     do j=ji+1,jmax-1
         zan(j) = 2d0/(l*(l+1d0)) * ((r(j)-2*m(j))*r(j)  &
              & /(l*l+l-2d0+6*m(j)*rd(j)) &
              & * (2d0*chi(j)-(k(j+1)-k(j-1))/dr  &
              & +k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
      end do
      
      j=ji
      zan(j) = 2d0/(l*(l+1d0)) * ((r(j)-2*m(j))*r(j) &
           & /(l*l+l-2d0+6*m(j)*rd(j)) &
           & * (2d0*(chi(j)-0.5d0*(4*k(j+1)-3*k(j)-k(j+2))/dr) &
           & +k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
       
      j=jmax
      zan(j) = 2d0/(l*(l+1d0)) * ((r(j)-2*m(j))*r(j) &
           & /(l*l+l-2d0+6*m(j)*rd(j)) &
           & * (2d0*(chi(j)-0.5d0/dr*(3*k(j)-4*k(j-1)+k(j-2))) &
           & + k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
      
      z = zan
      zh = zan
      
      !------------------------------------------ 
      ! set uz(j,1) and uz(j,2) 
      !------------------------------------------ 
      
      call diff2nd(z,dz,r,jmax)
      uz(:,2) = dz         
      ! uz(:,1) = -uz(:,2) ! ingoing
      uz(:,1) = 0.d0 ! time symmetric
      
   elseif(initialflag.eq.'kgausschizero')then
      
      ! ***********************************************************
      ! * gaussian pulse in the k function, chi=0, compute h      *
      ! ***********************************************************

      write(*,*)' '
      write(*,*)' k   := gaussian pulse'
      write(*,*)'     ri    = ',ri
      write(*,*)'     sigma = ',sigma
      write(*,*)' chi := 0'
      write(*,*)' h   := from hamiltonian constraint'
      write(*,*)' '      

      !------------------------------------------ 
      ! set k 
      !------------------------------------------ 
      
      k  = perturbation_amplitude*dexp(-(ri-r)**(2.d0)/q)
      k(jmin) = 0.d0 

      dk  = -2.d0*(r-ri)*k/q 
      d2k = -2.d0*k/q -2.d0*(r-ri)*dk/q
      
      !------------------------------------------ 
      ! set S = 0
      !------------------------------------------ 
      
      chi = 0.d0
      
      !------------------------------------------ 
      ! set chi,t=0
      !------------------------------------------ 
      
      u(:,1) = 0.d0
      chih = chi
     
      !------------------------------------------ 
      ! chi,r = 0
      !------------------------------------------ 
     
      u(:,2) = 0.d0
      dchi = 0.d0      

      !------------------------------------------ 
      ! get h from hamiltonian constrain
      !------------------------------------------ 

      h = (chamk*k - chamkr*dk - chamkrr*d2k)/chamh

      hh = h
      
      !------------------------------------------ 
      ! computation of zan from k and chi
      !------------------------------------------ 
      
      do j=ji+1,jmax-1
         zan(j) = 2d0/(l*(l+1d0))*((r(j)-2*m(j))*r(j) &
              & /(l*l+l-2d0+6*m(j)*rd(j)) &
              & *(2d0*chi(j)-(k(j+1)-k(j-1))/dr &
              & +k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
      end do
      
      j=ji
      zan(j) = 2d0/(l*(l+1d0))*((r(j)-2*m(j))*r(j) &
           & /(l*l+l-2d0+6*m(j)*rd(j)) &
           & *(2d0*(chi(j)-0.5d0*(4*k(j+1)-3*k(j)-k(j+2))/dr) &
           & +k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
       
      j=jmax
      zan(j) = 2d0/(l*(l+1d0))*((r(j)-2*m(j))*r(j) &
           & /(l*l+l-2d0+6*m(j)*rd(j)) &
           & *(2d0*(chi(j)-0.5d0/dr*(3*k(j)-4*k(j-1)+k(j-2))) &
           & +k(j)*rd(j)*(r(j)*l*(l+1d0)+2*m(j))/(r(j)-2*m(j))))
      
      z = zan
      zh = zan
      
      !------------------------------------------ 
      ! set uz(j,1) and uz(j,2) 
      !------------------------------------------ 
      
      call diff2nd(z,dz,r,jmax)
      uz(:,2) = dz         
      uz(:,1) = -uz(:,2)

   endif
   
   ! *****************
   ! * write on file *
   ! *****************
   
   open(40,file=filepolarinit,status='unknown')
   do j=jmin,jmax
      write(40,4000)r(j),h(j),chi(j),k(j),0.5d0*zan(j),0.5d0*z(j)
   end do
   close(40)
4000 format(6f20.12)
   
   return
 end subroutine polar_init
 
 
