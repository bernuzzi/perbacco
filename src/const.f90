! *****************************************************************
! * physical constant module                                      *
! *****************************************************************

module const

  use nrtype
  
  real(dp), parameter :: vlight = 2.99792458d+10 !cm/sec
  real(dp), parameter :: newtng = 6.673d-08      !cm^3/sec/sec/g
  real(dp), parameter :: msun   = 1.9889d+33     !g
  real(dp), parameter :: mb     = 1.675d-24      !g
  
  ! Dimensional factors for conversion from adimension cctk units
  ! (clight=Gnewton=Msun=1) to cgs units 
  ! -> multiply for cctk[x]_cgs to obtain a dimensional value <-

  real(dp), parameter :: cctklength_cgs = newtng*msun/(vlight**2)
  real(dp), parameter :: cctktime_cgs   = newtng*msun/(vlight**3)
  real(dp), parameter :: cctkmass_cgs   = msun
  real(dp), parameter :: cctkenergy_cgs = msun*vlight**2
  real(dp), parameter :: cctksurf_cgs   = cctklength_cgs**2
  real(dp), parameter :: cctkvol_cgs    = cctklength_cgs**3
  real(dp), parameter :: cctkpress_cgs  = msun/(cctkLength_cgs*cctktime_cgs**2)
  real(dp), parameter :: cctkmdens_cgs  = cctkmass_cgs/cctkvol_cgs
  real(dp), parameter :: cctkedens_cgs  = cctkenergy_cgs/cctkvol_cgs
  real(dp), parameter :: cctkamom_cgs   = cctkenergy_cgs*cctkTime_cgs

end module const

