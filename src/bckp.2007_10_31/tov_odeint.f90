! *****************************************************************
! * set derivatives (tov equations)                               *
! *****************************************************************

subroutine tov_derivs(r,v,yp,eosflag,k,g, &
     & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  
  use nrtype
  use eos

  implicit none  

  real(dp)     :: r,v(3)
  real(dp)     :: yp(3)
  character(64) :: eosflag
  real(dp)     :: k,g
  real(dp)     :: dens,pres,dpddens
  
  integer(i4b) :: ntab
  real(dp)     :: lgntab(ntab),lgdtab(ntab),lgptab(ntab),d2lgdp(ntab),dlgrn(ntab)
  
  ! remember, here:
  ! r    = radi
  ! v(1) = mass (gravitational)
  ! v(2) = phi
  ! v(3) = pressure

  pres = v(3)

  if(eosflag.eq.'poly')then
     call polyeos(dens,pres,dpddens,k,g)
  else if(eosflag.eq.'rhopoly') then
     call rhopolyeos(dens,pres,dpddens,k,g)
  else if(eosflag.eq.'tab') then
     call tabeos(pres,lgntab,lgdtab,lgptab,d2lgdp,dlgrn,ntab,dens,dpddens)
  end if

  yp(1)=4.d0*pi*r*r*dens
  yp(2)=(v(1)+4.d0*pi*r**3*pres)/(r*(r-2.d0*v(1)))
  yp(3)=-(dens+pres)*yp(2)

  return

end subroutine tov_derivs


! *****************************************************************
! * integrate tov odes                                            *
! *****************************************************************

subroutine odeint(ystart,nvar,x1,x2,h1,hmin,nok,nbad,eosflag,k,g, &
     & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)

  use nrtype
  implicit none
  
  integer(i4b)             :: nvar
  real(dp)                 :: ystart(nvar)
  real(dp)                 :: h1,hmin,x1,x2
  integer(i4b), parameter  :: maxstp=100000
  integer(i4b), parameter  :: nmax=50
  integer(i4b), parameter  :: kmaxx=100
  integer(i4b), parameter  :: tiny=1.d-30
  integer(i4b)             :: nbad,nok
  integer(i4b)             :: i,nstp
  real(dp)                 :: h,hdid,hnext,x,dydx(nmax),y(nmax),yscal(nmax)
  character(64)            :: eosflag
  real(dp)                 :: k,g
  integer(i4b)             :: ntab  
  real(dp)                 :: lgntab(ntab),lgdtab(ntab),lgptab(ntab),d2lgdp(ntab),dlgrn(ntab)

  x=x1
  h=sign(h1,x2-x1)
  nok=0
  nbad=0
  ! set initial values
  do i=1,nvar
     y(i)=ystart(i)
  end do
  do nstp=1,maxstp
     !-----------------------------------------------
     call tov_derivs(x,y,dydx,eosflag,k,g, &
          & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
     !-----------------------------------------------
     do i=1,nvar
        yscal(i)=dabs(y(i))+dabs(h*dydx(i))+tiny
     end do
     if((x+h-x2)*(x+h-x1).gt.0.d0)then 
        h=x2-x
     end if
     !---------------------------------------------------------
     call rkqs(y,dydx,nvar,x,h,yscal,hdid,hnext,eosflag,k,g,&
          & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)  
     !---------------------------------------------------------
     if(hdid.eq.h)then
        nok=nok+1
     else
        nbad=nbad+1
     end if
     if((x-x2)*(x2-x1).ge.0.d0)then
        do i=1,nvar
           ystart(i)=y(i)
        end do
        return    ! normal exit
     end if
     if(dabs(hnext).lt.hmin) pause'stepsize smaller than minimum in odeint'
     h=hnext
  end do
  pause 'too many steps in tov_odeint'
  return
end subroutine odeint


! *****************************************************************
! * "quality-controlled" runge-kutta step (see n.r. chap.16.1-2)  *
! *****************************************************************

subroutine rkqs(y,dydx,n,x,htry,yscal,hdid,hnext,eosflag,k,g, &
     & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)  
  
  use nrtype
  implicit none
  
  integer(i4b)             :: n
  character(64)            :: eosflag
  real(dp)                 :: k,g
  real(dp)                 :: lgntab(ntab),lgdtab(ntab),lgptab(ntab),d2lgdp(ntab),dlgrn(ntab)
  integer(i4b)             :: ntab
  real(dp)                 :: y(n)
  real(dp)                 :: dydx(n),yscal(n)
  real(dp)                 :: x
  real(dp)                 :: htry
  real(dp)                 :: hdid,hnext
  integer(i4b)             :: i  
  integer(i4b), parameter  :: nmax=50 
  real(dp)                 :: errmax,h,xnew,yerr(nmax),ytemp(nmax)
  real(sp), parameter      :: safety=0.9d0
  real(sp), parameter      :: pgrow=-.2d0
  real(sp), parameter      :: pshrnk=-.25d0
  real(sp), parameter      :: errcon=1.89d-4 
  
  h=htry
  !--------------------------------------------------
1 call rkck(y,dydx,n,x,h,ytemp,yerr,eosflag,k,g,&
       & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  !--------------------------------------------------
  errmax=0.d0
  do i=1,n
     errmax=max(errmax,dabs(yerr(i)/yscal(i)))
  end do
  errmax=errmax/eps
  if(errmax.gt.1.d0)then
     h=safety*h*(errmax**pshrnk)
     if(h.lt.0.1d0*h)then
        h=.1d0*h
     end if
     xnew=x+h
     if(xnew.eq.x)pause 'stepsize underflow in rkqs'
     goto 1
  else
     if(errmax.gt.errcon)then
        hnext=safety*h*(errmax**pgrow)
     else
        hnext=5.d0*h
     end if
     hdid=h
     x=x+h
     do i=1,n
        y(i)=ytemp(i)
     end do
     return
  end if
  return
end subroutine rkqs


! *****************************************************************
! * cash-karp runge-kutta step  (see n.r. chap.16.1-2)            *
! *****************************************************************

subroutine rkck(y,dydx,n,x,h,yout,yerr,eosflag,k,g,&
     & ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)  

  use nrtype
  implicit none   

  integer(i4b)             :: n
  character(64)            :: eosflag
  real(dp)                 :: k,g
  real(dp)                 :: lgntab(ntab),lgdtab(ntab),lgptab(ntab),d2lgdp(ntab),dlgrn(ntab)
  integer(i4b)             :: ntab
  real(dp)                 :: y(n),dydx(n)
  real(dp)                 :: x,h
  real(dp)                 :: yerr(n),yout(n)
  integer(i4b)             :: i
  integer(i4b), parameter  :: nmax=50
  real(dp)                 ::  ak2(nmax),ak3(nmax),ak4(nmax),ak5(nmax),ak6(nmax),ytemp(nmax)
  real(sp), parameter      :: a2=.2d0
  real(sp), parameter      :: a3=.3d0
  real(sp), parameter      :: a4=.6d0
  real(sp), parameter      :: a5=1.d0
  real(sp), parameter      :: a6=.875d0
  real(sp), parameter      :: b21=.2d0
  real(sp), parameter      :: b31=3.d0/40.d0
  real(sp), parameter      :: b32=9.d0/40.d0
  real(sp), parameter      :: b41=.3d0
  real(sp), parameter      :: b42=-.9d0
  real(sp), parameter      :: b43=1.2d0
  real(sp), parameter      :: b51=-11.d0/54.d0
  real(sp), parameter      :: b52=2.5d0
  real(sp), parameter      :: b53=-70.d0/27.d0
  real(sp), parameter      :: b54=35.d0/27.d0
  real(sp), parameter      :: b61=1631.d0/55296.d0
  real(sp), parameter      :: b62=175.d0/512.d0
  real(sp), parameter      :: b63=575.d0/13824.d0
  real(sp), parameter      :: b64=44275.d0/110592.d0
  real(sp), parameter      :: b65=253.d0/4096.d0
  real(sp), parameter      :: c1=37.d0/378.d0
  real(sp), parameter      :: c3=250.d0/621.d0
  real(sp), parameter      :: c4=125.d0/594.d0
  real(sp), parameter      :: c6=512.d0/1771.d0
  real(sp), parameter      :: dc1=c1-2825.d0/27648.d0
  real(sp), parameter      :: dc3=c3-18575.d0/48384.d0
  real(sp), parameter      :: dc4=c4-13525.d0/55296.d0
  real(sp), parameter      :: dc5=-277.d0/14336.d0
  real(sp), parameter      :: dc6=c6-.25d0

  do i=1,n
     ytemp(i)=y(i)+b21*h*dydx(i)
  end do
  call tov_derivs(x+a2*h,ytemp,ak2,eosflag,k,g,ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  do i=1,n
     ytemp(i)=y(i)+h*(b31*dydx(i)+b32*ak2(i))
  end do
  call tov_derivs(x+a3*h,ytemp,ak3,eosflag,k,g,ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  do i=1,n
     ytemp(i)=y(i)+h*(b41*dydx(i)+b42*ak2(i)+b43*ak3(i))
  end do
  call tov_derivs(x+a4*h,ytemp,ak4,eosflag,k,g,ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  do i=1,n
     ytemp(i)=y(i)+h*(b51*dydx(i)+b52*ak2(i)+b53*ak3(i)+b54*ak4(i))
  end do
  call tov_derivs(x+a5*h,ytemp,ak5,eosflag,k,g,ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  do i=1,n
     ytemp(i)=y(i)+h*(b61*dydx(i)+b62*ak2(i)+b63*ak3(i)+b64*ak4(i)+b65*ak5(i))
  end do
  call tov_derivs(x+a6*h,ytemp,ak6,eosflag,k,g,ntab,lgntab,lgdtab,lgptab,d2lgdp,dlgrn)
  do i=1,n
     yout(i)=y(i)+h*(c1*dydx(i)+c3*ak3(i)+c4*ak4(i)+c6*ak6(i))
  end do
  do i=1,n
     yerr(i)=h*(dc1*dydx(i)+dc3*ak3(i)+dc4*ak4(i)+dc5*ak5(i)+dc6*ak6(i))
  end do
  return
end subroutine rkck
