! *****************************************************************
! * T.O.V. polar perturbations                                    *
! * this routine solves the equations for the polar (even-parity) *
! * perturbations of a spherical tov star                         *
! *****************************************************************

subroutine polar(filetovsetup,filetovgrid,filetov,keos,geos,fileh,&
     & l,jmin,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4,djoutput,rextmax,ji, &
     & tottime,nmovie,ncount,cf,methodev, &
     & initialflag,ri,sigma,gets,getz,aleph,readfh,nnodes,&
     & perturbation_amplitude,flat,&
     & filepolartimez,filepolartimek, & 
     & filepolartimechi,filepolartimeh,filepolartimezan, &
     & filepolarz,filepolark,filepolarchi,filepolarzan,filepolarh,& 
     & filepolarnormz,filepolarnormk,filepolarnormchi,eosflag)

  use nrtype
  use bckgrd,      only : bckgrd_setup
  use myutil

  implicit none

  real(dp)                      :: dr,drd,drevol,rextmax,ri1,ri2,ri3,ri4,ro1,ro2,ro3,ro4
  real(dp),pointer,dimension(:) :: r(:),r2(:),r3(:)
  real(dp),pointer,dimension(:) :: rd(:),rd2(:),rd3(:),rd4(:)
  integer(i4b)                  :: jiequil,jmin,js,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4
  integer(i4b)                  :: ji,je,jmax,djoutput,jimo,jipo,jimt,jmaxmo  
  real(dp)                      :: dt,dt2,cf,cf2,time,tottime,q 
  real(dp)                      :: perturbation_amplitude
  integer(i4b)                  :: nr,nt,nmovie,ncount 
  real(dp)                      :: ri,sigma,aleph,nnodes
  character(64)                 :: initialflag,eosflag 
  real(dp)                      :: keos,geos                         ! eos parameters
  real(dp),pointer,dimension(:) :: eab(:),a(:),b(:),f(:)             ! equilibrium qts
  real(dp),pointer,dimension(:) :: rho(:),p(:),m(:),cs2(:)
  real(dp)                      :: mstar,drequil,rmin,rstar

  real(dp),pointer,dimension(:) :: k(:),chi(:)                       ! perturbative vars
  real(dp),pointer,dimension(:) :: knew(:),chinew(:),dknewdr(:)
  real(dp),pointer,dimension(:) :: u(:,:),unew(:,:),s(:,:)
  real(dp),pointer,dimension(:) :: h(:),hh(:),hnew(:)
  real(dp),pointer,dimension(:) :: zan(:),zanpo(:)
  real(dp),pointer,dimension(:) :: z(:),zh(:),znew(:)
  real(dp),pointer,dimension(:) :: uz(:,:),uznew(:,:),sz(:,:)


  real(dp),pointer,dimension(:) :: alpha(:),beta(:),gamma(:)          !eqs coefs
  real(dp),pointer,dimension(:) :: cs3u4(:),cs3k(:),cs3x(:),cs4u3(:)
  real(dp),pointer,dimension(:) :: chrr(:),chr(:),ch(:),chkr(:)
  real(dp),pointer,dimension(:) :: chxr(:),chk(:),chx(:)
  real(dp),pointer,dimension(:) :: czu3(:),czu4(:),czz(:),czsz(:)
  real(dp),pointer,dimension(:) :: czrr(:),czr(:),vz(:)
  real(dp),pointer,dimension(:) :: aux(:),aux2(:)
  real(dp),pointer,dimension(:) :: zx(:),zk(:),zkr(:) 
  real(dp),pointer,dimension(:) :: chamu4(:),chamx(:),chamk(:)
  real(dp),pointer,dimension(:) :: chamkrr(:),chamkr(:),chamh(:)

  character(64)                 :: filetovsetup,filetovgrid,filetov,fileh   ! filenames
  character(64)                 :: filepolartimez,filepolartimek,filepolartimechi,filepolartimeh,filepolartimezan
  character(64)                 :: filepolarz,filepolark,filepolarchi,filepolarh,filepolarzan
  character(64)                 :: filepolarnormz,filepolarnormk,filepolarnormchi
  real(dp)                      :: l,l2,lam,lam2                            ! shorthands
  integer(i4b)                  :: j,n,count                                ! counters
  character(64)                 :: readfh,methodev,gets,getz,flat           ! flags

  count = 0   !initialization of the counter
  
  ! **************************************************************
  ! * 1. setting up the the grid and the potential               *
  ! **************************************************************
  
  call bckgrd_setup(filetovsetup,mstar,drequil,rmin,rstar,jiequil)
   
  drevol=(rstar-rmin)/(ji-0.5d0)

  jmax=ji+((rextmax-rstar)/drevol+1)  
  jmaxmo = jmax-1
  
  allocate(r(jmax))
  
  !r(0)=rmin is a ghost ...
  r(1)=rmin+0.5d0*drevol
  do j=1,jmaxmo
     r(j+1)=r(j)+drevol
  end do
  
  ! allocate vars
  allocate(r2(jmax),r3(jmax),rd(jmax),rd2(jmax),rd3(jmax),rd4(jmax)) 
  allocate(rho(jmax),p(jmax),m(jmax),cs2(jmax),a(jmax),b(jmax),eab(jmax))
  
  ! allocate perturbation vars
  allocate(k(jmax),chi(jmax),knew(jmax),chinew(jmax),dknewdr(jmax))
  allocate(u(jmax,2),unew(jmax,2),s(jmax,2)) 
  allocate(h(jmax),hh(jmax),hnew(jmax),zan(jmax),zanpo(jmax))
  allocate(z(jmax),zh(jmax),znew(jmax),uz(jmax,2),uznew(jmax,2),sz(jmax,2))

  ! allocate coeffs
  allocate(alpha(jmax),beta(jmax),gamma(jmax),f(jmax))
  allocate(cs3u4(jmax),cs3k(jmax),cs3x(jmax),cs4u3(jmax))
  allocate(chrr(jmax),chr(jmax),ch(jmax))
  allocate(chkr(jmax),chxr(jmax),chk(jmax),chx(jmax))
  allocate(czu3(jmax),czu4(jmax),czz(jmax),czsz(jmax),czrr(jmax),czr(jmax),vz(jmax))
  allocate(aux(jmax),aux2(jmax))
  allocate(zx(jmax),zk(jmax),zkr(jmax),chamu4(jmax),chamx(jmax),chamk(jmax))
  allocate(chamkrr(jmax),chamkr(jmax),chamh(jmax))
  
  dr = drevol 
  drd = 1.d0/dr

  r2  = r*r
  r3  = r2*r
  rd  = 1.d0/r
  rd2 = rd*rd
  rd3 = rd*rd2
  rd4 = rd*rd3
  
  js = jmin+1   ! 1st point for evolution
  jimo = ji-1
  jipo = ji+1
  jimt = ji-2
  
  ! internal and external observers indexes
  ji1 = (ri1-r(jmin))/drevol
  ji2 = (ri2-r(jmin))/drevol
  ji3 = (ri3-r(jmin))/drevol
  ji4 = (ri4-r(jmin))/drevol
  jo1 = (ro1-r(jmin))/drevol
  jo2 = (ro2-r(jmin))/drevol
  jo3 = (ro3-r(jmin))/drevol
  jo4 = (ro4-r(jmin))/drevol
   
  ! potentials and coeffs
  call polar_potentials( a,b,eab,mstar,drevol,&
       & r,r2,r3,rd,rd2,rd3,rd4,dr,drd,rho,p,m,cs2, &
       & alpha,beta,gamma,cs3u4,cs3k,cs3x,cs4u3, &
       & chrr,chr,ch,chkr,chxr,chk,chx,czu3,czu4,czz,czsz, &
       & czrr,czr,vz,zx,zk,zkr,&
       & chamu4,chamx,chamk,chamkrr,chamkr,chamh, &
       & jiequil,jmin,js,ji,jmax,l,filetov,filetovgrid,flat)
  
  ! time evolution settings
  dt      = cf*dr
  dt2     = dt*dt
  cf2     = cf*cf
  nt      = nint(tottime/dt)
  q       = (cf-1.d0)/(cf+1.d0)
  l2      = l*l
  lam     = l*(l+1.d0)
  lam2    = lam*lam
   
  ! screen info
  write(*,*)'  '
  write(*,*)' Settings : '
  write(*,*)'  '
  write(*,*)' l           = ',l
  write(*,*)' cf          = ',cf
  write(*,*)' total time  = ',tottime
  write(*,*)' nt          = ',nt   
  write(*,*)' ncount      = ',ncount
  write(*,*)' dt          = ',dt
  write(*,*)' movie time  = ',tottime
  write(*,*)' nmovie      = ',nmovie
  write(*,*)'  '
  write(*,*)' TOV equilibrium radial grid :'
  write(*,*)'  '
  write(*,*)' dr_equil    = ',drequil
  write(*,*)' jiequil          = ',jiequil
  write(*,*)'  '
  write(*,*)' Radial grid to evolve perturbations :'
  write(*,*)'  '
  write(*,*)' jmin        = ',jmin
  write(*,*)' ji          = ',ji
  write(*,*)' je          = ',jmax-ji
  write(*,*)' jmax        = ',jmax
  write(*,*)' ji1         = ',ji1
  write(*,*)' ji2         = ',ji2
  write(*,*)' ji3         = ',ji3
  write(*,*)' ji4         = ',ji4
  write(*,*)' jo2         = ',jo1
  write(*,*)' jo2         = ',jo2
  write(*,*)' jo3         = ',jo3
  write(*,*)' jo4         = ',jo4
  write(*,*)' djoutput    = ',djoutput
  write(*,*)' dr          = ',drevol  
  write(*,*)' r(1)        = ',r(1)
  write(*,*)' r(jmin)     = ',r(jmin)
  write(*,*)' r(ji)       = ',r(ji)
  write(*,*)' r(jmax)     = ',r(jmax)
  write(*,*)' r(ji1)      = ',r(ji1)
  write(*,*)' r(ji2)      = ',r(ji2)
  write(*,*)' r(ji3)      = ',r(ji3)
  write(*,*)' r(ji4)      = ',r(ji4)
  write(*,*)' r(jo1)      = ',r(jo1)
  write(*,*)' r(jo2)      = ',r(jo2)
  write(*,*)' r(jo3)      = ',r(jo3)
  write(*,*)' r(jo4)      = ',r(jo4)
  write(*,*)'  '
  
  ! **************************************************************
  ! * 2. initial conditions                                      *
  ! **************************************************************
  
  call polar_init( ri,sigma,initialflag,aleph,readfh,nnodes,&
       & r,r2,r3,rd,rd2,dr,rho,m, &
       & k,chi,u,h,hh,zan,z,zh,uz, &
       & chamu4,chamx,chamk,chamkrr,chamkr,chamh, &
       & js,ji,jmax,dt,l,fileh,perturbation_amplitude )
   
  ! **************************************************************
  ! * 3. open all the output files                               *
  ! **************************************************************
  
  open(10,file = filepolartimez   ,status = 'unknown')
  open(11,file = filepolartimek   ,status = 'unknown')
  open(12,file = filepolartimechi ,status = 'unknown')
  open(13,file = filepolartimeh   ,status = 'unknown')
  open(14,file = filepolartimezan ,status = 'unknown')
  open(20,file = filepolarz       ,status = 'unknown')  
  open(21,file = filepolark       ,status = 'unknown')  
  open(22,file = filepolarchi     ,status = 'unknown')  
  open(23,file = filepolarzan     ,status = 'unknown')  
  open(24,file = filepolarh       ,status = 'unknown')  
  open(30,file = filepolarnormz   ,status = 'unknown')
  open(31,file = filepolarnormk   ,status = 'unknown')
  open(32,file = filepolarnormchi ,status = 'unknown')
  
  ! **************************************************************
  ! * 4. time evolution                                          *
  ! **************************************************************
  
  write(*,*)' Time evolution :'
  write(*,*)' '                                              
  write(*,*)'         iter       of                  '      
  write(*,*)' ***************************************'
  
  do n=0,nt
     
     time = n*dt
     if (mod(n,100).eq.0)then
        write(*,*)n,nt
     end if
     
     if (mod(n,nmovie).eq.0)then
        call polar_movie(n,dt,r,k,chi,zan,z,h,js,ji,jmax,djoutput)
     end if
     
     ! 4.1
     ! ***********************************************************
     ! * compute sources for upwind schemes                      *
     ! ***********************************************************
     
     s(:,1) = cs3u4(:) * u(:,2) + cs3k(:) * k(:) + cs3x(:) * chi(:)
     s(:,2) = cs4u3(:) * u(:,1)
     
     sz(:,1) = czu4(:) * uz(:,2) + czz(:)*z(:)
     sz(:,2) = czu3(:) * uz(:,1)
     do j=jmin,jimo
        sz(j,1) = 0.d0
        sz(j,2) = 0.d0
     end do
     
     ! 4.2
     ! ***********************************************************
     ! * evolution of u(1) and u(2)                              *
     ! ***********************************************************
     
     do j=js,jmaxmo
        
        unew(j,1) = u(j,1)-0.5d0*dt*drd*eab(j) &
             & * (2.d0*u(j,1)-u(j-1,1)-u(j+1,1)+u(j+1,2)-u(j-1,2))&
             & + dt * s(j,1)
        unew(j,2) = u(j,2)-0.5d0*dt*drd*eab(j)&
             &  * (2.d0*u(j,2)-u(j-1,2)-u(j+1,2)+u(j+1,1)-u(j-1,1))&
             &  + dt * s(j,2) 
     end do
      
     unew(jmin,1)  = 0.d0
     unew(jmin,2)  = 0.d0                    

     unew(jmax,1)  = u(jmaxmo,1)+(unew(jmaxmo,1)-u(jmax,1))*q
     unew(jmax,2)  = u(jmaxmo,2)+(unew(jmaxmo,2)-u(jmax,2))*q
     
     ! 4.3
     ! ***********************************************************
     ! * evolution of the fluid variable h                       *
     ! ***********************************************************
     
     hh(jmin) = 0.d0
     h(jmin)  = 0.d0
     
     do j=js,jimo
        
        hnew(j) = 2d0*hh(j)-h(j)+cf2*( &
             & chrr(j)*( hh(j+1)-2*hh(j)+hh(j-1) ) &
             & + dr*( chr(j) * 0.5d0*(hh(j+1)-hh(j-1)) &
             & + chkr(j) * 0.5d0*(k(j+1)-k(j-1)) &
             & + chxr(j) * dr*u(j,2) ) &
             & + dr**2*( ch(j)*hh(j)+chk(j) * k(j) &
             & + chx(j) * chi(j) ) &
             &  )
     end do
     
     hnew(ji) =  2d0*hh(j)-h(j) + dt2 * ( &
          & -mstar*(r(ji)-2*mstar)*rd3(ji)* &
          & (0.5*drd*(3*hh(ji)-4*hh(jimo)+hh(jimt)) &
          & +0.5d0*(r(ji)*u(ji,2) &
          & -0.5*drd*(k(jipo)-k(jimo)))) &
          & -0.5d0*mstar*(2*mstar+r(ji))*rd3(ji)*chi(ji) &
          & -2*mstar**2*rd2(ji)*rd2(ji)*k(ji) &
          & )  
     
     ! 4.4
     ! ***********************************************************
     ! * get S from evolution vars                               *
     ! ***********************************************************
     
     if(gets.eq.'dsdt')then
     
        ! Reconstruction method No 1 (standard)
        ! -------------------------------------
        
        chinew(jmin) = 0.d0
        do j=js,jmax
           chinew(j) = chi(j)-dexp(a(j)-b(j))*0.5d0*dt*(unew(j,1)+u(j,1)) 
        end do
        
     elseif(gets.eq.'dsdr')then
        
        ! Reconstruction method No 2
        ! --------------------------
        
        chinew(jmin) = 0.d0
        
        ! -> I order backward
        ! -------------------
        !do j=js,jmax
        !   chinew(j) = chinew(j-1) + dr*unew(j,2)
        !end do
        ! -------------------
        ! -> II order backward
        ! -------------------
!!!j=js
!!!chinew(j) = chi(j)-dexp(a(j)-b(j))*0.5*dt*(unew(j,1)+u(j,1)) 
        j=js
        chinew(j) = chinew(j-1) + dr*unew(j,2)
        do j=js+1,jmax
           chinew(j) = 1.d0/3.d0*(4.d0*chinew(j-1)-chinew(j-2) &
                & + 2.d0*dr*unew(j,2)) 
        end do
        ! -------------------
        
     endif
     
     ! 4.5
     ! ***********************************************************
     ! * time evolution of k using hamiltonian constraint        *
     ! ***********************************************************
     
     ! computation of knew
     
     ! set f inside
     j=js-1
     f(j) = ( (2*r(j)+0.5d0*r(j)*(l*l+l)-2*m(j) &
          & -8*pi*rho(j)*r3(j))*chinew(j) ) * dr**2
     
     do j=js,ji
        f(j) = ( r(j)*(r(j)-2*m(j))*unew(j,2) &
             & +(2*r(j)+0.5d0*r(j)*(l*l+l)-2*m(j) &
             & -8*pi*rho(j)*r3(j))*chinew(j)  &
             & -8*pi*(p(j)+rho(j))*r2(j)/cs2(j)*hnew(j) &
             &  ) * dr**2
     end do
     
     ! set f at surface 
     
     f(ji) = ( r(ji)*(r(ji)-2*m(ji))*unew(ji,2) & 
          &  +(2*r(ji)+0.5d0*r(ji)*(l*l+l)-2*m(ji) &
          &  -8.d0*pi*rho(ji)*r3(ji))*chinew(ji) &
          !---------------------------------------------
          ! ~ -8 pi H dens / cs^2  
          !---------------------------------------------
          & -8.d0*pi*hnew(ji)*rho(ji)/cs2(ji)&
          !---------------------------------------------
          &  ) * dr**2 
     
     
     ! set f outside
     
     do j=jipo,jmaxmo
        f(j) = ( r(j)*(r(j)-2*m(j))*unew(j,2) &
             & +(2*r(j)+0.5d0*r(j)*(l*l+l)-2*m(j) &
             & -8*pi*rho(j)*r3(j))*chinew(j) ) * dr**2
     end do
     
     j=jmax
     alpha(j) = -q
     beta(j)  =  1.d0
     gamma(j) =  0.d0
     f(j)     =  k(j-1)-k(j)*q
     
     call tridag(alpha,beta,gamma,f,knew,jmax)

     ! 4.6
     ! ***********************************************************
     ! * algebrical computation of z                             *
     ! ***********************************************************
     
     ! spatial derivative of knew :
     
     call diff2nd(knew,dknewdr,r,jmax)
     
     !dknewdr(jmin) = 0.d0
     !j=js
     !dknewdr(j) = drd*(knew(j)-knew(j-1))
     !do j=js,jmax
     !dknewdr(j) = drd*(knew(j)-knew(j-1))
     !do js,jmaxmo
     !do j=js+1,jmax
     !   dknewdr(j) = 0.5d0*drd*(3.d0*knew(j)-4.d0*knew(j-1)+knew(j-2))
     !end do
     !j=ji
     !dknewdr(j) = drd*(knew(j+1)-knew(j))
     !dknewdr(j) = 0.5d0*drd*(-3.d0*knew(j)+4.d0*knew(j+1)-knew(j+2))
     ! j=jmax
     !dknewdr(j) =drd*(knew(j)-knew(j-1))
     !dknewdr(j) = 0.5d0*drd*(3.d0*knew(j)-4.d0*knew(j-1)+knew(j-2))
     
     zanpo = zx*chinew + zk*knew + zkr*dknewdr
     do j=jmin,jimo
        zanpo(j) = 0.d0
     end do

     ! 4.7
     ! ***********************************************************
     ! * time evolution of z                                     *
     ! ***********************************************************
     
     if(methodev.eq.'leapfrog')then
        
        ! Leapfrog evolution
        ! --------------------------

        do j=jipo,jmax-1
           znew(j) = 2d0*zh(j)-z(j)+cf2*( &
                &  czrr(j)*(zh(j+1)-2.d0*zh(j)+zh(j-1)) &
                & + dr * ( czr(j) *0.5d0*(zh(j+1)-zh(j-1)) ) &     
                & + dr**2* vz(j)*zh(j) )
        end do
        
        ! boundary conditions on z
        j=ji
        znew(j) = 2d0/lam * ((r(j)-2*mstar)*r(j)/(lam-2d0+6.d0*mstar*rd(j)) &
             &  *(2d0*(chinew(j)-0.5d0*drd*(4*knew(j+1)-3*knew(j)-knew(j+2))) &
             &  +knew(j)*rd(j)*(r(j)*lam+2.d0*mstar)/(r(j)-2.d0*mstar)))
        
        znew(jmax)  = zh(jmax-1)+(znew(jmax-1)-zh(jmax))*q
        
     elseif(methodev.eq.'upwind')then

        ! Upwind evolution
        ! --------------------------
        
        do j=jipo,jmax-1
           uznew(j,1) = uz(j,1)-0.5d0*dt*drd*eab(j) &
                & * (2.d0*uz(j,1)-uz(j-1,1)-uz(j+1,1)+uz(j+1,2)-uz(j-1,2)) &
                & + dt * sz(j,1)
           uznew(j,2) = uz(j,2)-0.5d0*dt*drd*eab(j) &
                & * (2.d0*uz(j,2)-uz(j-1,2)-uz(j+1,2)+uz(j+1,1)-uz(j-1,1)) &
                & + dt * sz(j,2) 
        end do
        
        ! evolution at boundaries
        do j=ji,ji+2
           znew(j)= 2d0/lam * ((r(j)-2d0*mstar)*r(j)/(lam-2d0+6d0*mstar*rd(j)) &
                & *( 2d0*( chinew(j) &
                & -0.5d0*drd*(4*knew(j+1)-3*knew(j)-knew(j+2))) &
                & +knew(j)* rd(j)*(r(j)*lam+2d0*mstar)/(r(j)-2d0*mstar)))
        end do
        
        ! j=ji : compute uz(1) and uz(2)
        uznew(ji,1) = -dexp(b(ji)-a(ji))*(znew(ji)-z(ji))/dt
        uznew(ji,2) = 0.5d0*drd*(-3*znew(ji)+4*znew(jipo)-znew(ji+2))
        
        ! j=jmax : outgoing sommerfeld conditions   
        uznew(jmax,1)  = uz(jmax-1,1)+(uznew(jmax-1,1)-uz(jmax,1))*q
        uznew(jmax,2)  = uz(jmax-1,2)+(uznew(jmax-1,2)-uz(jmax,2))*q
        
        ! Reconstruction of Z
        ! -------------------

        if(getz.eq.'dzdt')then
           
           ! Metho No1 : obtain z from its time derivative uz(1)
           ! --------------------------------------------------
           
           do j=ji,jmax
              znew(j) = z(j)-dexp(a(j)-b(j))*0.5*dt*(uznew(j,1)+uz(j,1)) 
           end do
           
           ! correction to eliminate the residual
           do j=jipo,jmax-1
              aux(j) = 0.5d0*(znew(j-1)+znew(j+1))
           end do
           do j=jipo,jmax-1
              znew(j) = aux(j)
           end do
           
        else if(getz.eq.'dzdr')then
           
           ! Method No2 : obtain z from its space derivative uz(2)
           ! --------------------------------------------------

           j= jipo
           znew(j) = z(j)-dexp(a(j)-b(j))*0.5*dt*(uznew(j,1)+uz(j,1)) 

           do j=ji+2,jmax
              znew(j) = 1.d0/3.d0*(4.d0*znew(j-1)-znew(j-2) &
                   & + 2.d0*dr*uznew(j,2))
           end do
           
        end if
        
     end if
     
     ! ***********************************************************
     ! * 5. update all the variables for the next time cycle     *
     ! ***********************************************************
     
     u(:,1) = unew(:,1)
     u(:,2) = unew(:,2)
     
     h  = hh
     hh = hnew
     
     k   = knew
     chi = chinew
     
     zan = zanpo
     
     if(methodev.eq.'leapfrog')then
        z  = zh
        zh = znew
     else if(methodev.eq.'upwind')then
        z      = znew
        uz(:,1) = uznew(:,1)
        uz(:,2) = uznew(:,2)
     end if
     
     ! round to 0
     do j=jmin,jmax
        if(dabs(h(j)).lt.1.d-20)      h(j)=0.d0
        if(dabs(hh(j)).lt.1.d-20)     hh(j)=0.d0
        if(dabs(zan(j)).lt.1.d-20)    zan(j)=0.d0
        if(dabs(k(j)).lt.1.d-20)      k(j)=0.d0
        if(dabs(chi(j)).lt.1.d-20)    chi(j)=0.d0
        if(dabs(z(j)).lt.1.d-20)    z(j)=0.d0
        if(dabs(u(j,1)).lt.1.d-20)    u(j,1)=0.d0
        if(dabs(u(j,2)).lt.1.d-20)    u(j,2)=0.d0
        if(dabs(uz(j,1)).lt.1.d-20)    uz(j,1)=0.d0
        if(dabs(uz(j,2)).lt.1.d-20)    uz(j,2)=0.d0
     end do
     
     ! ***********************************************************
     ! * 6. printout of the data as functions of time            *
     ! ***********************************************************
     
     if(n.eq.(0+count*ncount))then
        call polar_outtime(n,dt,js,ji,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4,&
             & jmax,k,chi,h,zan,z)
        count=count+1
     end if
     
  end do
  
  write(*,*)' *************************************** ' 
  write(*,*)' '
  
  ! **************************************************************
  ! * 7. close all the output files                              *
  ! **************************************************************
  
  close(10)
  close(11)
  close(12)
  close(13)
  close(14)
  close(20)
  close(21)
  close(22)
  close(23)
  close(24)
  close(30)
  close(31)
  close(32)
  
  write(*,*)' Datafiles : '
  write(*,*)' '
  write(*,*)' ',filepolartimez
  write(*,*)' ',filepolartimek
  write(*,*)' ',filepolartimechi
  write(*,*)' ',filepolartimeh
  write(*,*)' ',filepolartimezan
  write(*,*)' ',filepolark
  write(*,*)' ',filepolarchi
  write(*,*)' ',filepolarh
  write(*,*)' ',filepolarzan
  write(*,*)' ',filepolarnormz
  write(*,*)' ',filepolarnormk
  write(*,*)' ',filepolarnormchi
   
44 format(i10,e20.8)
   
  return
  
end subroutine polar


! ****************************************************************
! * output of the data                                           *
! ****************************************************************

subroutine polar_outtime(n,dt,js,ji,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4,&
     & jmax,k,chi,h,zan,z)
  
  use nrtype
  
  implicit none
  
  integer(i4b)  :: js,ji,je,ji1,ji2,ji3,ji4,jo1,jo2,jo3,jo4,jmax,n,j
  real(dp)      :: dt,r(jmax),k(jmax),chi(jmax),h(jmax),zan(jmax),z(jmax)
  real(dp)      :: normk,normchi,normz,s
  
  je = jmax-ji
  
  ! **************************************************************
  ! * computation of the square norms                            *
  ! **************************************************************
  
  normchi =  0.d0
  normk   =  0.d0
  normz   =  0.d0

  do j=js-1,jmax
     normk   = normk   + 0.25d0*k(j)*k(j)
     normchi = normchi + chi(j)*chi(j)
  end do
  normk   = normk/jmax
  normchi = normchi/jmax
  
  do j=ji+1,jmax
     normz   = normz + 0.25d0*z(j)*z(j)
  end do
  normz = normz/je

  ! **************************************************************
  ! * printout of data in time                                   *
  ! * zan -> 0.5 zan = Psie                                      * 
  ! * z   -> 0.5 z   = Psie                                      * 
  ! **************************************************************

  write(11,100)n*dt,k(jo1),k(jo2),k(jo3),k(jo4) 
  write(12,100)n*dt,chi(jo1),chi(jo2),chi(jo3),chi(jo4) 
  write(13,100)n*dt,h(ji1),h(ji2),h(ji3),h(ji4)
  write(14,100)n*dt,0.5d0*zan(jo1),0.5d0*zan(jo2),0.5d0*zan(jo3),0.5d0*zan(jo4)   
  write(10,100)n*dt,0.5d0*z(jo1),0.5d0*z(jo2),0.5d0*z(jo3),0.5d0*z(jo4)   

  write(30,300)n*dt,normz
  write(31,300)n*dt,normk
  write(32,300)n*dt,normchi 

100 format(5e20.12)
300 format(2e20.12)
  
  return
end subroutine polar_outtime

! ****************************************************************
! * print out movies for ygraph                                  *
! ****************************************************************

subroutine polar_movie(n,dt,r,k,chi,zan,z,h,js,ji,jmax,djoutput)
   
  use nrtype
  
  implicit none
  
  integer(i4b) :: jmin,js,ji,jmax,n,j,djoutput
  real(dp)     :: dt,r(jmax),k(jmax),chi(jmax),h(jmax),zan(jmax),z(jmax)
  
  write(24,*)' '              
  write(24,*)'"time = ',n*dt  
  do j=js,ji
     write(24,200)r(j),h(j)
  end do
  
  write(23,*)' '              
  write(23,*)'"time = ',n*dt  
  do j=ji,jmax,djoutput
     write(23,200)r(j),0.5d0*zan(j)
  end do
  
  write(21,*)' '              
  write(21,*)'"time = ',n*dt  
  write(22,*)' '              
  write(22,*)'"time = ',n*dt  
  do j=js,jmax,djoutput             
     write(21,200)r(j),k(j)         
     write(22,200)r(j),chi(j)       
  end do
  
  write(20,*)' '              
  write(20,*)'"time = ',n*dt  
  do j=ji,jmax,djoutput
     write(23,200)r(j),0.5d0*z(j)
  end do

200 format(2e20.12)
  
  return
end subroutine polar_movie
      


      




 
