! **************************************************************
! * nr : adapted from nr.f90 module                            *
! **************************************************************

module nradp

  interface
     subroutine tridag(a,b,c,r,u,n)
       use nrtype  
       implicit none 
       integer(i4b):: n
       real(dp)    :: a(n),b(n),c(n),r(n),u(n)
     end subroutine tridag
  end interface

  interface
     subroutine spline(x,y,n,yp1,ypn,y2)
       use nrtype  
       implicit none
       integer(i4b):: n
       real(dp):: yp1,ypn,x(n),y(n),y2(n)
     end subroutine spline
  end interface

  interface
     subroutine splint(xa,ya,y2a,n,x,y)
       use nrtype  
       implicit none
       integer(i4b)  :: n
       real(dp) :: x,y,xa(n),y2a(n),ya(n)
     end subroutine splint
  end interface

  interface
     subroutine splint_der(xa,ya,y2a,n,x,y,y1,y2)
       use nrtype  
       implicit none
       integer(i4b)  :: n
       real(dp) :: x,y,y1,y2,xa(n),y2a(n),ya(n)
     end subroutine splint_der
  end interface
  
end module nradp

!--------------------------------------------
! * nr : tridag.f90 , matrix inversion                           *
!--------------------------------------------

subroutine tridag(a,b,c,r,u,n)
  
  use nrtype  

  implicit none 

  integer(i4b):: n,j
  real(dp)    :: a(n),b(n),c(n),r(n),u(n)
  real(dp)    :: bet,ggm(n)

  if(b(1).eq.0.d0)pause 'tridag: rewrite equations'
  bet=b(1)
  u(1)=r(1)/bet       
  do j=2,n
     ggm(j)=c(j-1)/bet
     bet = b(j)-a(j)*ggm(j)
     if(bet.eq.0.d0)pause 'tridag failed'
     u(j) = (r(j)-a(j)*u(j-1))/bet
  end do
  do j=n-1,1,-1
     bet = ggm(j+1)*u(j+1)
     u(j) = u(j)- ggm(j+1)*u(j+1)
  end do

  return

end subroutine tridag

!********************************************************
!*** subs tridag works only with jmin=1
!*** one day try this to use every jmin ...
!subroutine tridag(a,b,c,r,u,ni,nf)
!  use nrtype  
!  implicit none    
!  integer(i4b) :: ni,nf,j
!  real(dp)     :: a(nf),b(nf),c(nf),r(nf),u(nf)
!  real(dp)     :: bet,ggm(nf)
!  if (b(ni).eq.0.d0) pause ' tridag: rewrite equations'
!  bet=b(ni)
!  u(ni)=r(ni)/bet       
!  do j=ni+1,nf
!     ggm(j)=c(j-1)/bet
!     bet = b(j)-a(j)*ggm(j)
!     if (bet.eq.0.d0) pause 'tridag failed'
!     u(j) = (r(j)-a(j)*u(j-1))/bet
!  end do
!  do j=nf-1,ni,-1
!     bet = ggm(j+1)*u(j+1)
!     u(j) = u(j)- ggm(j+1)*u(j+1)
!  end do
!  return

!end subroutine tridag
!***
!********************************************************

!-----------------------------------------------------------
! * nr : spline.f90 , compute II derivative to interpolate
!      computes second order derivative of the function y(x)
!      given the first order derivatives at x(1) and x(n)
!-----------------------------------------------------------

subroutine spline(x,y,n,yp1,ypn,y2)

  use nrtype  

  implicit none

  integer(i4b):: n,NMAX
  real(dp):: yp1,ypn,x(n),y(n),y2(n)
  parameter (NMAX=50000)
  integer(i4b) :: i,k
  real(dp) :: p,qn,sig,un,u(NMAX)

  if (yp1.gt..99d30) then
     y2(1)=0.d0
     u(1)=0.d0
  else
     y2(1)=-0.5d0
     u(1)=(3.d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
  endif
  do i=2,n-1
     sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
     p=sig*y2(i-1)+2.d0
     y2(i)=(sig-1.d0)/p
     u(i)=(6.d0*((y(i+1)-y(i))/(x(i+ 1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
  enddo
  if (ypn.gt..99d30) then
     qn=0.d0
     un=0.d0
  else
     qn=0.5d0
     un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
  endif
  y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
  do k=n-1,1,-1
     y2(k)=y2(k)*y2(k+1)+u(k)
  enddo

  return

end subroutine spline

!-----------------------------------------------------------
! * nr : splint.f90 , cubic interpolation at point x(i) of
!      function y(x(i)) using values
!      xa(1...n) ya(1...n) ya''(1...n)
!-----------------------------------------------------------

subroutine splint(xa,ya,y2a,n,x,y)

  use nrtype  

  implicit none

  integer(i4b)  :: n
  real(dp) :: x,y,xa(n),y2a(n),ya(n)
  integer(i4b) :: k,khi,klo
  real(dp) :: a,b,h

  klo=1
  khi=n
1 if (khi-klo.gt.1) then
     k=(khi+klo)/2
     if(xa(k).gt.x)then
        khi=k
     else
        klo=k
     endif
     goto 1
  endif
  h=xa(khi)-xa(klo)
  if (h.eq.0.d0) pause 'bad xa input in splint'
  a=(xa(khi)-x)/h
  b=(x-xa(klo))/h
  y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0

  return

end subroutine splint

!---------------------------------------------
! Modification of splint, return derivatives:
! y(x), y'(x) and y''(x) 
! See NR pg.114
!---------------------------------------------

subroutine splint_der(xa,ya,y2a,n,x,y,y1,y2)

  use nrtype  

  implicit none

  integer(i4b)  :: n
  real(dp) :: x,y,y1,y2,xa(n),y2a(n),ya(n)
  integer(i4b) :: k,khi,klo
  real(dp) :: a,b,h

  klo=1
  khi=n
1 if (khi-klo.gt.1) then
     k=(khi+klo)/2
     if(xa(k).gt.x)then
        khi=k
     else
        klo=k
     endif
     goto 1
  endif
  h=xa(khi)-xa(klo)
  if (h.eq.0.d0) pause 'bad xa input in splint_der'
  a=(xa(khi)-x)/h
  b=(x-xa(klo))/h

  y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0

  y1=(ya(khi)-ya(klo))/h-((3.d0*a**2-1.d0)*y2a(klo)+(3.d0*b**2-1.d0)*y2a(khi))*h/6.d0

  y2=a*y2a(klo)+b*y2a(khi)

  return
  
end subroutine splint_der


