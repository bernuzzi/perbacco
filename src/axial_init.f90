! ****************************************************************
! * Initial condition for the time evolution                     *
! ****************************************************************

subroutine axial_init(zold,z,dt,r,dr,ri,sigma,l,initialflag,crr, &
     &  jmin,jmax,fileaxialinit,methodev,filez)

  use nrtype

  implicit none  

  integer(i4b)  :: j,jmin,jmax 
  real(dp)      :: r(jmax),zold(jmax),z(jmax),dzdt(jmax),crr(jmax)
  real(dp)      :: l,omega,tau,ri,sigma,dt,dr
  real(dp)      :: s2,a,intgr
  character(64) :: fileaxialinit,methodev,filez
  character(64) :: initialflag
   
     
  s2=sigma*sigma

  if(initialflag.eq.'gauss')then

     ! **************************************************************
     ! * gaussian pulse                                             *
     ! **************************************************************

     write(*,*)' initial condition :'
     write(*,*)' '
     write(*,*)' z := gaussian pulse'
     write(*,*)'    ri    = ',ri
     write(*,*)'    sigma = ',sigma
     write(*,*)' '

     ! compute amplitude
     intgr=0
     do j=jmin,jmax 
        intgr = intgr + dexp( -(ri-r(j))**(2.d0)/s2 )*dr*r(j)**2
     end do
     a =  1/intgr
     
     if(methodev.eq.'leapfrog')then
        ! leapfrog     
        zold = a*dexp( -(ri-r)**(2.d0)/s2 )                 ! z(r,0) 
        z    = zold + dt*sqrt(crr)*2.0d0*(ri-r)/s2*zold     ! ingoing condition: z,t = sqrt(crr)z,r
     elseif(methodev.eq.'lw')then    
        ! lax-wendroff
        zold = a*dexp( -(ri-r)**(2.d0)/s2 )                 ! z(r,0) 
        !z    = 2.d0*sqrt(crr)*2.0d0*(ri-r)/s2*zold         ! w(r,0)= z,t - sqrt(crr)z,r !time-symmetric
        z    = 0                                            ! w(r,0)= z,t - sqrt(crr)z,r !ingoing
     end if
     
     dzdt = sqrt(crr)*2.0d0*(ri-r)/s2*zold                  ! dz/dt(r,0) for ingoing id
     !dzdt=0                                                ! dz/dt(r,0) for time symm id
     
  else if(initialflag.eq.'wavep')then
     
     ! **************************************************************
     ! * wave packet                                                *       
     ! **************************************************************

     write(*,*)' initial condition :'
     write(*,*)' '
     write(*,*)' z := wave packet '
     write(*,*)'   ri        = ',ri
     write(*,*)'   sigma     = ',sigma
     write(*,*)'   frequency = ',omega
     write(*,*)'   tau       = ',tau
     write(*,*)' '
     
     !fixme: take from out
     
     omega=0.7
     tau=0.3
     
     ! compute amplitude
     intgr=0
     do j=jmin,jmax
        intgr = intgr + dexp( -(ri-r(j))**(2.d0)/s2 )*sin(omega)*dexp(-tau*r(j))*dr*r(j)**2
     end do
     a = 1/intgr
     
     if(methodev.eq.'leapfrog')then
        
        ! leapfrog     
        zold = a*sin(omega*r)*dexp(-(ri-r/sigma)**(2.d0))  ! z(r,0) 
        z    = zold + dt*sqrt(crr)*( zold*(2.d0*(ri-r)/s2 &
             & + omega*cos(omega*r)/sin(omega*r) -tau) )   ! ingoing condition: z,t = sqrt(crr)z,r
        
     else if(methodev.eq.'lw')then
        
        ! lax-wendroff
        zold = a*sin(omega*r)*dexp(-(ri-r/sigma)**(2.d0))   ! z(r,0) 
        !z    = zold + dt*sqrt(crr)*( zold*(2.d0*(ri-r)/s2 &
        !&+ omega*cos(omega*r)/sin(omega*r) -tau) )  ! w(r,0)= z,t - sqrt(crr)z,r !time-symmetric
        z    = 0                        ! w(r,0)= z,t - sqrt(crr)z,r !ingoing
        
     end if
     
     dzdt = sqrt(crr)*( zold*(2.d0*(ri-r)/s2 &
          & + omega*cos(omega*r)/sin(omega*r) -tau) )             ! dz/dt(r,0) for ingoing id
     
     !dzdt=0                                                ! dz/dt(r,0) for time symm id
     
  else if(initialflag.eq.'file')then
     
     ! **************************************************************
     ! * from file                                                  *       
     ! **************************************************************
     
     write(*,*)' initial condition :'
     write(*,*)' '
     write(*,*)' z := from file ',filez
     write(*,*)' '
     
     open(27, file = filez  ,status = 'old')
     do j=1,jmax
        read(27,*) r(j),zold(j)
     end do
     close(27)

     if(methodev.eq.'leapfrog')then
        
        ! leapfrog      
        
        ! ingoing condition: z,t = sqrt(crr)z,r
        do j=2,jmax-1
           z(j) = sqrt(crr(j))*(zold(j+1)-zold(j-1))/(r(j+1)-r(j-1))
        end do
        z(1)    = sqrt(crr(j))*(zold(2)-zold(1))/(r(2)-r(1))
        z(jmax) = sqrt(crr(j))*(zold(jmax)-zold(jmax-1))/(r(jmax)-r(jmax-1))

        dzdt = z 
        
     else if(methodev.eq.'lw')then
        
        write(*,*)' No ID from file for Lax-Wendroff implemented yet, choose leapfrog evolution.'
        
        ! enable later ...        
        ! lax-wendroff
        
     end if
     
  end if
  
  ! write on file
  open(40,file=fileaxialinit ,status='unknown')
  do j=jmin,jmax
     write(40,4000)r(j),zold(j),dzdt(j)
  end do
  close(40)

4000 format(3f20.12)
  
  return
end subroutine axial_init
