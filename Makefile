#
# PerBaCCo makefile
#

BASE = $(shell /bin/pwd)
EXED = $(BASE)/$(EXECTBL)
OBJD = $(BASE)/obj
SRCD = $(BASE)/src

vpath %.o $(OBJD)
vpath %.f90 $(SRCD)
vpath %.f $(SRCD)

EXECTBL = PerBaCCo.x

SRCS = 	nrtype.f90 nrutil.f90 nradp.f90 myutil.f90 const.f90	\
	eos.f90 background.f90 tov_odeint.f90 equil.f90		\
	radial.f90 						\
	axial.f90 axial_init.f90 				\
	polar.f90 polar_potentials.f90 polar_init.f90 		\
	driver.f90						\

OBJS =  nrtype.o nrutil.o nradp.o myutil.o const.o		\
	eos.o background.o tov_odeint.o equil.o			\
	radial.o 						\
	axial.o axial_init.o 					\
	polar.o polar_potentials.o polar_init.o 		\
	driver.o						\

#F90 = ifort
F90 = gfortran-9
#F90FLAGS = -O3     #super optimized
#F90FLAGS = -O1
#F90FLAGS = -static 
F90FLAGS = -g      #debug

all: $(EXECTBL)

$(EXECTBL): $(OBJS)
	@echo '' 
	@echo '===> Building the executable'
	@echo ''
	cd $(OBJD); $(F90) $(F90FLAGS) -o ../$(EXECTBL) $(OBJS)
	@echo ''
	@echo '===> All Done !'

%.o $(OBJD)/%.o: %.f90
	@echo '' 
	@echo '===> Building' $*.o	
	@echo ''
	cd $(OBJD); $(F90) -c $(F90FLAGS) $(SRCD)/$(*).f90

flush:
	rm -f $(EXECTBL) *.dat $(OBJD)/*.o $(OBJD)/*.kmo $(OBJD)/*.mod $(OBJD)/*.d $(OBJD)/*.pc*
clean:
	rm -f $(OBJD)/*.o $(OBJD)/*.kmo $(OBJD)/*.mod $(OBJD)/*.d $(OBJD)/*.pc*

.SUFFIXES: $(SUFFIXES) .f90

.f90.o:
	$(F90) $(F90FLAGS) -c $<

# DO NOT DELETE
