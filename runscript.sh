#!/bin/bash
# Use this script to launch job and save data 
# [bernuzzi@prpc81]$ qsub -N NAME runscript.sh

# ---------------------------------------------------------------
#  Set up the name of the EXE and of the PAR files
# ---------------------------------------------------------------

export WORKDIR=$PBS_JOBNAME
export PARF=$PBS_JOBNAME.par
export EXE=./PerturbTOV.exe.HermiteInt

# ---------------------------------------------------------------
#  Launch job 
# ---------------------------------------------------------------

cd $PBS_O_WORKDIR
echo "................................"
echo "Current launch directory is:"
pwd
mkdir $WORKDIR
cp $EXE $WORKDIR
cp $PARF $WORKDIR

cd $WORKDIR/
echo "................................"
echo "Current execution directory is:"
pwd
echo "................................"
echo "running..."
time $EXE $PARF 
echo "...end"
echo "................................"

exit

# ---------------------------------------------------------------
#  End
# ---------------------------------------------------------------
